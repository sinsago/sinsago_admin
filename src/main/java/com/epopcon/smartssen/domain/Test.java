package com.epopcon.smartssen.domain;

public class Test {
	
	private Long test_seq;
	private String st_id;
	private String test_date;
	private String test_rating;
	private Integer approve_cnt;
	private Integer keep_deadline_cnt;
	private Integer over_deadline_cnt;
	private Integer give_back_cnt;
	private Integer test_fee;
	private String apply_date;
	private String reg_date;
	
	private String is_removed; //added fee
	private Long added_seq;
	
	public String getIs_removed() {
		return is_removed;
	}
	public void setIs_removed(String is_removed) {
		this.is_removed = is_removed;
	}
	public Long getAdded_seq() {
		return added_seq;
	}
	public void setAdded_seq(Long added_seq) {
		this.added_seq = added_seq;
	}
	public Long getTest_seq() {
		return test_seq;
	}
	public void setTest_seq(Long test_seq) {
		this.test_seq = test_seq;
	}
	public String getSt_id() {
		return st_id;
	}
	public void setSt_id(String st_id) {
		this.st_id = st_id;
	}
	public String getTest_date() {
		return test_date;
	}
	public void setTest_date(String test_date) {
		this.test_date = test_date;
	}
	public String getTest_rating() {
		return test_rating;
	}
	public void setTest_rating(String test_rating) {
		this.test_rating = test_rating;
	}
	public Integer getApprove_cnt() {
		return approve_cnt;
	}
	public void setApprove_cnt(Integer approve_cnt) {
		this.approve_cnt = approve_cnt;
	}
	public Integer getKeep_deadline_cnt() {
		return keep_deadline_cnt;
	}
	public void setKeep_deadline_cnt(Integer keep_deadline_cnt) {
		this.keep_deadline_cnt = keep_deadline_cnt;
	}
	public Integer getOver_deadline_cnt() {
		return over_deadline_cnt;
	}
	public void setOver_deadline_cnt(Integer over_deadline_cnt) {
		this.over_deadline_cnt = over_deadline_cnt;
	}
	public Integer getGive_back_cnt() {
		return give_back_cnt;
	}
	public void setGive_back_cnt(Integer give_back_cnt) {
		this.give_back_cnt = give_back_cnt;
	}
	public Integer getTest_fee() {
		return test_fee;
	}
	public void setTest_fee(Integer test_fee) {
		this.test_fee = test_fee;
	}
	public String getApply_date() {
		return apply_date;
	}
	public void setApply_date(String apply_date) {
		this.apply_date = apply_date;
	}
	public String getReg_date() {
		return reg_date;
	}
	public void setReg_date(String reg_date) {
		this.reg_date = reg_date;
	}
	@Override
	public String toString() {
		return "Test [test_seq=" + test_seq + ", st_id=" + st_id + ", test_date=" + test_date + ", test_rating="
				+ test_rating + ", approve_cnt=" + approve_cnt + ", keep_deadline_cnt=" + keep_deadline_cnt
				+ ", over_deadline_cnt=" + over_deadline_cnt + ", give_back_cnt=" + give_back_cnt + ", test_fee="
				+ test_fee + ", apply_date=" + apply_date + ", reg_date=" + reg_date + "]";
	}
}
