package com.epopcon.smartssen.domain;

public class Fee {
	
	private Long fee_seq;
	private String st_id;
	private String st_nm;
	private Integer st_type;
	private String reg_date;
	private String st_rrn;
	private String fee_date;
	private Integer an_approve_cnt;
	private Integer calc_fee;
	private Integer calc_amount;
	private String calc_date;
	private Integer calc_state;
	private String calc_payments_date;
	
	private String apply_date;
	
	private String start_day;
	private String end_day;
	
	private Integer total;
	private Integer rownum;
	
	public String getApply_date() {
		return apply_date;
	}
	public void setApply_date(String apply_date) {
		this.apply_date = apply_date;
	}
	private Integer sub_sum;
	private Integer tot_sum;
	
	private Boolean thisMonth = false;
	
	public Integer getSub_sum() {
		return sub_sum;
	}
	public void setSub_sum(Integer sub_sum) {
		this.sub_sum = sub_sum;
	}
	public Integer getTot_sum() {
		return tot_sum;
	}
	public void setTot_sum(Integer tot_sum) {
		this.tot_sum = tot_sum;
	}
	public Long getFee_seq() {
		return fee_seq;
	}
	public void setFee_seq(Long fee_seq) {
		this.fee_seq = fee_seq;
	}
	public String getSt_id() {
		return st_id;
	}
	public void setSt_id(String st_id) {
		this.st_id = st_id;
	}
	public String getSt_nm() {
		return st_nm;
	}
	public void setSt_nm(String st_nm) {
		this.st_nm = st_nm;
	}
	public Integer getSt_type() {
		return st_type;
	}
	public void setSt_type(Integer st_type) {
		this.st_type = st_type;
	}
	public String getReg_date() {
		return reg_date;
	}
	public void setReg_date(String reg_date) {
		this.reg_date = reg_date;
	}
	public String getSt_rrn() {
		return st_rrn;
	}
	public void setSt_rrn(String st_rrn) {
		this.st_rrn = st_rrn;
	}
	public String getFee_date() {
		return fee_date;
	}
	public void setFee_date(String fee_date) {
		this.fee_date = fee_date;
	}
	public Integer getAn_approve_cnt() {
		return an_approve_cnt;
	}
	public void setAn_approve_cnt(Integer an_approve_cnt) {
		this.an_approve_cnt = an_approve_cnt;
	}
	public Integer getCalc_fee() {
		return calc_fee;
	}
	public void setCalc_fee(Integer calc_fee) {
		this.calc_fee = calc_fee;
	}
	public Integer getCalc_amount() {
		return calc_amount;
	}
	public void setCalc_amount(Integer calc_amount) {
		this.calc_amount = calc_amount;
	}
	public String getCalc_date() {
		return calc_date;
	}
	public void setCalc_date(String calc_date) {
		this.calc_date = calc_date;
	}
	public Integer getCalc_state() {
		return calc_state;
	}
	public void setCalc_state(Integer calc_state) {
		this.calc_state = calc_state;
	}
	public String getCalc_payments_date() {
		return calc_payments_date;
	}
	public void setCalc_payments_date(String calc_payments_date) {
		this.calc_payments_date = calc_payments_date;
	}
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	public Integer getRownum() {
		return rownum;
	}
	public void setRownum(Integer rownum) {
		this.rownum = rownum;
	}
	public String getStart_day() {
		return start_day;
	}
	public void setStart_day(String start_day) {
		this.start_day = start_day;
	}
	public String getEnd_day() {
		return end_day;
	}
	public void setEnd_day(String end_day) {
		this.end_day = end_day;
	}
	public Boolean getThisMonth() {
		return thisMonth;
	}
	public void setThisMonth(Boolean thisMonth) {
		this.thisMonth = thisMonth;
	}
	@Override
	public String toString() {
		return "Fee [fee_seq=" + fee_seq + ", st_id=" + st_id + ", st_nm=" + st_nm + ", reg_date=" + reg_date
				+ ", st_rrn=" + st_rrn + ", fee_date=" + fee_date + ", an_approve_cnt=" + an_approve_cnt + ", calc_fee="
				+ calc_fee + ", calc_amount=" + calc_amount + ", calc_date=" + calc_date + ", calc_state=" + calc_state
				+ ", calc_payments_date=" + calc_payments_date + ", total=" + total + ", rownum=" + rownum + "]";
	}
}
