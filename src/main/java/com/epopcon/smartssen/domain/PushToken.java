package com.epopcon.smartssen.domain;

public class PushToken {
	
	private Long push_seq;
	private String st_id;
	private String push_token;
	private String gcm_token;
	private String reg_date;
	
	public Long getPush_seq() {
		return push_seq;
	}
	public void setPush_seq(Long push_seq) {
		this.push_seq = push_seq;
	}
	public String getSt_id() {
		return st_id;
	}
	public void setSt_id(String st_id) {
		this.st_id = st_id;
	}
	public String getPush_token() {
		return push_token;
	}
	public void setPush_token(String push_token) {
		this.push_token = push_token;
	}
	public String getGcm_token() {
		return gcm_token;
	}
	public void setGcm_token(String gcm_token) {
		this.gcm_token = gcm_token;
	}
	public String getReg_date() {
		return reg_date;
	}
	public void setReg_date(String reg_date) {
		this.reg_date = reg_date;
	}
	
	@Override
	public String toString() {
		return "PushToken [push_seq=" + push_seq + ", st_id=" + st_id + ", push_token=" + push_token + ", gcm_token="
				+ gcm_token + ", reg_date=" + reg_date + "]";
	}

}
