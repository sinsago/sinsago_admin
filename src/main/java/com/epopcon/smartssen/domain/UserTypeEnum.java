package com.epopcon.smartssen.domain;

public enum UserTypeEnum {
	
	ADMIN(10),
	USER(20);
	
	
	
	private Integer value;
	private UserTypeEnum(Integer value){
		this.value = value;
	}
	
	public Integer getValue() {
		return this.value;
	}

}
