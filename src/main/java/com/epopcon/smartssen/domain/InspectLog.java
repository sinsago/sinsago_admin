package com.epopcon.smartssen.domain;

public class InspectLog {
	
	public InspectLog() {}
	
	private Integer seq;
	private String an_id;
	private Integer inspect_state;
	private String inspect_reason;
	private String admin_id;
	private String admin_nm;
	private String reg_date;
	
	public Integer getSeq() {
		return seq;
	}
	public void setSeq(Integer seq) {
		this.seq = seq;
	}
	public String getAn_id() {
		return an_id;
	}
	public void setAn_id(String an_id) {
		this.an_id = an_id;
	}
	public Integer getInspect_state() {
		return inspect_state;
	}
	public void setInspect_state(Integer inspect_state) {
		this.inspect_state = inspect_state;
	}
	
	public String getInspect_reason() {
		return inspect_reason;
	}
	public void setInspect_reason(String inspect_reason) {
		this.inspect_reason = inspect_reason;
	}
	public String getAdmin_id() {
		return admin_id;
	}
	public void setAdmin_id(String admin_id) {
		this.admin_id = admin_id;
	}
	public String getAdmin_nm() {
		return admin_nm;
	}
	public void setAdmin_nm(String admin_nm) {
		this.admin_nm = admin_nm;
	}
	public String getReg_date() {
		return reg_date;
	}
	public void setReg_date(String reg_date) {
		this.reg_date = reg_date;
	}
	
	@Override
	public String toString() {
		return "InspectLog [seq=" + seq + ", an_id=" + an_id + ", inspect_state=" + inspect_state + ", admin_id="
				+ admin_id + ", admin_nm=" + admin_nm + ", reg_date=" + reg_date + "]";
	}
}