package com.epopcon.smartssen.domain;

public class NoteLog {
	
	public NoteLog() {}
	
	private Integer an_log_seq;
	private String an_id;
	private Integer an_state;
	private Integer writer_type;
	private Integer refuse_state;
	private String writer_id;
	private String writer_nm;
	private String reg_date;
	
	private Integer total;
	private Integer rownum;
	
	public Integer getAn_log_seq() {
		return an_log_seq;
	}
	public void setAn_log_seq(Integer an_log_seq) {
		this.an_log_seq = an_log_seq;
	}
	public String getAn_id() {
		return an_id;
	}
	public void setAn_id(String an_id) {
		this.an_id = an_id;
	}
	public Integer getAn_state() {
		return an_state;
	}
	public void setAn_state(Integer an_state) {
		this.an_state = an_state;
	}
	public Integer getWriter_type() {
		return writer_type;
	}
	public void setWriter_type(Integer writer_type) {
		this.writer_type = writer_type;
	}
	public String getWriter_id() {
		return writer_id;
	}
	public void setWriter_id(String writer_id) {
		this.writer_id = writer_id;
	}
	public String getWriter_nm() {
		return writer_nm;
	}
	public void setWriter_nm(String writer_nm) {
		this.writer_nm = writer_nm;
	}
	public String getReg_date() {
		return reg_date;
	}
	public void setReg_date(String reg_date) {
		this.reg_date = reg_date;
	}
	
	@Override
	public String toString() {
		return "NoteLog [an_log_seq=" + an_log_seq + ", an_id=" + an_id + ", an_state=" + an_state + ", writer_type="
				+ writer_type + ", writer_id=" + writer_id + ", reg_date=" + reg_date + "]";
	}
	public Integer getRownum() {
		return rownum;
	}
	public void setRownum(Integer rownum) {
		this.rownum = rownum;
	}
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	
	
	public NoteLog(String an_id, Integer an_state, Integer writer_type, String writer_id) {
		super();
		this.an_id = an_id;
		this.an_state = an_state;
		this.writer_type = writer_type;
		this.writer_id = writer_id;
	}
	public Integer getRefuse_state() {
		return refuse_state;
	}
	public void setRefuse_state(Integer refuse_state) {
		this.refuse_state = refuse_state;
	}
	
	
}