package com.epopcon.smartssen.domain;

public class TestResult {
	
	private String st_id;
	private Integer note_cnt;
	private Integer state0;
	private Integer state1;
	private Integer state2;
	private Integer state3;
	private Integer state4;
	
	private Integer keep_deadline;
	private Integer over_deadline;
	
	private Integer keep_deadline_rate;
	private Integer approve_rate;
	
	private String next_day;
	private String apply_day;
	
	public String getSt_id() {
		return st_id;
	}
	public void setSt_id(String st_id) {
		this.st_id = st_id;
	}
	public Integer getNote_cnt() {
		return note_cnt;
	}
	public void setNote_cnt(Integer note_cnt) {
		this.note_cnt = note_cnt;
	}
	public Integer getState0() {
		return state0;
	}
	public void setState0(Integer state0) {
		this.state0 = state0;
	}
	public Integer getState1() {
		return state1;
	}
	public void setState1(Integer state1) {
		this.state1 = state1;
	}
	public Integer getState2() {
		return state2;
	}
	public void setState2(Integer state2) {
		this.state2 = state2;
	}
	public Integer getState3() {
		return state3;
	}
	public void setState3(Integer state3) {
		this.state3 = state3;
	}
	public Integer getState4() {
		return state4;
	}
	public void setState4(Integer state4) {
		this.state4 = state4;
	}
	public Integer getKeep_deadline() {
		return keep_deadline;
	}
	public void setKeep_deadline(Integer keep_deadline) {
		this.keep_deadline = keep_deadline;
	}
	public Integer getOver_deadline() {
		return over_deadline;
	}
	public void setOver_deadline(Integer over_deadline) {
		this.over_deadline = over_deadline;
	}
	public Integer getKeep_deadline_rate() {
		return keep_deadline_rate;
	}
	public void setKeep_deadline_rate(Integer keep_deadline_rate) {
		this.keep_deadline_rate = keep_deadline_rate;
	}
	public Integer getApprove_rate() {
		return approve_rate;
	}
	public void setApprove_rate(Integer approve_rate) {
		this.approve_rate = approve_rate;
	}
	public String getNext_day() {
		return next_day;
	}
	public void setNext_day(String next_day) {
		this.next_day = next_day;
	}
	public String getApply_day() {
		return apply_day;
	}
	public void setApply_day(String apply_day) {
		this.apply_day = apply_day;
	}
	
	
	
	
	
	

}
