package com.epopcon.smartssen.domain;

public class TodaysFee {
	
	private Integer fee;
	
	private String based_date;
	
	public Integer getFee() {
		return fee;
	}
	public void setFee(Integer fee) {
		this.fee = fee;
	}
	public String getBased_date() {
		return based_date;
	}
	public void setBased_date(String based_date) {
		this.based_date = based_date;
	}
	
	@Override
	public String toString() {
		return "TodaysFee [fee=" + fee + ", based_date=" + based_date + "]";
	}
	
	
}
