package com.epopcon.smartssen.domain;

public class Teacher {
	
	private Long st_seq;
	private String st_id;
	private String st_pwd;
	private String st_nm;
	private String st_rrn;
	private String st_tel;
	private Integer st_state;
	private String mngr_id;
	private String mngr_nm;
	private Integer default_fee;
	private Integer period_fee;
	private Integer test_fee;
	private Integer tot_fee;
	private String reg_date;
	private String end_date;
	
	private Integer total;
	private Integer rownum;
	
	private Integer st_type;
	
	public Long getSt_seq() {
		return st_seq;
	}
	public void setSt_seq(Long st_seq) {
		this.st_seq = st_seq;
	}
	public String getSt_id() {
		return st_id;
	}
	public void setSt_id(String st_id) {
		this.st_id = st_id;
	}
	public String getSt_pwd() {
		return st_pwd;
	}
	public void setSt_pwd(String st_pwd) {
		this.st_pwd = st_pwd;
	}
	public String getSt_nm() {
		return st_nm;
	}
	public void setSt_nm(String st_nm) {
		this.st_nm = st_nm;
	}
	public String getSt_rrn() {
		return st_rrn;
	}
	public void setSt_rrn(String st_rrn) {
		this.st_rrn = st_rrn;
	}
	public String getSt_tel() {
		return st_tel;
	}
	public void setSt_tel(String st_tel) {
		this.st_tel = st_tel;
	}
	public Integer getSt_state() {
		return st_state;
	}
	public Integer getSt_type() {
		return st_type;
	}
	public void setSt_type(Integer st_type) {
		this.st_type = st_type;
	}
	public void setSt_state(Integer st_state) {
		this.st_state = st_state;
	}
	public String getMngr_id() {
		return mngr_id;
	}
	public void setMngr_id(String mngr_id) {
		this.mngr_id = mngr_id;
	}
	public String getMngr_nm() {
		return mngr_nm;
	}
	public void setMngr_nm(String mngr_nm) {
		this.mngr_nm = mngr_nm;
	}
	public Integer getDefault_fee() {
		return default_fee;
	}
	public void setDefault_fee(Integer default_fee) {
		this.default_fee = default_fee;
	}
	public Integer getPeriod_fee() {
		return period_fee;
	}
	public void setPeriod_fee(Integer period_fee) {
		this.period_fee = period_fee;
	}
	public Integer getTest_fee() {
		return test_fee;
	}
	public void setTest_fee(Integer test_fee) {
		this.test_fee = test_fee;
	}
	public String getReg_date() {
		return reg_date;
	}
	public void setReg_date(String reg_date) {
		this.reg_date = reg_date;
	}
	public String getEnd_date() {
		return end_date;
	}
	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	public Integer getRownum() {
		return rownum;
	}
	public void setRownum(Integer rownum) {
		this.rownum = rownum;
	}
	@Override
	public String toString() {
		return "Teacher [st_seq=" + st_seq + ", st_id=" + st_id + ", st_pwd=" + st_pwd + ", st_nm=" + st_nm
				+ ", st_rrn=" + st_rrn + ", st_tel=" + st_tel + ", st_state=" + st_state + ", mngr_id=" + mngr_id
				+ ", default_fee=" + default_fee + ", period_fee=" + period_fee + ", test_fee=" + test_fee
				+ ", reg_date=" + reg_date + ", end_date=" + end_date + ", total=" + total + ", rownum=" + rownum + "]";
	}
	public Integer getTot_fee() {
		return tot_fee;
	}
	public void setTot_fee(Integer tot_fee) {
		this.tot_fee = tot_fee;
	}
	
	
	
	
	
}
