package com.epopcon.smartssen.domain;

public class ApproveManager {
	
	private Integer seq;
	
	private Integer approve_state;
	private String approve_date;
	private String reg_date;
	
	public ApproveManager() {
		// TODO Auto-generated constructor stub
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public Integer getApprove_state() {
		return approve_state;
	}

	public void setApprove_state(Integer approve_state) {
		this.approve_state = approve_state;
	}

	public String getApprove_date() {
		return approve_date;
	}

	public void setApprove_date(String approve_date) {
		this.approve_date = approve_date;
	}

	public String getReg_date() {
		return reg_date;
	}

	public void setReg_date(String reg_date) {
		this.reg_date = reg_date;
	}
	
	

}
