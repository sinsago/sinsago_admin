package com.epopcon.smartssen.domain;

public class Refuse {
	
	public Refuse() {
	}
	
	public Refuse(String an_id) {
		super();
		this.an_id = an_id;
	}


	public Refuse(Integer seq, String an_id, String comment, String admin_id) {
		super();
		this.seq = seq;
		this.an_id = an_id;
		this.comment = comment;
		this.admin_id = admin_id;
	}

	private Integer seq;
	private String an_id;
	private String comment;
	private String admin_id;
	private String admin_nm;
	private String admin_type;
	private String reg_date;
	
	public Integer getSeq() {
		return seq;
	}
	public void setSeq(Integer seq) {
		this.seq = seq;
	}
	public String getAn_id() {
		return an_id;
	}
	public void setAn_id(String an_id) {
		this.an_id = an_id;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getAdmin_id() {
		return admin_id;
	}
	public void setAdmin_id(String admin_id) {
		this.admin_id = admin_id;
	}
	public String getAdmin_nm() {
		return admin_nm;
	}
	public void setAdmin_nm(String admin_nm) {
		this.admin_nm = admin_nm;
	}
	public String getAdmin_type() {
		return admin_type;
	}
	public void setAdmin_type(String admin_type) {
		this.admin_type = admin_type;
	}
	public String getReg_date() {
		return reg_date;
	}
	public void setReg_date(String reg_date) {
		this.reg_date = reg_date;
	}

}
