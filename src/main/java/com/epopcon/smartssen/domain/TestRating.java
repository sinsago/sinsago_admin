package com.epopcon.smartssen.domain;

public class TestRating {
	
	private String st_id;
	
	//승인 요청 건수
	private Integer count_a;
	//일정 준수 건수
	private Integer count_b;
	//일정 미준수 건수
	private Integer count_c;
	//일정 준수 + 미준수 합계
	private Integer count_d;
	//반려 건수
	private Integer count_e;
	
	private Integer keep_rate;
	private Integer approve_rate;
	
	private String next_day;
	private String apply_day;
	
	public String getSt_id() {
		return st_id;
	}
	public void setSt_id(String st_id) {
		this.st_id = st_id;
	}
	public Integer getCount_a() {
		return count_a;
	}
	public void setCount_a(Integer count_a) {
		this.count_a = count_a;
	}
	public Integer getCount_b() {
		return count_b;
	}
	public void setCount_b(Integer count_b) {
		this.count_b = count_b;
	}
	public Integer getCount_c() {
		return count_c;
	}
	public void setCount_c(Integer count_c) {
		this.count_c = count_c;
	}
	public Integer getCount_d() {
		return count_d;
	}
	public void setCount_d(Integer count_d) {
		this.count_d = count_d;
	}
	public Integer getCount_e() {
		return count_e;
	}
	public void setCount_e(Integer count_e) {
		this.count_e = count_e;
	}
	public Integer getKeep_rate() {
		return keep_rate;
	}
	public void setKeep_rate(Integer keep_rate) {
		this.keep_rate = keep_rate;
	}
	public Integer getApprove_rate() {
		return approve_rate;
	}
	public void setApprove_rate(Integer approve_rate) {
		this.approve_rate = approve_rate;
	}
	public String getNext_day() {
		return next_day;
	}
	public void setNext_day(String next_day) {
		this.next_day = next_day;
	}
	public String getApply_day() {
		return apply_day;
	}
	public void setApply_day(String apply_day) {
		this.apply_day = apply_day;
	}
}
