package com.epopcon.smartssen.domain;

public class TeacherTest {
	
	
	private String st_id;
	private String start_day;
	private String end_day;
	private String next_day;
	private String apply_day;
	
	
	
	public TeacherTest() {
		super();
	}
	
	public TeacherTest(String st_id, String start_day, String end_day) {
		super();
		this.st_id = st_id;
		this.start_day = start_day;
		this.end_day = end_day;
	}
	public String getNext_day() {
		return next_day;
	}
	public void setNext_day(String next_day) {
		this.next_day = next_day;
	}
	public String getApply_day() {
		return apply_day;
	}
	public void setApply_day(String apply_day) {
		this.apply_day = apply_day;
	}
	public String getSt_id() {
		return st_id;
	}
	public void setSt_id(String st_id) {
		this.st_id = st_id;
	}
	public String getStart_day() {
		return start_day;
	}
	public void setStart_day(String start_day) {
		this.start_day = start_day;
	}
	public String getEnd_day() {
		return end_day;
	}
	public void setEnd_day(String end_day) {
		this.end_day = end_day;
	}
	
}
