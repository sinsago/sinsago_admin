package com.epopcon.smartssen.domain;

public class Ticket {
	
	private Long tk_seq;
	private Long tk_id;
	private Long tk_punch;
	private String reg_date;
	
	public Long getTk_seq() {
		return tk_seq;
	}
	public void setTk_seq(Long tk_seq) {
		this.tk_seq = tk_seq;
	}
	public Long getTk_id() {
		return tk_id;
	}
	public void setTk_id(Long tk_id) {
		this.tk_id = tk_id;
	}
	public Long getTk_punch() {
		return tk_punch;
	}
	public void setTk_punch(Long tk_punch) {
		this.tk_punch = tk_punch;
	}
	public String getReg_date() {
		return reg_date;
	}
	public void setReg_date(String reg_date) {
		this.reg_date = reg_date;
	}
	
	@Override
	public String toString() {
		return "Ticket [tk_seq=" + tk_seq + ", tk_id=" + tk_id + ", tk_punch=" + tk_punch + ", reg_date=" + reg_date
				+ "]";
	}

}
