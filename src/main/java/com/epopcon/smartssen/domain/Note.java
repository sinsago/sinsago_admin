package com.epopcon.smartssen.domain;

public class Note {

	
	private String an_approve_date;
	private String an_deadline_date;
	private String an_id;
	private String an_img_path;
	private Integer an_page;
	private String an_request_date;
	private Integer an_state;
	
	private Long approve_cnt;
	private Long blocked_cnt;
	private String comment;
	
	private String file_note;
	
	private Integer is_keep_deadline;
	private Long pending_cnt;
	
	private String product_cd;
	private String product_nm;
	private String reg_date;
	private Integer rownum;
	private Long running_cnt;
	private String st_id;
	private String st_nm;
	
	private Integer student_grade;
	private String student_nm;
	
	private Integer student_seq;
	private Integer student_type;
	private Integer total;
	private String user_id;
	private Integer user_seq;
	
	private Long waiting_cnt;
	
	private String wr_img_path;
	
	private Integer tab;
	
	private Integer refuse_type;
	
	private Integer inspect_state;
	private String inspect_reason;
	
	public Note() {
		// TODO Auto-generated constructor stub
	}
	
	public String getInspect_reason() {
		return inspect_reason;
	}
	public void setInspect_reason(String inspect_reason) {
		this.inspect_reason = inspect_reason;
	}
	public Integer getInspect_state() {
		return inspect_state;
	}
	public void setInspect_state(Integer inspect_state) {
		this.inspect_state = inspect_state;
	}
	public Integer getTab() {
		return tab;
	}
	public void setTab(Integer tab) {
		this.tab = tab;
	}
	public String getAn_approve_date() {
		return an_approve_date;
	}
	public String getAn_deadline_date() {
		return an_deadline_date;
	}
	public String getAn_id() {
		return an_id;
	}
	public String getAn_img_path() {
		return an_img_path;
	}
	public Integer getAn_page() {
		return an_page;
	}
	public String getAn_request_date() {
		return an_request_date;
	}
	public Integer getAn_state() {
		return an_state;
	}
	public Long getApprove_cnt() {
		return approve_cnt;
	}
	public Long getBlocked_cnt() {
		return blocked_cnt;
	}
	public String getComment() {
		return comment;
	}
	public String getFile_note() {
		return file_note;
	}
	public Integer getIs_keep_deadline() {
		return is_keep_deadline;
	}
	public Long getPending_cnt() {
		return pending_cnt;
	}
	public String getProduct_cd() {
		return product_cd;
	}
	public String getProduct_nm() {
		return product_nm;
	}
	public String getReg_date() {
		return reg_date;
	}
	public Integer getRownum() {
		return rownum;
	}
	public Long getRunning_cnt() {
		return running_cnt;
	}
	public String getSt_id() {
		return st_id;
	}
	public String getSt_nm() {
		return st_nm;
	}
	public Integer getStudent_grade() {
		return student_grade;
	}
	public String getStudent_nm() {
		return student_nm;
	}
	public Integer getStudent_seq() {
		return student_seq;
	}
	public Integer getStudent_type() {
		return student_type;
	}
	public Integer getTotal() {
		return total;
	}
	public String getUser_id() {
		return user_id;
	}
	public Integer getUser_seq() {
		return user_seq;
	}
	public Long getWaiting_cnt() {
		return waiting_cnt;
	}
	public String getWr_img_path() {
		return wr_img_path;
	}
	public void setAn_approve_date(String an_approve_date) {
		this.an_approve_date = an_approve_date;
	}
	public void setAn_deadline_date(String an_deadline_date) {
		this.an_deadline_date = an_deadline_date;
	}
	public void setAn_id(String an_id) {
		this.an_id = an_id;
	}
	public void setAn_img_path(String an_img_path) {
		this.an_img_path = an_img_path;
	}
	public void setAn_page(Integer an_page) {
		this.an_page = an_page;
	}
	public void setAn_request_date(String an_request_date) {
		this.an_request_date = an_request_date;
	}
	public void setAn_state(Integer an_state) {
		this.an_state = an_state;
	}
	public void setApprove_cnt(Long approve_cnt) {
		this.approve_cnt = approve_cnt;
	}
	public void setBlocked_cnt(Long blocked_cnt) {
		this.blocked_cnt = blocked_cnt;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public void setFile_note(String file_note) {
		this.file_note = file_note;
	}
	public void setIs_keep_deadline(Integer is_keep_deadline) {
		this.is_keep_deadline = is_keep_deadline;
	}
	public void setPending_cnt(Long pending_cnt) {
		this.pending_cnt = pending_cnt;
	}
	public void setProduct_cd(String product_cd) {
		this.product_cd = product_cd;
	}
	public void setProduct_nm(String product_nm) {
		this.product_nm = product_nm;
	}
	public void setReg_date(String reg_date) {
		this.reg_date = reg_date;
	}
	public void setRownum(Integer rownum) {
		this.rownum = rownum;
	}
	public void setRunning_cnt(Long running_cnt) {
		this.running_cnt = running_cnt;
	}
	public void setSt_id(String st_id) {
		this.st_id = st_id;
	}
	public void setSt_nm(String st_nm) {
		this.st_nm = st_nm;
	}
	public void setStudent_grade(Integer student_grade) {
		this.student_grade = student_grade;
	}
	public void setStudent_nm(String student_nm) {
		this.student_nm = student_nm;
	}
	public void setStudent_seq(Integer student_seq) {
		this.student_seq = student_seq;
	}
	public void setStudent_type(Integer student_type) {
		this.student_type = student_type;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public void setUser_seq(Integer user_seq) {
		this.user_seq = user_seq;
	}
	public void setWaiting_cnt(Long waiting_cnt) {
		this.waiting_cnt = waiting_cnt;
	}
	public void setWr_img_path(String wr_img_path) {
		this.wr_img_path = wr_img_path;
	}
	
	@Override
	public String toString() {
		return "Note [an_id=" + an_id + ", an_page=" + an_page + ", an_state=" + an_state + ", product_cd=" + product_cd
				+ ", product_nm=" + product_nm + ", user_id=" + user_id + ", user_seq=" + user_seq + ", student_seq="
				+ student_seq + ", student_nm=" + student_nm + ", student_grade=" + student_grade + ", student_type="
				+ student_type + ", st_id=" + st_id + ", st_nm=" + st_nm + ", reg_date=" + reg_date
				+ ", an_approve_date=" + an_approve_date + ", an_deadline_date=" + an_deadline_date
				+ ", an_request_date=" + an_request_date + ", an_img_path=" + an_img_path + ", wr_img_path="
				+ wr_img_path + ", is_keep_deadline=" + is_keep_deadline + ", total=" + total + ", rownum=" + rownum
				+ ", waiting_cnt=" + waiting_cnt + ", blocked_cnt=" + blocked_cnt + ", running_cnt=" + running_cnt
				+ ", pending_cnt=" + pending_cnt + ", approve_cnt=" + approve_cnt + ", file_note=" + file_note + "]";
	}

	public Integer getRefuse_type() {
		return refuse_type;
	}

	public void setRefuse_type(Integer refuse_type) {
		this.refuse_type = refuse_type;
	}
	
}

