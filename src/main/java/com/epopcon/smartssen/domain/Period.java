package com.epopcon.smartssen.domain;

public class Period {
	
	private Long period_seq;
	private String st_id;
	private String period_date;
	private Integer period_fee;
	private String apply_date;
	private String reg_date;
	private Integer is_add_end;
	
	public Long getPeriod_seq() {
		return period_seq;
	}
	public void setPeriod_seq(Long period_seq) {
		this.period_seq = period_seq;
	}
	public String getSt_id() {
		return st_id;
	}
	public void setSt_id(String st_id) {
		this.st_id = st_id;
	}
	public String getPeriod_date() {
		return period_date;
	}
	public void setPeriod_date(String period_date) {
		this.period_date = period_date;
	}
	public Integer getPeriod_fee() {
		return period_fee;
	}
	public void setPeriod_fee(Integer period_fee) {
		this.period_fee = period_fee;
	}
	public String getApply_date() {
		return apply_date;
	}
	public void setApply_date(String apply_date) {
		this.apply_date = apply_date;
	}
	public String getReg_date() {
		return reg_date;
	}
	public void setReg_date(String reg_date) {
		this.reg_date = reg_date;
	}
	public Integer getIs_add_end() {
		return is_add_end;
	}
	public void setIs_add_end(Integer is_add_end) {
		this.is_add_end = is_add_end;
	}
	
	@Override
	public String toString() {
		return "Period [period_seq=" + period_seq + ", st_id=" + st_id + ", period_date=" + period_date
				+ ", period_fee=" + period_fee + ", apply_date=" + apply_date + ", reg_date=" + reg_date
				+ ", is_add_end=" + is_add_end + "]";
	}
}
