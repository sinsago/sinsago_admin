package com.epopcon.smartssen.domain;

public class NoteStatistics {
	
	public NoteStatistics() {}
	
	private String stId;
	private String startDate;
	private String endDate;
	private Integer deadlineHour;
	
	private Integer countA;
	private Integer countB;
	private Integer countC;
	private Integer countD;
	private Integer countE;
	
	private Float percentageA;
	private Float percentageB;
	
	public String getStId() {
		return stId;
	}
	public void setStId(String stId) {
		this.stId = stId;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public Integer getDeadlineHour() {
		return deadlineHour;
	}
	public void setDeadlineHour(Integer deadlineHour) {
		this.deadlineHour = deadlineHour;
	}
	
	
	
	
	public Integer getCountA() {
		return countA;
	}
	public void setCountA(Integer countA) {
		this.countA = countA;
	}
	public Integer getCountB() {
		return countB;
	}
	public void setCountB(Integer countB) {
		this.countB = countB;
	}
	public Integer getCountC() {
		return countC;
	}
	public void setCountC(Integer countC) {
		this.countC = countC;
	}
	public Integer getCountD() {
		return countD;
	}
	public void setCountD(Integer countD) {
		this.countD = countD;
	}
	public Integer getCountE() {
		return countE;
	}
	public void setCountE(Integer countE) {
		this.countE = countE;
	}
	public Float getPercentageA() {
		return percentageA;
	}
	public void setPercentageA(Float percentageA) {
		this.percentageA = percentageA;
	}
	public Float getPercentageB() {
		return percentageB;
	}
	public void setPercentageB(Float percentageB) {
		this.percentageB = percentageB;
	}
	public NoteStatistics(String stId, String startDate, String endDate, Integer deadlineHour) {
		super();
		this.stId = stId;
		this.startDate = startDate;
		this.endDate = endDate;
		this.deadlineHour = deadlineHour;
	}
	
	
	
	
	
	

}
