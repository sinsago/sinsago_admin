package com.epopcon.smartssen.domain;

public class App {
	
	public App() {}

	private Long app_seq;
	private String app_version;
	private String app_desc;

	private String app_path;
	private Integer is_active;
	private String mngr_id;
	private String reg_date;
	private String mod_date;
	
	private Integer total;
	private Integer rownum;
	
	public Long getApp_seq() {
		return app_seq;
	}
	public void setApp_seq(Long app_seq) {
		this.app_seq = app_seq;
	}
	public String getApp_version() {
		return app_version;
	}
	public void setApp_version(String app_version) {
		this.app_version = app_version;
	}
	public String getApp_desc() {
		return app_desc;
	}
	public void setApp_desc(String app_desc) {
		this.app_desc = app_desc;
	}
	public String getApp_path() {
		return app_path;
	}
	public void setApp_path(String app_path) {
		this.app_path = app_path;
	}
	public Integer getIs_active() {
		return is_active;
	}
	public void setIs_active(Integer is_active) {
		this.is_active = is_active;
	}
	public String getMngr_id() {
		return mngr_id;
	}
	public void setMngr_id(String mngr_id) {
		this.mngr_id = mngr_id;
	}
	public String getReg_date() {
		return reg_date;
	}
	public void setReg_date(String reg_date) {
		this.reg_date = reg_date;
	}
	public String getMod_date() {
		return mod_date;
	}
	public void setMod_date(String mod_date) {
		this.mod_date = mod_date;
	}
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	public Integer getRownum() {
		return rownum;
	}
	public void setRownum(Integer rownum) {
		this.rownum = rownum;
	}
	@Override
	public String toString() {
		return "App [app_version=" + app_version + ", app_desc=" + app_desc + ", app_path=" + app_path + ", is_active="
				+ is_active + ", mngr_id=" + mngr_id + ", reg_date=" + reg_date + ", mod_date=" + mod_date + ", total="
				+ total + ", rownum=" + rownum + "]";
	}
	
	
	
}
