package com.epopcon.smartssen.domain;

public class GcmResponse {

	
	public Integer multicast_id;
    public Integer success;
    public Integer failure;
    public Integer canonical_ids;
    
	public Integer getMulticast_id() {
		return multicast_id;
	}
	public void setMulticast_id(Integer multicast_id) {
		this.multicast_id = multicast_id;
	}
	public Integer getSuccess() {
		return success;
	}
	public void setSuccess(Integer success) {
		this.success = success;
	}
	public Integer getFailure() {
		return failure;
	}
	public void setFailure(Integer failure) {
		this.failure = failure;
	}
	public Integer getCanonical_ids() {
		return canonical_ids;
	}
	public void setCanonical_ids(Integer canonical_ids) {
		this.canonical_ids = canonical_ids;
	}
	
	@Override
	public String toString() {
		return "GcmResponse [multicast_id=" + multicast_id + ", success=" + success + ", failure=" + failure
				+ ", canonical_ids=" + canonical_ids + "]";
	}
	
}
