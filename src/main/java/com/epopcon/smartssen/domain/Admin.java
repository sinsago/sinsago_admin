package com.epopcon.smartssen.domain;

import java.util.List;

public class Admin {
	
	private String admin_id;
	private String admin_pwd;
	private String admin_nm;
	private String admin_type;
	private String reg_date;
	private String mod_date;
	
	private Integer total;
	private Integer rownum;
	
	private List<GradeCharge> grade_charge;
	
	private Long managed_cnt;
	
	public String getAdmin_id() {
		return admin_id;
	}
	public void setAdmin_id(String admin_id) {
		this.admin_id = admin_id;
	}
	public String getAdmin_pwd() {
		return admin_pwd;
	}
	public void setAdmin_pwd(String admin_pwd) {
		this.admin_pwd = admin_pwd;
	}
	public String getAdmin_nm() {
		return admin_nm;
	}
	public void setAdmin_nm(String admin_nm) {
		this.admin_nm = admin_nm;
	}
	public String getAdmin_type() {
		return admin_type;
	}
	public void setAdmin_type(String admin_type) {
		this.admin_type = admin_type;
	}
	public String getReg_date() {
		return reg_date;
	}
	public void setReg_date(String reg_date) {
		this.reg_date = reg_date;
	}
	public String getMod_date() {
		return mod_date;
	}
	public void setMod_date(String mod_date) {
		this.mod_date = mod_date;
	}
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	public Integer getRownum() {
		return rownum;
	}
	public void setRownum(Integer rownum) {
		this.rownum = rownum;
	}
	public Long getManaged_cnt() {
		return managed_cnt;
	}
	public void setManaged_cnt(Long managed_cnt) {
		this.managed_cnt = managed_cnt;
	}
	public List<GradeCharge> getGrade_charge() {
		return grade_charge;
	}
	public void setGrade_charge(List<GradeCharge> grade_charge) {
		this.grade_charge = grade_charge;
	}
	@Override
	public String toString() {
		return "Admin [admin_id=" + admin_id + ", admin_pwd=" + admin_pwd + ", admin_nm=" + admin_nm + ", admin_type="
				+ admin_type + ", reg_date=" + reg_date + ", mod_date=" + mod_date + ", total=" + total + ", rownum="
				+ rownum + ", managed_cnt=" + managed_cnt + "]";
	}
}
