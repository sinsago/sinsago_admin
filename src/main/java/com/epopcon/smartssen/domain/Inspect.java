package com.epopcon.smartssen.domain;

public class Inspect {
	
	private Integer seq;
	
	private String an_id;
	private String admin_id;
	private String admin_nm;
	private String inspect_reason;
	
	private Integer inspect_state;
	private Integer inspect_type;
	private Integer inspect_rate;
	private Integer inspect_fee;
	
	private Integer student_type;
	private Integer student_grade;
	private Integer note_count;
	
	private String reg_date;
	
	public Inspect() {
		// TODO Auto-generated constructor stub
	}

	public Inspect(Integer inspect_rate, Integer inspect_state, Integer student_type, Integer student_grade) {
		super();
		this.inspect_rate = inspect_rate;
		this.inspect_state = inspect_state;
		this.student_type = student_type;
		this.student_grade = student_grade;
	}
	
	public String getAdmin_nm() {
		return admin_nm;
	}

	public void setAdmin_nm(String admin_nm) {
		this.admin_nm = admin_nm;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public String getAn_id() {
		return an_id;
	}

	public void setAn_id(String an_id) {
		this.an_id = an_id;
	}

	public String getAdmin_id() {
		return admin_id;
	}

	public void setAdmin_id(String admin_id) {
		this.admin_id = admin_id;
	}

	public String getInspect_reason() {
		return inspect_reason;
	}

	public void setInspect_reason(String inspect_reason) {
		this.inspect_reason = inspect_reason;
	}

	public Integer getInspect_state() {
		return inspect_state;
	}

	public void setInspect_state(Integer inspect_state) {
		this.inspect_state = inspect_state;
	}

	public Integer getInspect_type() {
		return inspect_type;
	}

	public void setInspect_type(Integer inspect_type) {
		this.inspect_type = inspect_type;
	}

	public Integer getInspect_rate() {
		return inspect_rate;
	}

	public void setInspect_rate(Integer inspect_rate) {
		this.inspect_rate = inspect_rate;
	}

	public Integer getInspect_fee() {
		return inspect_fee;
	}

	public void setInspect_fee(Integer inspect_fee) {
		this.inspect_fee = inspect_fee;
	}

	public String getReg_date() {
		return reg_date;
	}

	public void setReg_date(String reg_date) {
		this.reg_date = reg_date;
	}

	public Integer getStudent_type() {
		return student_type;
	}

	public void setStudent_type(Integer student_type) {
		this.student_type = student_type;
	}

	public Integer getStudent_grade() {
		return student_grade;
	}

	public void setStudent_grade(Integer student_grade) {
		this.student_grade = student_grade;
	}

	public Integer getNote_count() {
		return note_count;
	}

	public void setNote_count(Integer note_count) {
		this.note_count = note_count;
	}

	@Override
	public String toString() {
		return "Inspect [student_type=" + student_type + ", student_grade=" + student_grade + ", note_count="
				+ note_count + "]";
	}

	

	
}
