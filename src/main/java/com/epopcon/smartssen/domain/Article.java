package com.epopcon.smartssen.domain;

import java.io.Serializable;

public class Article implements Serializable {
    
	
	private Comment result;

	public Comment getResult() {
		return result;
	}

	public void setResult(Comment result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return "Article [result=" + result + "]";
	}
	

	
	
	
}

