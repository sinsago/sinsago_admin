package com.epopcon.smartssen.domain;

public class InspectFee {
	
	private Integer total;
	private Integer rownum;
	
	private String admin_id;
	private String admin_nm;
	
	private String reg_date;
	
	private Integer approve_cnt;
	private Integer calc_fee;
	private Integer calc_amount;
	
	public InspectFee() {}
	
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	public Integer getRownum() {
		return rownum;
	}
	public void setRownum(Integer rownum) {
		this.rownum = rownum;
	}
	public String getAdmin_id() {
		return admin_id;
	}
	public void setAdmin_id(String admin_id) {
		this.admin_id = admin_id;
	}
	public String getAdmin_nm() {
		return admin_nm;
	}
	public void setAdmin_nm(String admin_nm) {
		this.admin_nm = admin_nm;
	}
	public String getReg_date() {
		return reg_date;
	}
	public void setReg_date(String reg_date) {
		this.reg_date = reg_date;
	}
	public Integer getApprove_cnt() {
		return approve_cnt;
	}
	public void setApprove_cnt(Integer approve_cnt) {
		this.approve_cnt = approve_cnt;
	}
	public Integer getCalc_fee() {
		return calc_fee;
	}
	public void setCalc_fee(Integer calc_fee) {
		this.calc_fee = calc_fee;
	}
	public Integer getCalc_amount() {
		return calc_amount;
	}
	public void setCalc_amount(Integer calc_amount) {
		this.calc_amount = calc_amount;
	}
	
	
	
	

}
