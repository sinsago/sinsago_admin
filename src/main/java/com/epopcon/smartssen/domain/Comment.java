package com.epopcon.smartssen.domain;

import java.io.Serializable;

/**
 * Created by saltfactory<saltfactory@gmail.com> on 11/21/15.
 */
public class Comment implements Serializable {
    
	private String code;
	private String message;
	private String server_date;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getServer_date() {
		return server_date;
	}
	public void setServer_date(String server_date) {
		this.server_date = server_date;
	}
	
	@Override
	public String toString() {
		return "Comment [code=" + code + ", message=" + message + ", server_date=" + server_date + "]";
	}
	
	
	
}
