package com.epopcon.smartssen.domain;

public class GradeCharge {
	
	private Integer seq;
	private String admin_id;
	private Integer student_grade;
	private Integer student_type;
	private String grade_charge;
	private String reg_date;
	
	public GradeCharge() {
		// TODO Auto-generated constructor stub
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public String getAdmin_id() {
		return admin_id;
	}

	public void setAdmin_id(String admin_id) {
		this.admin_id = admin_id;
	}

	public Integer getStudent_grade() {
		return student_grade;
	}

	public void setStudent_grade(Integer student_grade) {
		this.student_grade = student_grade;
	}

	public Integer getStudent_type() {
		return student_type;
	}

	public void setStudent_type(Integer student_type) {
		this.student_type = student_type;
	}

	public String getReg_date() {
		return reg_date;
	}

	public void setReg_date(String reg_date) {
		this.reg_date = reg_date;
	}
	
	public String getGrade_charge() {
		return grade_charge;
	}
	public void setGrade_charge(String grade_charge) {
		this.grade_charge = grade_charge;
	}

	public GradeCharge(String admin_id) {
		super();
		this.admin_id = admin_id;
	}

	@Override
	public String toString() {
		return "GradeCharge [seq=" + seq + ", admin_id=" + admin_id + ", student_grade=" + student_grade
				+ ", student_type=" + student_type + ", grade_charge=" + grade_charge + ", reg_date=" + reg_date + "]";
	}

	
	
}
