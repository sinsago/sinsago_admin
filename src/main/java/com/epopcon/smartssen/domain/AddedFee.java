package com.epopcon.smartssen.domain;

public class AddedFee {
	
	private Integer seq;
	private String st_id;
	private String based_date;
	private String apply_date;
	private Integer fee;
	private String comment;
	private String is_use;
	private Boolean is_recalculation;
	private Integer recalculation_day;
	
	public Integer getSeq() {
		return seq;
	}
	public void setSeq(Integer seq) {
		this.seq = seq;
	}
	public String getIs_use() {
		return is_use;
	}
	public void setIs_use(String is_use) {
		this.is_use = is_use;
	}
	public String getSt_id() {
		return st_id;
	}
	public void setSt_id(String st_id) {
		this.st_id = st_id;
	}
	public String getBased_date() {
		return based_date;
	}
	public void setBased_date(String based_date) {
		this.based_date = based_date;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public Integer getFee() {
		return fee;
	}
	public void setFee(Integer fee) {
		this.fee = fee;
	}
	public String getApply_date() {
		return apply_date;
	}
	public void setApply_date(String apply_date) {
		this.apply_date = apply_date;
	}
	public Boolean getIs_recalculation() {
		return is_recalculation;
	}
	public void setIs_recalculation(Boolean is_recalculation) {
		this.is_recalculation = is_recalculation;
	}
	public Integer getRecalculation_day() {
		return recalculation_day;
	}
	public void setRecalculation_day(Integer recalculation_day) {
		this.recalculation_day = recalculation_day;
	}
	
	@Override
	public String toString() {
		return "AddedFee [seq=" + seq + ", st_id=" + st_id + ", based_date=" + based_date + ", apply_date=" + apply_date
				+ ", fee=" + fee + ", comment=" + comment + ", is_use=" + is_use + ", is_recalculation="
				+ is_recalculation + ", recalculation_day=" + recalculation_day + "]";
	}

}
