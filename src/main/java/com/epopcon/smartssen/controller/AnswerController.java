package com.epopcon.smartssen.controller;

import java.text.ParseException;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.epopcon.smartssen.domain.Admin;
import com.epopcon.smartssen.domain.Note;
import com.epopcon.smartssen.domain.NoteLog;
import com.epopcon.smartssen.domain.Refuse;
import com.epopcon.smartssen.mapper.NoteMapper;
import com.epopcon.smartssen.service.AnswerNoteService;
import com.epopcon.smartssen.service.ExcelBuilder;
import com.epopcon.smartssen.service.LoginUserDetails;
import com.epopcon.smartssen.service.LoginUserDetailsService;
import com.google.common.base.Stopwatch;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

@Controller
public class AnswerController {


    private static final Logger logger = LoggerFactory.getLogger(AnswerController.class);

    @Autowired
    private NoteMapper noteMapper;

    @Autowired
    private AnswerNoteService noteService;

    @Autowired
    private LoginUserDetailsService details;

    @Value("${properties.note.state.waiting}")
    private Integer NOTE_STATE_WAITING;

    @Value("${properties.note.state.running}")
    private Integer NOTE_STATE_RUNNING;

    @Value("${properties.note.state.pending}")
    private Integer NOTE_STATE_PENDING;

    @Value("${properties.note.state.blocked}")
    private Integer NOTE_STATE_BLOCKED;

    @Value("${properties.note.state.approve}")
    private Integer NOTE_STATE_APPROVE;

    @Value("${properties.answer.excel-filename}")
    private String EXCEL_FILENAME;


    @Value("${properties.note-url.base}")
    private String NOTE_BASE_URL;

    @RequestMapping("/answer")
    public String answer() {
        return "/answer/list";
    }

    @RequestMapping("/answer/search")
    public @ResponseBody Map<String, Object> search(

    		@RequestParam(value = "dateType", required = false) Integer dateType
            , @RequestParam(value = "searchStart", required = false) String searchStart
            , @RequestParam(value = "searchEnd", required = false) String searchEnd

            , @RequestParam(value = "studentType", required = false) Integer studentType
            , @RequestParam(value = "gradeType", required = false) Integer gradeType
            
            , @RequestParam(value = "searchType", required = false) Integer searchType
            , @RequestParam(value = "searchWord", required = false) String searchWord

            , @RequestParam(value = "page", required = false, defaultValue = "0") Integer page
            , @RequestParam(value = "size", required = false, defaultValue = "10") Integer size
            , @RequestParam(value = "state") Integer state) throws Exception {
    	
        Stopwatch stopwatch = Stopwatch.createStarted();

        Integer startCnt = ((page + 1) - 1) * size + 1;
        Integer endCnt = (page + 1) * size;

        logger.info("=======================================");
        logger.info("* search 노트상태[{}] 검색타입[{}] 검색어[{}]", state, searchType, searchWord);
        logger.info("* date type [{}] start [{}] end[{}]", dateType, searchStart, searchEnd);
        logger.info("* student type [{}] grade [{}]", studentType, gradeType);
        logger.info("* paging page[{}] 시작[{}] 종료[{}]", page, startCnt, endCnt);
        logger.info("=======================================");

        Admin admin = details.getUserInfo();
        logger.info("session Id[{}] Name[{}] Type[{}]", admin.getAdmin_id(), admin.getAdmin_nm(), admin.getAdmin_type());

        String mngrId = "";
        if(!admin.getAdmin_type().equals("10")) {
            mngrId = admin.getAdmin_id();
        }

        List<Note> list = Lists.newArrayList();
        list = noteMapper.findByNotes(
        		state
        		, searchType
        		, searchWord
        		, searchStart
        		, searchEnd
        		, startCnt
        		, endCnt
        		, mngrId
        		, dateType
        		, studentType
        		, gradeType);

        Map<String, Object> result = Maps.newLinkedHashMap();
        result.put("recordsTotal", list.size() > 0 ? list.get(0).getTotal() : 0);
        result.put("recordsFiltered", list.size() > 0 ? list.get(0).getTotal() : 0);
        result.put("data", list);

        logger.info("* notes {}", list.toString());

        Map<String, Long> tabs = Maps.newHashMap();
        tabs = noteMapper.countByNoteState(
        		state
        		, searchType
        		, searchWord
        		, searchStart
        		, searchEnd
        		, startCnt
        		, endCnt
        		, mngrId
        		, dateType
        		, studentType
        		, gradeType);
        result.put("tabs", tabs);

        stopwatch.stop();
        logger.info(">>> elapsed time  [{}]", stopwatch);

        return result;
    }

    @RequestMapping("/answer/form")
    public String form(
            Locale locale
            , Model model
            , HttpSession session
            , @RequestParam(value = "anId") String anId) {

        logger.info("=======================================");
        logger.info("* form anId[{}]", anId);
        logger.info("=======================================");

        Note nInfo = noteMapper.findByAnId(anId);

        logger.info("* nInfo {}", nInfo.toString());

        model.addAttribute("nInfo", nInfo);

        String path = "";
        if (nInfo.getAn_state() < 1) {
            path = NOTE_BASE_URL + nInfo.getAn_img_path();
        } else {
            path = NOTE_BASE_URL + nInfo.getWr_img_path();

            if (StringUtils.isBlank(path)) {
                path = NOTE_BASE_URL;
            }
        }

        model.addAttribute("nImage", path);

        return "/answer/form";
    }

    @RequestMapping("/answer/detail")
    public @ResponseBody Map<String, Object> findByNoteDetail(
    		@RequestParam("anId") String anId) throws Exception {
    	
    	Stopwatch stopwatch = Stopwatch.createStarted();
    	
    	logger.info("=======================================");
    	logger.info("* findByNoteDetail anId[{}] ", anId);
    	logger.info("=======================================");
    	
    	Note note = noteMapper.findByAnId(anId);

        logger.info("* note {}", note.toString());

        String path = "";
        if (note.getAn_state() < 1) {
            path = NOTE_BASE_URL + note.getAn_img_path();
        } else {
            path = NOTE_BASE_URL + note.getWr_img_path();

            if (StringUtils.isBlank(path)) {
                path = NOTE_BASE_URL;
            }
        }
        
        //check 반려사유
        List<Refuse> refuse = noteMapper.findByNoteRefuse(new Refuse(anId));
        logger.info(">>> Note Refuse count[{}]", refuse.size());

        Map<String, Object> result = Maps.newLinkedHashMap();
		result.put("note", note);
		result.put("path", path);
		result.put("refuse", refuse);
		
		stopwatch.stop();
		logger.info(">>> elapsed time  [{}]", stopwatch);
    	
    	return result;
    	
    }
    
    @RequestMapping("/answer/logs")
    public @ResponseBody List<NoteLog> findByNoteLogs(String anId) throws Exception {

        logger.info("=======================================");
        logger.info("* findByNoteLogs anId[{}] ", anId);
        logger.info("=======================================");

        List<NoteLog> logs = Lists.newArrayList();
        logs = noteMapper.findByNoteLogs(anId);

        logger.info("* logs {}", logs.toString());

        return logs;

    }

    @RequestMapping("/answer/save")
    public @ResponseBody Note save(Note note) throws Exception {

        logger.info("=======================================");
        logger.info("* save note[{}] ", note.toString());
        logger.info("=======================================");
        
        
        //update note
        noteService.updateStateByNote(note);

        if(note.getTab() == 2 && StringUtils.isNotBlank(note.getComment())) {
        	
        	logger.info("* Note Refuse [{}]", note.getAn_id());
        	
        	LoginUserDetails user = (LoginUserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    		Admin admin = user.getUser();
    		
        	Integer count = noteMapper.insertRefuse(new Refuse(null, note.getAn_id(), note.getComment(), admin.getAdmin_id()));
        	
        	//10: 사유등록 
    		noteMapper.insertRefuseLog(note.getAn_id(), admin.getAdmin_id(), 10);
        	
        	logger.info("* Note Refuse add count [{}]", count);
        }
        return note;
        
    }

    @RequestMapping("/answer/approve")
    public @ResponseBody Long approve(
            @RequestParam(value = "anIds[]") String[] anIds) throws Exception {

        logger.info("* approve ids [{}]", anIds.toString());

        return noteService.updateStateByIds(anIds, NOTE_STATE_APPROVE);

    }
    
    @RequestMapping("/answer/change")
    public @ResponseBody Long change(
    		@RequestParam(value = "anIds[]") String[] anIds
    		, @RequestParam(value = "stId") String stId) throws Exception {
    	
    	logger.info(">>> change teacher [{}] note ids [{}]", stId, anIds.toString());
    	
    	return noteService.changeTeacher(anIds, NOTE_STATE_WAITING, stId);
    	
    }

    @RequestMapping(value = "/answer/excel/download", method = RequestMethod.GET)
    public ModelAndView excelDownload(
    		
    		HttpServletRequest request
			, HttpServletResponse response
			
    		, @RequestParam(value = "dateType", required = false) Integer dateType
            , @RequestParam(value = "searchStart", required = false) String searchStart
            , @RequestParam(value = "searchEnd", required = false) String searchEnd
            , @RequestParam(value = "studentType", required = false) Integer studentType
            , @RequestParam(value = "gradeType", required = false) Integer gradeType
            , @RequestParam(value = "searchType", required = false) Integer searchType
            , @RequestParam(value = "searchWord", required = false) String searchWord
            , @RequestParam(value = "page", required = false, defaultValue = "0") Integer page
            , @RequestParam(value = "size", required = false, defaultValue = "10") Integer size
            , @RequestParam(value = "state") Integer state) throws ParseException {

        logger.info("=======================================");
        logger.info("* search  검색타입[{}] 검색어[{}]", searchType, searchWord);
        logger.info("* date start [{}] end[{}]", searchStart, searchEnd);
        logger.info("=======================================");
        
        Stopwatch stopwatch = Stopwatch.createStarted();
        
//		Integer startCnt = ((page + 1) - 1) * size + 1;
//		Integer endCnt = (page + 1) * size;
        
		Integer startCnt = 0;
		Integer endCnt = 0;

        logger.info("=======================================");
        logger.info("* search 노트상태[{}] 검색타입[{}] 검색어[{}]", state, searchType, searchWord);
        logger.info("* date type [{}] start [{}] end[{}]", dateType, searchStart, searchEnd);
        logger.info("* student type [{}] grade [{}]", studentType, gradeType);
        logger.info("* paging page[{}] 시작[{}] 종료[{}]", page, startCnt, endCnt);
        logger.info("=======================================");

        Admin admin = details.getUserInfo();
        logger.info("session Id[{}] Name[{}] Type[{}]", admin.getAdmin_id(), admin.getAdmin_nm(), admin.getAdmin_type());

        String mngrId = "";
        if(!admin.getAdmin_type().equals("10")) {
            mngrId = admin.getAdmin_id();
        }

        List<Note> list = Lists.newArrayList();
        list = noteMapper.findByNotes(
        		state
        		, searchType
        		, searchWord
        		, searchStart
        		, searchEnd
        		, startCnt
        		, endCnt
        		, mngrId
        		, dateType
        		, studentType
        		, gradeType);
        
        Map<String, Object> model = Maps.newHashMap();
        
        //Sheet name
		model.put("sheetname", "첨삭리스트");
		
		//Headers List
		List<String> headers = Lists.newArrayList();
		headers.add("No.");
		headers.add("답안ID");
		headers.add("학년");
		headers.add("교재명");
		headers.add("페이지");
		headers.add("아이디");
		headers.add("학습자");
		
		headers.add("첨삭요청일");
		switch (state) {
			case 0:
				headers.add("첨삭목표일");
				break;
			case 1:
				headers.add("첨삭목표일");
				break;
			case 2:
				headers.add("승인요청일");
				break;
			case 3:
				headers.add("승인요청일");
				break;
			case 4:
				headers.add("승인요청일");
				headers.add("승인완료일");
				break;
	
			default:
				break;
		}
		headers.add("담당교사");
		model.put("headers", headers);
		
		//Results Table
		model.put("results", resultsHandler(state, list));
		
		response.setContentType("application/Msexcel");
		response.setHeader( "Content-disposition", "attachment; filename="+EXCEL_FILENAME);
		
		stopwatch.stop();
        logger.info(">>> elapsed time  [{}]", stopwatch);
		
		return new ModelAndView(new ExcelBuilder(), model);
        
    }

    private List<List<Object>> resultsHandler(Integer state, List<Note> data) throws ParseException {

		List<List<Object>> results = Lists.newArrayList();
		for(Note note : data){
			
			List<Object> columns = Lists.newArrayList();
			
			columns.add(String.valueOf(note.getRownum()));
			columns.add(note.getAn_id());
			
			switch (note.getStudent_type()) {
				case 10:
					columns.add("초등 " + note.getStudent_grade());
					break;
				case 20:
					columns.add("중등 " + note.getStudent_grade());
					break;
				case 30:
					columns.add("고등 " + note.getStudent_grade());
					break;
				default:
					break;
			}
			
			columns.add(note.getProduct_nm());
			columns.add(note.getAn_page());
			columns.add(note.getUser_id());
			columns.add(note.getStudent_nm());
			
			columns.add(getDate(note.getReg_date()));
			
			switch (state) {
				case 0:
					columns.add(getDate(note.getAn_deadline_date()));
					break;
				case 1:
					columns.add(getDate(note.getAn_deadline_date()));
					break;
				case 2:
					columns.add(getDate(note.getAn_request_date()));
					break;
				case 3:
					columns.add(getDate(note.getAn_request_date()));
					break;
				case 4:
					columns.add(getDate(note.getAn_request_date()));
					columns.add(getDate(note.getAn_approve_date()));
					break;
		
				default:
					break;
			}
			columns.add(note.getSt_nm());
			
			results.add(columns);
		}
		
		return results;
	}

	private String getDate(String date) throws ParseException {
		if(StringUtils.isBlank(date)){
			return "-";
		}else{
			return DateFormatUtils.format(DateUtils.parseDate(date, new String[]{"yyyy-MM-dd HH:mm:ss.sss"}), "yyyy-MM-dd HH:mm");
		}
	}
	
	@RequestMapping("/answer/refuse/remove")
    public @ResponseBody Map<String, Object> refuseRemove(
    		@RequestParam(value = "seq", required = true) Integer seq
    		, @RequestParam(value = "anId", required = true) String anId) {
    	
		LoginUserDetails user = (LoginUserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Admin admin = user.getUser();
		
    	logger.info("* Note Refuse [{}]", seq);
    	
    	Integer count = noteMapper.deleteRefuse(new Refuse(seq, "", "", ""));
    	
    	//30: 사유삭제 
		noteMapper.insertRefuseLog(anId, admin.getAdmin_id(), 30);
    	
    	Map<String, Object> result = Maps.newLinkedHashMap();
    	result.put("data", count);
    	
    	return result;
    	
    }
    
    @RequestMapping("/answer/refuse/add")
    public @ResponseBody Map<String, Object> refuseAdd(
    		@RequestParam(value = "anId", required = true) String anId
    		, @RequestParam(value = "comment", required = true) String comment) {

    	LoginUserDetails user = (LoginUserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Admin admin = user.getUser();
		
    	logger.info("* Note Refuse [{}]", anId);
    	
    	Integer count = noteMapper.insertRefuse(new Refuse(null, anId, comment, admin.getAdmin_id()));
    
    	//10: 사유등록 
		noteMapper.insertRefuseLog(anId, admin.getAdmin_id(), 10);
    	
    	Map<String, Object> result = Maps.newLinkedHashMap();
		result.put("data", count);
    	
    	return result;
    	
    }
    
    @RequestMapping("/answer/refuse/modify")
    public @ResponseBody Map<String, Object> refuseModify(
    		@RequestParam(value = "seq", required = true) Integer seq
    		,@RequestParam(value = "comment", required = true) String comment
    		, @RequestParam(value = "anId", required = true) String anId) {
    	
    	LoginUserDetails user = (LoginUserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Admin admin = user.getUser();
		
    	logger.info("* Note Refuse modify[{}] admin[{}]", seq, admin.getAdmin_id());
    	
    	Integer count = noteMapper.updateRefuse(new Refuse(seq, "", comment, admin.getAdmin_id()));
    	
    	//20: 사유수정 
    	noteMapper.insertRefuseLog(anId, admin.getAdmin_id(), 20);
    	
    	Map<String, Object> result = Maps.newLinkedHashMap();
    	result.put("data", count);
    	
    	return result;
    	
    }
}
