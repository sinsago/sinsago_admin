package com.epopcon.smartssen.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.epopcon.smartssen.domain.Admin;
import com.epopcon.smartssen.domain.App;
import com.epopcon.smartssen.mapper.ManagerMapper;
import com.epopcon.smartssen.service.FileService;
import com.epopcon.smartssen.service.LoginUserDetails;
import com.google.common.base.Stopwatch;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

@Controller
public class AppController {

	private static final Logger logger = LoggerFactory.getLogger(ManagerController.class);
	
	@Autowired
	private FileService fileService;
	
	@Value("${properties.app.base}")
	private String APP_BASE_PATH;
	
	@Autowired
	private ManagerMapper managerMapper;
	
	@RequestMapping("/app")
	public String app(){
		return "/app/list";
	}
	
	@RequestMapping("/app/search")
	public @ResponseBody Map<String, Object> search(
			@RequestParam(value="page", required=false, defaultValue="0") Integer page
			, @RequestParam(value="size", required=false, defaultValue="10") Integer size) {
		
		
		Stopwatch stopwatch = Stopwatch.createStarted();
		
		Integer startCnt = ((page+1) - 1) * size + 1;
		Integer endCnt = (page+1) * size;
		
		logger.info("=======================================");
		logger.info("* search app paging page[{}] start[{}] end[{}]", page, startCnt, endCnt);
		logger.info("=======================================");
		
		List<App> list = Lists.newArrayList();
		list = managerMapper.findByAppliction(startCnt, endCnt);
		
		Map<String, Object> result = Maps.newLinkedHashMap();
		result.put("recordsTotal", list.size() > 0 ? list.get(0).getTotal() : 0);
		result.put("recordsFiltered", list.size() > 0 ? list.get(0).getTotal() : 0);
		result.put("data", list);
		
		stopwatch.stop();
		
		logger.info(">>> elapsed time  [{}]", stopwatch);
		
		return result;
	}
	
	@RequestMapping(value="/app/save", method = RequestMethod.POST)
	public @ResponseBody Long save(
			@RequestParam(value = "app_version", required = true) String version
			, @RequestParam(value = "app_path", required = true) MultipartFile file) throws Exception {
		
		logger.info("=======================================");
		logger.info("* save app info[{}] ", version.toString());
		logger.info("=======================================");
		
		LoginUserDetails user = (LoginUserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Admin admin = user.getUser();
		
		String path = "";
		
		path = fileService.storeByApplication(file);
		
		App app = new App();
		app.setApp_version(version);
		app.setApp_path(path);
		app.setIs_active(1);
		app.setMngr_id(admin.getAdmin_id());
		
		//all inactive
		managerMapper.updateByApplictionInactive();

		return managerMapper.createByAppliction(app);
	}
	
	
	@RequestMapping("/app/remove")
	public @ResponseBody Long remove(
			@RequestParam Long seq) throws Exception {
		
		logger.info("=======================================");
		logger.info("* remove seq[{}] ", seq);
		logger.info("=======================================");
		

		return managerMapper.deleteByAppliction(seq);
		
	}
	
	@RequestMapping(value = "/apk/{seq}", method = RequestMethod.GET, produces = "application/vnd.android.package-archive")
	public ResponseEntity<InputStreamResource> download(@PathVariable("seq") Long seq) throws IOException {

		logger.info("=======================================");
		logger.info("* download apk seq [{}]", seq);
		logger.info("=======================================");

		String path = managerMapper.findAppPathBySeq(seq);

		String resource = APP_BASE_PATH.concat(path);

		File file = new File(resource);
		if (!file.exists()) {
			logger.info("Oops! File not found");
			
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		InputStreamResource isResource = new InputStreamResource(new FileInputStream(file));
		FileSystemResource fileSystemResource = new FileSystemResource(file);
		
		String fileName = file.getName();
		fileName = new String(fileName.getBytes("UTF-8"), "iso-8859-1");
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
		headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
		headers.add("Pragma", "no-cache");
		headers.add("Expires", "0");
		
		headers.setContentLength(fileSystemResource.contentLength());
		headers.setContentDispositionFormData("attachment", fileName);
		
		return new ResponseEntity<InputStreamResource>(isResource, headers, HttpStatus.OK);
	}
	
//	@RequestMapping(value = "/app/path/**", method = RequestMethod.GET, produces = "application/apk")
//	public FileSystemResource downloadByVersion(HttpServletRequest request) throws IOException {
//		
//		String uri = request.getRequestURI();
//		
//		logger.info("=======================================");
//		logger.info("* download apk path [{}]", uri);
//		logger.info("=======================================");
//		
//		String resource = uri.replace("/app/path/", APP_BASE_PATH);
//		
//		//String path = managerMapper.findAppPathBySeq(version);
//		//String resource = APP_BASE_PATH.concat(path);
//		
//		File file = new File(resource);
//		if (!file.exists()) {
//			logger.info("Oops! File not found");
//		}
//		
////		FileOutputStream fop = new FileOutputStream(file);
////		fop.flush();
////		fop.close();
//
//		logger.info("* Done... ");
//		
//		return new FileSystemResource(file);
//		
//	}
	
	@RequestMapping(value = "/apk/path/**", method = RequestMethod.GET)
	public ResponseEntity<InputStreamResource> downloadByVersion(HttpServletRequest request) {
		
		Stopwatch stopwatch = Stopwatch.createStarted();
		
		try {
			String uri = request.getRequestURI();
			
			logger.info("=======================================");
			logger.info("* download apk path [{}]", uri);
			logger.info("=======================================");
			
			String resource = uri.replace("/apk/path/", APP_BASE_PATH);
			
			//String path = managerMapper.findAppPathBySeq(version);
			//String resource = APP_BASE_PATH.concat(path);
			
			File file = new File(resource);
			if (!file.exists()) {
				logger.info("* Oops! File not found");
				
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
			
			InputStreamResource isResource = new InputStreamResource(new FileInputStream(file));
			FileSystemResource fileSystemResource = new FileSystemResource(file);
			
			String fileName = file.getName();
			fileName = new String(fileName.getBytes("UTF-8"), "iso-8859-1");
			
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
			headers.add("Pragma", "no-cache");
			headers.add("Expires", "0");
			
			headers.setContentLength(fileSystemResource.contentLength());
			headers.setContentDispositionFormData("attachment", fileName);
			
			logger.info("* apk download done...");
			
			stopwatch.stop();
			
			logger.info(">>> elapsed time  [{}]", stopwatch);
			
			return new ResponseEntity<InputStreamResource>(isResource, headers, HttpStatus.OK);
		} catch (Exception e) {
			
			e.printStackTrace();
			
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		
		
	}
}
