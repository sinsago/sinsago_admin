package com.epopcon.smartssen.controller;

import java.text.ParseException;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.epopcon.smartssen.domain.Admin;
import com.epopcon.smartssen.domain.ApproveManager;
import com.epopcon.smartssen.domain.Fee;
import com.epopcon.smartssen.domain.InspectFee;
import com.epopcon.smartssen.mapper.ManagerMapper;
import com.epopcon.smartssen.service.ExcelBuilder;
import com.epopcon.smartssen.service.LoginUserDetailsService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

@Controller
public class FeeController {

	
	private static final Logger logger = LoggerFactory.getLogger(FeeController.class);
	
	@Autowired
	private ManagerMapper managerMapper;
	
	@Autowired
	private LoginUserDetailsService details;
	
	@Value("${properties.fee.excel-filename}")
	private String EXCEL_FILENAME;
	
	@Value("${properties.inspect.excel-filename}")
	private String INSPECT_EXCEL_FILENAME;
	
	@RequestMapping("/fee")
	public String fee(){
		return "/fee/list";
	}
	
	@RequestMapping("/fee/month")
	public @ResponseBody Map<String, Object> getMonthlyFee(String month){
		
		Admin admin = details.getUserInfo();
		
		String mngrId = "";
		if(!admin.getAdmin_type().equals("10")){
			mngrId = admin.getAdmin_id();
		}
		
		List<Fee> list = Lists.newArrayList();
		
		//list = findByMonthlyFeeData(month, mngrId);
		
		//정산관리 기준 변경 > 일별 수수료 적용
		list = managerMapper.findByMonthlyClosing(month, mngrId);
		
		String isComplate = managerMapper.findByMonthlyClosingComplete(month);
		
		
		logger.info("* get monthly fee [{}]", month);
		logger.info("* result list [{}]", list.toString());
		
		Map<String, Object> result = Maps.newHashMap();
		result.put("data", list);	//정산리스트
		result.put("isComplete", isComplate);	//정산완료 여부 
		
		
		return result ;
	}
	
	@SuppressWarnings("unused")
	private List<Fee> findByMonthlyFeeData(String month, String mngrId) {
		
		List<Fee> list = Lists.newArrayList();
		
		if(month.equals(DateFormatUtils.format(new Date(), "yyyy-MM"))){
			
			list = managerMapper.findByTodayFee(mngrId);
			
			Integer aac = 0;
			Integer aca = 0;
			for(Fee f : list){
				aac += f.getAn_approve_cnt();
				aca += f.getCalc_amount();
			}
			
			//리스트 정렬을 위해  뒤집어 준다
			Collections.reverse(list);
			
			Fee tRow = new Fee();
			
			tRow.setFee_seq(0L);
			tRow.setAn_approve_cnt(aac);
			tRow.setCalc_amount(aca);
			tRow.setThisMonth(true);
			
			list.add(tRow);
			
			Collections.reverse(list);
			
		}else{
			list = managerMapper.findByMonthlyFee(month, mngrId);
		}
		
		return list;
	}

	@RequestMapping(value = "/fee/excel/download", method = RequestMethod.GET)
	public ModelAndView excelDownload(
			HttpServletRequest request
			, HttpServletResponse response
			, String month) throws ParseException{
		
		Admin admin = details.getUserInfo();
		
		String mngrId = "";
		if(!admin.getAdmin_type().equals("10")){
			mngrId = admin.getAdmin_id();
		}
		
		Map<String, Object> model = Maps.newHashMap();
		
		//Sheet name
		model.put("sheetname", month+" 정산내역");
		
		//Headers List
	    List<String> headers = Lists.newArrayList();
	    headers.add("No.");
	    headers.add("교사명");
	    headers.add("교사ID");
	    headers.add("등록일");
	    
	    headers.add("승인완료 건수");
	    headers.add("적용 수수료");
	    headers.add("수수료 적용일");
	    headers.add("소계");
	    headers.add("정산금액");
	    
	    model.put("headers", headers);
	    
	    //Results Table
	    //model.put("results", resultsHandler(findByMonthlyFeeData(month, mngrId)));
	    model.put("results", resultsHandler(managerMapper.findByMonthlyClosing(month, mngrId)));
	    
        response.setContentType("application/Msexcel");
        response.setHeader( "Content-disposition", "attachment; filename="+EXCEL_FILENAME);
        
        return new ModelAndView(new ExcelBuilder(), model);
	}

	private List<List<String>> resultsHandler(List<Fee> data) throws ParseException {

		List<List<String>> results = Lists.newArrayList();
		
		// 총계 row
		results.add(makeTotalRow(data));
		
		for(Fee fee : data){
			
			List<String> columns = Lists.newArrayList();
			columns.add(String.valueOf(fee.getRownum()));
			columns.add(fee.getSt_nm());
			columns.add(fee.getSt_id());
			columns.add(DateFormatUtils.format(DateUtils.parseDate(fee.getReg_date(), new String[]{"yyyy-MM-dd HH:mm:ss.sss"}), "yyyy-MM-dd"));
			columns.add(String.valueOf(fee.getAn_approve_cnt()));
			columns.add(String.valueOf(fee.getCalc_fee()));
			columns.add(String.valueOf(fee.getApply_date()));
			columns.add(String.valueOf(fee.getSub_sum()));
			columns.add(String.valueOf(fee.getTot_sum()));
			
			results.add(columns);
		}
		
		return results;
	}
	
	private List<String> makeTotalRow(List<Fee> data) {
		
		int approve = 0;
		int fees = 0;
		
		for(Fee fee : data){
			
			approve += fee.getAn_approve_cnt();
			fees += fee.getSub_sum();
			
		}
		
		List<String> columns = Lists.newArrayList();
		columns.add("총계");
		columns.add("-");
		columns.add("-");
		columns.add("-");
		columns.add(String.valueOf(approve));
		columns.add("-");
		columns.add("-");
		columns.add("-");
		columns.add(String.valueOf(fees));
	
		return columns;
	}

	@SuppressWarnings("unused")
	private String getTeachersType(Integer stType) {
		switch (stType) {
		case 10:
			return "교사";
		case 20:
			return "아르바이트";
		case 30:
			return "내부직원";
		default:
			return "교사";
		}
		
	}

	@RequestMapping(value = "/fee/save", method = RequestMethod.POST)
	public @ResponseBody Long monthlyAccounting(String month) {
		
		logger.debug(">>> MONTHLY ACCOUNTING: {}", month);
		
		Long count = 0L;
		
		count = managerMapper.insertMonthlyAccounting(month);
		
        return count;
	}
	
	@RequestMapping("/fee/approve/month")
	public @ResponseBody Map<String, Object> getMonthlyApproveFee(String month){
		
		Admin admin = details.getUserInfo();
		
		String mngrId = "";
		if(!admin.getAdmin_type().equals("10")){
			mngrId = admin.getAdmin_id();
		}
		
		List<InspectFee> list = Lists.newArrayList();
		if(month.equals(DateFormatUtils.format(new Date(), "yyyy-MM"))){
			list = managerMapper.findByMonthlyApproveFee(month, mngrId);
		}else{
			list = managerMapper.findByApproveFee(month, mngrId);
		}
		
		logger.info("* get monthly fee [{}]", month);
		logger.info("* result list [{}]", list.toString());
		
		Map<String, Object> result = Maps.newLinkedHashMap();
		
		
		ApproveManager state = 
		managerMapper.findApproveByState(month);
		
		result.put("data", list);
		result.put("state", state);
		
		return result;
	}
	
	@RequestMapping("/fee/closing")
	public @ResponseBody Map<String, Object> getMonthlyApproveFeeClosing(String month){
		
		Admin admin = details.getUserInfo();
		
		String mngrId = "";
		if(!admin.getAdmin_type().equals("10")){
			mngrId = admin.getAdmin_id();
		}
		
		List<InspectFee> list = Lists.newArrayList();
		if(month.equals(DateFormatUtils.format(new Date(), "yyyy-MM"))){
			list = managerMapper.findByMonthlyApproveFee(month, mngrId);
		}else{
			list = managerMapper.findByApproveFee(month, mngrId);
		}
		
		logger.info("* get monthly fee [{}]", month);
		logger.info("* result list [{}]", list.toString());
		
		Map<String, Object> result = Maps.newLinkedHashMap();
		
		
		ApproveManager state = 
				managerMapper.findApproveByState(month);
		
		result.put("data", list);
		result.put("state", state);
		
		return result;
	}
	
	@RequestMapping(value = "/fee/approve/excel/download", method = RequestMethod.GET)
	public ModelAndView excelDownloadByApprove(
			HttpServletRequest request
			, HttpServletResponse response
			, String month) throws ParseException{
		
		Admin admin = details.getUserInfo();
		
		String mngrId = "";
		if(!admin.getAdmin_type().equals("10")){
			mngrId = admin.getAdmin_id();
		}
		
		List<InspectFee> list = Lists.newArrayList();
		if(month.equals(DateFormatUtils.format(new Date(), "yyyy-MM"))){
			list = managerMapper.findByMonthlyApproveFee(month, mngrId);
		}else{
			list = managerMapper.findByApproveFee(month, mngrId);
		}
		
		Map<String, Object> model = Maps.newHashMap();
		
		//Sheet name
		model.put("sheetname", month+" 정산내역");
		
		//Headers List
	    List<String> headers = Lists.newArrayList();
	    headers.add("No.");
	    headers.add("관리자ID");
	    headers.add("관리자명");
	    headers.add("등록일");
	    headers.add("승인완료 건수");
	    headers.add("적용 수수료");
	    headers.add("정산금액");
	    model.put("headers", headers);
	    
	    //Results Table
	    model.put("results", resultsHandlerByApprove(list));
	    
        response.setContentType("application/Msexcel");
        response.setHeader( "Content-disposition", "attachment; filename="+INSPECT_EXCEL_FILENAME);
        
        return new ModelAndView(new ExcelBuilder(), model);
	}
	
	private List<List<String>> resultsHandlerByApprove(List<InspectFee> data) throws ParseException {

		List<List<String>> results = Lists.newArrayList();
		for(InspectFee fee : data){
			
			List<String> columns = Lists.newArrayList();
			if(fee.getTotal() == 0){
				columns.add("총계");
				columns.add("-");
				columns.add("-");
				columns.add("-");
				columns.add(String.valueOf(fee.getApprove_cnt()));
				columns.add("-");
				columns.add(String.valueOf(fee.getCalc_amount()));
			}else{
				columns.add(String.valueOf(fee.getRownum()));
				columns.add(fee.getAdmin_id());
				columns.add(fee.getAdmin_nm());
				columns.add(DateFormatUtils.format(DateUtils.parseDate(fee.getReg_date(), new String[]{"yyyy-MM-dd HH:mm:ss.sss"}), "yyyy-MM-dd"));
				columns.add(String.valueOf(fee.getApprove_cnt()));
				columns.add(String.valueOf(fee.getCalc_fee()));
				columns.add(String.valueOf(fee.getCalc_amount()));
			}
			results.add(columns);
		}
		
		return results;
	}
	
	@RequestMapping(value = "/fee/approve/save", method = RequestMethod.POST)
	public @ResponseBody Long approveSave(String month) {
		
		Long count = 0L;
		
		count = managerMapper.insertApproveManager(month);
		
        return count;
	}
}
