package com.epopcon.smartssen.controller;

import java.text.ParseException;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.epopcon.smartssen.component.DateUtils;
import com.epopcon.smartssen.domain.AddedFee;
import com.epopcon.smartssen.domain.Admin;
import com.epopcon.smartssen.domain.Period;
import com.epopcon.smartssen.domain.Teacher;
import com.epopcon.smartssen.domain.TeacherTest;
import com.epopcon.smartssen.domain.Test;
import com.epopcon.smartssen.domain.TestRating;
import com.epopcon.smartssen.domain.TodaysFee;
import com.epopcon.smartssen.mapper.ManagerMapper;
import com.epopcon.smartssen.mapper.TeacherMapper;
import com.epopcon.smartssen.service.ExcelBuilder;
import com.epopcon.smartssen.service.LoginUserDetailsService;
import com.google.common.base.Stopwatch;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

@Controller
public class TeacherController {
	
	private static final Logger logger = LoggerFactory.getLogger(TeacherController.class);
	
	@Autowired
	private TeacherMapper teacherMapper;
	
	@Autowired
	private ManagerMapper managerMapper;
	
	@Autowired
	private LoginUserDetailsService details;
	
	@Autowired
	private DateUtils dateUtils;
	
	@Value("${properties.note.deadline.hour}")
	private Integer DEADLINE_HOUR;
	
	@Value("${properties.teacher.excel-filename}")
    private String EXCEL_FILENAME;

	@RequestMapping("/teacher")
	public String teacher(){
		return "/teacher/list";
	}

	@RequestMapping("/teacher/search")
	public @ResponseBody Map<String, Object> search(
			@RequestParam(value="searchType", required=false) Integer searchType
			, @RequestParam(value="searchWord", required=false) String searchWord
			, @RequestParam(value="page", required=false, defaultValue="0") Integer page
			, @RequestParam(value="size", required=false, defaultValue="10") Integer size) {

		Stopwatch stopwatch = Stopwatch.createStarted();
		
		Integer startCnt = ((page+1) - 1) * size + 1;
		Integer endCnt = (page+1) * size;
		
		logger.info("=======================================");
		logger.info("* search 검색타입[{}] 검색어[{}]", searchType, searchWord);
		logger.info("* paging page[{}] 시작[{}] 종료[{}]", page, startCnt, endCnt);
		logger.info("=======================================");
		
		Admin admin = details.getUserInfo();
		
		String mngrId = "";
		if(!admin.getAdmin_type().equals("10")){
			mngrId = admin.getAdmin_id();
		}
		
		List<Teacher> list = Lists.newArrayList(); 
		list = teacherMapper.findByTeachers(searchType, searchWord, startCnt, endCnt, mngrId);
		
		Map<String, Object> result = Maps.newLinkedHashMap();
		result.put("recordsTotal", list.size() > 0 ? list.get(0).getTotal() : 0);
		result.put("recordsFiltered", list.size() > 0 ? list.get(0).getTotal() : 0);
		result.put("data", list);
		
		stopwatch.stop();
		
		logger.info(">>> elapsed time  [{}]", stopwatch);
		
		return result;
	}
	
	@RequestMapping("/teacher/form")
	public String form(
			Locale locale
			, Model model
			, HttpSession session
			, @RequestParam(value="stId", required=false) String stId){
		
		logger.info("=======================================");
		logger.info("* form stId[{}]", stId);
		logger.info("=======================================");
		
		Teacher tInfo = new Teacher();
		
		if(StringUtils.isBlank(stId)){

			//add
			tInfo.setSt_id("");
			tInfo.setSt_nm("");
			tInfo.setDefault_fee(600);
			tInfo.setSt_rrn("");
			tInfo.setSt_pwd("");
			tInfo.setSt_type(10);
			tInfo.setSt_state(1);
			tInfo.setPeriod_fee(0);
			tInfo.setTest_fee(0);
			tInfo.setMngr_id("");
			tInfo.setSt_tel("");
			
		}else{
			//read
			tInfo = teacherMapper.findByStId(stId);
		};
		
		logger.info("* tInfo {}", tInfo.toString());
		
		model.addAttribute("tInfo", tInfo);
		
		return "/teacher/form";
	}
	
	@RequestMapping("/teacher/validation/id")
	public @ResponseBody Boolean isValidationId(
			@RequestParam("stId") String stId) {
		
		Integer count = 0;
		count = teacherMapper.countById(stId);
		
		logger.info("=======================================");
		logger.info("* isValidationId 교사ID[{}] ", stId);
		logger.info("* countById [{}] ", count);
		logger.info("=======================================");
		
		return count < 1;
	}
	
	@RequestMapping("/teacher/validation/name")
	public @ResponseBody Boolean isValidationName(
			@RequestParam("stName") String stName) {
		
		Integer count = 0;
		count = teacherMapper.countByName(stName);
		
		logger.info("=======================================");
		logger.info("* isValidationName 교사명[{}] ", stName);
		logger.info("* countByName [{}] ", count);
		logger.info("=======================================");
		
		return count < 1;
	}
	
	@RequestMapping("/teacher/managers")
	public @ResponseBody List<Admin> getManagers() {
		
		List<Admin> mngr = Lists.newArrayList();
		mngr = managerMapper.findAllManagers();
		
		return mngr;
	}
	
	@RequestMapping("/teacher/teachers")
	public @ResponseBody List<Teacher> getTeachers() {
		
		List<Teacher> list = Lists.newArrayList();
		list = teacherMapper.findTeachersByState(1);
		
		return list;
	}
	
	@RequestMapping("/teacher/save")
	public @ResponseBody Teacher save(
			@RequestParam("isModify") Boolean isModify
			, Teacher teacher) throws Exception {

		if(isModify){
			teacherMapper.updateByTeacher(teacher);
			teacherStateHandler(teacher);
		}else{
			teacherMapper.createByTeacher(teacher);
		}
		return new Teacher();
		
	}
	
	private void teacherStateHandler(Teacher teacher) {
		
		//휴직처리하는 관리자 아이디를 저장
		teacher.setMngr_id(details.getUserInfo().getAdmin_id());
		
		switch (teacher.getSt_state()) {
		case 0:		//근무종료
			
			break;
		case 1:		//근무중
			
//			update 휴직중
//			Leave l = teacherMapper.findTeacherLeave(teacher);
//			if(!ObjectUtils.isEmpty(l)){
//				teacherMapper.updateByTeacherLeave(l);
//			}
			
			teacherMapper.updateByTeacherLeave(teacher);
			
			break;
		case 2:		//휴직중

			teacherMapper.saveTeacherLeave(teacher);
			break;

		default:
			break;
		}
		
	}

	@RequestMapping("/teacher/fee/period")
	public @ResponseBody List<Period> feeByPeriod(
			String stId) throws Exception {
		
		logger.info("=======================================");
		logger.info("* feeByPeriod stId[{}] ", stId);
		logger.info("=======================================");
		
		List<Period> period = Lists.newArrayList();  
		period = teacherMapper.findByPeriod(stId);
		
		logger.info("* period {}", period.toString());
		
		return period;
		
	}
	
	@RequestMapping("/teacher/fee/test")
	public @ResponseBody Map<String, Object> feeByTest(
			String stId) throws Exception {
		
		logger.info("=======================================");
		logger.info("* feeByTest stId[{}] ", stId);
		logger.info("=======================================");
		
		List<Test> test = Lists.newArrayList();  
		test = teacherMapper.findByTest(stId);
		
		logger.info("* test {}", test.toString());
		
		TodaysFee today = 
		teacherMapper.findByTodaysFee(stId);
		
		logger.info(">>> today fee {}", today.toString());
		
		Map<String, Object> result = Maps.newHashMap();
		result.put("recordOfTest", test);
		result.put("feeOfTodays", today);
		
		return result;
		
	}
	
	@RequestMapping("/teacher/fee/added")
	public @ResponseBody AddedFee feeByAdded(AddedFee added) throws Exception {
		
		logger.info("=======================================");
		logger.info("* feeByAdded added fee: {}", added.toString());
		logger.info("=======================================");
		
		int count = 0;
		count = teacherMapper.saveAddedFee(added);
		
		if(count > 0 && added.getIs_recalculation()){
			//Todo SP update
			
			
			logger.info(">>> SP update daily fee: ST_ID[{}] DAYS[{}]", added.getSt_id(), added.getRecalculation_day());
			
			//delete data
			teacherMapper.deleteDailyFee(added);
			
			//update data
			teacherMapper.updateDailyFee(added);
			
		}
		
		return new AddedFee();
		
	}
	
	@RequestMapping("/teacher/fee/added/remove")
	public @ResponseBody AddedFee feeByAddedRemove(AddedFee added) throws Exception {
		
		logger.info("=======================================");
		logger.info("* feeByAdded added fee: {}", added.toString());
		logger.info("=======================================");
		
		teacherMapper.updateByAddedFeeState(added);
		
		return new AddedFee();
		
	}
	
	
	@RequestMapping("/teacher/note/statistic")
	public @ResponseBody TestRating findByNoteStatistic(
			String stId
			, String startDate 
			, String endDate) throws Exception {
		
		logger.info("=======================================");
		logger.info("* findByNoteStatistic stId[{}] start[{}] end[{}] DEADLINE_HOUR[{}]", stId, startDate, endDate, DEADLINE_HOUR);
		logger.info("=======================================");
		
		TestRating statistics;
		//statistics = teacherMapper.findByNoteStatistic(new NoteStatistics(stId, startDate, endDate, DEADLINE_HOUR));
		
		statistics = teacherMapper.findByTeacherTest(new TeacherTest(stId, startDate, endDate));
		
//		if(StringUtils.isBlank(statistics.getStId())){
//			statistics = new NoteStatistics();
//		}
		
		return statistics;
		
	}
	
	@RequestMapping(value = "/teacher/excel/download", method = RequestMethod.GET)
    public ModelAndView excelDownload(
    		
    		HttpServletRequest request
			, HttpServletResponse response
			
    		, @RequestParam(value = "searchType", required = false) Integer searchType
            , @RequestParam(value = "searchWord", required = false) String searchWord) throws ParseException {

        Stopwatch stopwatch = Stopwatch.createStarted();
        
        logger.info("=======================================");
		logger.info("* search 검색타입[{}] 검색어[{}]", searchType, searchWord);
		logger.info("=======================================");
		
		Admin admin = details.getUserInfo();
		
		String mngrId = "";
		if(!admin.getAdmin_type().equals("10")){
			mngrId = admin.getAdmin_id();
		}
		
		List<Teacher> list = Lists.newArrayList(); 
		list = teacherMapper.findByTeachers(searchType, searchWord, 0, 0, mngrId);
        
        Map<String, Object> model = Maps.newHashMap();
        
        //Sheet name
		model.put("sheetname", "교사리스트");
		
		//Headers List
		List<String> headers = Lists.newArrayList();
		headers.add("No.");
		headers.add("교사ID");
		headers.add("교사명");
		headers.add("구분");
		headers.add("등록일");
		headers.add("종료일");
		headers.add("적용수수료");
		headers.add("관리담당자");
		headers.add("상태");
		
		model.put("headers", headers);
		
		//Results Table
		model.put("results", resultsHandler(list));
		
		response.setContentType("application/Msexcel");
		response.setHeader( "Content-disposition", "attachment; filename="+EXCEL_FILENAME);
		
		stopwatch.stop();
        logger.info(">>> elapsed time  [{}]", stopwatch);
		
		return new ModelAndView(new ExcelBuilder(), model);
        
    }
	
	private List<List<Object>> resultsHandler(List<Teacher> data) throws ParseException {

		List<List<Object>> results = Lists.newArrayList();
		for(Teacher teacher : data){
			
			List<Object> columns = Lists.newArrayList();
			
			columns.add(String.valueOf(teacher.getRownum()));
			columns.add(teacher.getSt_id());
			columns.add(teacher.getSt_nm());
			
			//교사구분
			switch(teacher.getSt_type()) {
				case 10:
					columns.add("교사");
					break;
				case 20:
					columns.add("아르바이트");
					break;
				case 30:
					columns.add("내부직원");
					break;
				default:
					columns.add("교사");
			}
			
			columns.add(dateUtils.getDate(teacher.getReg_date(), "yyyy-MM-dd"));
			columns.add(dateUtils.getDate(teacher.getEnd_date(), "yyyy-MM-dd"));
			columns.add((teacher.getDefault_fee() + teacher.getPeriod_fee() + teacher.getTest_fee()) + " 원");
			columns.add(teacher.getMngr_nm());
			
			//교사 상태구분
			switch(teacher.getSt_state()) {
				case 0:
					columns.add("근무종료");
					break;
				case 1:
					columns.add("근무중");
					break;
				case 2:
					columns.add("휴직중");
					break;
				default:
					columns.add("근무중");
			}
			results.add(columns);
		}
		
		return results;
	}

	@RequestMapping("/teacher/complateOfClosedMonth")
	public @ResponseBody List<String> findByMonthlyAccounting(String startDate) throws Exception {
		
		logger.info("=======================================");
		logger.info("* findByMonthlyAccounting start[{}]", startDate);
		logger.info("=======================================");
		
		List<String> result = Lists.newArrayList();
		result = managerMapper.findByMonthlyAccounting(startDate);
		
		logger.info(">>> result > {}", result.toString());
		
		return result;
		
	}
}
