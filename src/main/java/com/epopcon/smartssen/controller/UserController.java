package com.epopcon.smartssen.controller;

import java.util.Locale;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.epopcon.smartssen.domain.Admin;
import com.epopcon.smartssen.service.LoginUserDetails;

@Controller
public class UserController {

	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	
	@RequestMapping("/login")
	public String loginForm(
			Locale locale
			, Model model
			, HttpSession session
			, @RequestParam(value = "fail", required = false) Boolean isFail) {
		
		logger.debug("=====================================================================");
		logger.debug("As always sir, a great pleasure watching you work.");
		logger.debug("=====================================================================");
		
		return "/user/login";
	}
	
	@RequestMapping("/login_success")
	public String loginSuccess(HttpSession session){
		
		LoginUserDetails userDetails = (LoginUserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		Admin user = userDetails.getUser();
		
		session.setAttribute("userLoginInfo", user);
		
//		String page;
//		if(user.getAdminLevel() > 0){
//			page = "/creditCard/creditCardView";
//		}else{
//			page = "/dashboard";
//		}
        
		logger.debug("=====================================================================");
//		logger.info("Welcome login_success! {}, {}", session.getId(), user.getAdminId() + "/" + user.getAdminLevel() + "/" + page);
		logger.info("Welcome login_success! {}, {}", session.getId()  + "/" + "dashboard");
		logger.debug("=====================================================================");
		
		return "/answer/list";
	}
}
