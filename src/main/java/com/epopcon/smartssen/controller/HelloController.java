package com.epopcon.smartssen.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.epopcon.smartssen.domain.Admin;
import com.epopcon.smartssen.mapper.ManagerMapper;

@Controller
public class HelloController {
	
	private static final Logger logger = LoggerFactory.getLogger(HelloController.class);
	
	@Autowired 
	private ManagerMapper userMapper;
	
	@RequestMapping("/hello")
	public String hello(){
		
		logger.debug(">>> comming...... hello!!!");
		
		Admin admin = userMapper.findManagerById("spadmin");
		
		return admin.toString();
	}
	

}
