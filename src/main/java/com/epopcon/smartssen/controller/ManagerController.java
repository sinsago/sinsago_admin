package com.epopcon.smartssen.controller;

import java.text.ParseException;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.epopcon.smartssen.component.DateUtils;
import com.epopcon.smartssen.domain.Admin;
import com.epopcon.smartssen.domain.GradeCharge;
import com.epopcon.smartssen.domain.Teacher;
import com.epopcon.smartssen.mapper.ManagerMapper;
import com.epopcon.smartssen.service.ExcelBuilder;
import com.epopcon.smartssen.service.ManagementService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Stopwatch;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

@Controller
public class ManagerController {

	private static final Logger logger = LoggerFactory.getLogger(ManagerController.class);
	
	@Autowired
	private ManagerMapper managerMapper;
	
	@Autowired
	private ManagementService service;
	
	@Autowired
	private DateUtils dateUtils;
	
	@Value("${properties.manager.excel-filename}")
    private String EXCEL_FILENAME;
	
	@RequestMapping("/manager")
	public String manager(){
		return "/manager/list";
	}
	
	@RequestMapping("/manager/search")
	public @ResponseBody Map<String, Object> search(
			
			@RequestParam(value="page", required=false, defaultValue="0") Integer page
			, @RequestParam(value="size", required=false, defaultValue="10") Integer size) {

		Stopwatch stopwatch = Stopwatch.createStarted();
		
		Integer startCnt = ((page+1) - 1) * size + 1;
		Integer endCnt = (page+1) * size;
		
		logger.debug("=======================================");
		logger.debug("* search paging page[{}] 시작[{}] 종료[{}]", page, startCnt, endCnt);
		logger.debug("=======================================");
		
		List<Admin> list = Lists.newArrayList();
		list = managerMapper.findByManagers(startCnt, endCnt);
		
		for(Admin data : list) {
			data.setGrade_charge(service.findGradeCharge(data.getAdmin_id()));
		}
		
		Map<String, Object> result = Maps.newLinkedHashMap();
		result.put("recordsTotal", list.size() > 0 ? list.get(0).getTotal() : 0);
		result.put("recordsFiltered", list.size() > 0 ? list.get(0).getTotal() : 0);
		result.put("data", list);
		
		logger.debug("* managers {}", list.toString());
		logger.debug(">>> elapsed time  [{}]", stopwatch);
		
		stopwatch.stop();
		
		return result;
	}
	
	@RequestMapping("/manager/form")
	public String form(
			Locale locale
			, Model model
			, HttpSession session
			, @RequestParam(value="adminId", required=false) String id){
		
		logger.debug("=======================================");
		logger.debug("* form manager id[{}]", id);
		logger.debug("=======================================");
		
		Admin info = new Admin();
		
		if(StringUtils.isNotBlank(id)){
			info = managerMapper.findManagerById(id);
		}else{
			info.setAdmin_type("20");
			info.setManaged_cnt(0L);
		}
		
		logger.debug("* manager info {}", info.toString());
		model.addAttribute("info", info);
		
		return "/manager/form";
	}
	
	@RequestMapping("/manager/validation/id")
	public @ResponseBody Boolean isValidationId(
			@RequestParam("adminId") String id) {
		
		Integer count = 0;
		count = managerMapper.countById(id);
		
		logger.debug("=======================================");
		logger.debug("* isValidationId Id[{}] ", id);
		logger.debug("* countById [{}] ", count);
		logger.debug("=======================================");
		
		return count < 1;
	}
	
	@RequestMapping("/manager/validation/name")
	public @ResponseBody Boolean isValidationName(
			@RequestParam("adminName") String name) {
		
		Integer count = 0;
		count = managerMapper.countByName(name);
		
		logger.debug("=======================================");
		logger.debug("* isValidationName Name[{}] ", name);
		logger.debug("* countByName [{}] ", count);
		logger.debug("=======================================");
		
		return count < 1;
	}
	
	@RequestMapping("/manager/save")
	public @ResponseBody Admin save(
			@RequestParam("isModify") Boolean isModify
			, @RequestParam("charge") String charge
			, Admin admin) throws Exception {
		
		logger.debug("=======================================");
		logger.debug("* save manager info[{}] ", admin.toString());
		logger.debug("=======================================");
		
		ObjectMapper mapper = new ObjectMapper();
		
		List<GradeCharge> list = 
		mapper.readValue(charge, new TypeReference<List<GradeCharge>>(){});
		
		/*
		for(GradeCharge d : myObjects){
			logger.debug(" >>> type{} grade{}", d.getStudent_type(), d.getStudent_grade());
		}
		*/
		
		if(isModify){
			managerMapper.updateByManager(admin);
		}else{
			managerMapper.createByManager(admin);
		}
		
		service.gradeCharge(admin, list);
		
		return new Admin();
		
	}
	
	@RequestMapping("/manager/managed")
	public @ResponseBody Map<String, Object> findByManagedTeachers(
			String managerId
			, @RequestParam(value="page", required=false, defaultValue="0") Integer page
			, @RequestParam(value="size", required=false, defaultValue="10") Integer size) {
		
		
		Stopwatch stopwatch = Stopwatch.createStarted();
		
		Integer startCnt = ((page+1) - 1) * size + 1;
		Integer endCnt = (page+1) * size;
		
		logger.debug("=======================================");
		logger.debug("* search manager id[{}]", managerId);
		logger.debug("* paging page[{}] 시작[{}] 종료[{}]", page, startCnt, endCnt);
		logger.debug("=======================================");
		
		List<Teacher> list = Lists.newArrayList();
		list = managerMapper.findByManagedTeachers(managerId, startCnt, endCnt);
		
		Map<String, Object> result = Maps.newLinkedHashMap();
		result.put("recordsTotal", list.size() > 0 ? list.get(0).getTotal() : 0);
		result.put("recordsFiltered", list.size() > 0 ? list.get(0).getTotal() : 0);
		result.put("data", list);
		
		stopwatch.stop();
		
		logger.debug(">>> elapsed time  [{}]", stopwatch);
		
		return result;
	}
	
	@RequestMapping("/manager/grade")
	public @ResponseBody Map<String, Object> findGradeByManagerId 
	(
		@RequestParam(value="id", required=true) String managerId
	){
		
		Stopwatch stopwatch = Stopwatch.createStarted();
		
		logger.debug("=======================================");
		logger.debug("* findGradeByManagerId[{}]", managerId);
		logger.debug("=======================================");
		
		List<GradeCharge> list = service.findGradeCharge(managerId);
		
		Map<String, Object> result = Maps.newLinkedHashMap();
		result.put("data", list);
		
		stopwatch.stop();
		
		logger.debug(">>> elapsed time  [{}]", stopwatch);
		
		return result;
	}
	
	@RequestMapping("/manager/remove")
	public @ResponseBody Long remove(
			@RequestParam("managerId") String id) {
		
		Long count = managerMapper.deleteByManager(id);
		
		//담당학년 정보 삭제
		managerMapper.deleteGradeCharge(id);
		
		logger.debug("=======================================");
		logger.debug("* remove manager Id[{}] Count[{}] ", id, count);
		logger.debug("=======================================");
		
		return count;
	}
	
	
	@RequestMapping(value = "/manager/excel/download", method = RequestMethod.GET)
    public ModelAndView excelDownload(
    		
    		HttpServletRequest request
			, HttpServletResponse response) throws ParseException {

        Stopwatch stopwatch = Stopwatch.createStarted();
        
		List<Admin> list = Lists.newArrayList();
		list = managerMapper.findByManagers(0, 0);
		
		for(Admin data : list) {
			data.setGrade_charge(service.findGradeCharge(data.getAdmin_id()));
		}
        
        Map<String, Object> model = Maps.newHashMap();
        
        //Sheet name
		model.put("sheetname", "관리자리스트");
		
		//Headers List
		List<String> headers = Lists.newArrayList();
		headers.add("No.");
		headers.add("관리자ID");
		headers.add("구분");
		headers.add("관리자명");
		headers.add("등록일");
		headers.add("담당학년");
		headers.add("관리교사 수");
		
		model.put("headers", headers);
		
		//Results Table
		model.put("results", resultsHandler(list));
		
		response.setContentType("application/Msexcel");
		response.setHeader( "Content-disposition", "attachment; filename="+EXCEL_FILENAME);
		
		stopwatch.stop();
        logger.info(">>> elapsed time  [{}]", stopwatch);
		
		return new ModelAndView(new ExcelBuilder(), model);
        
    }
	
	private List<List<Object>> resultsHandler(List<Admin> data) throws ParseException {

		List<List<Object>> results = Lists.newArrayList();
		for(Admin admin : data){
			
			List<Object> columns = Lists.newArrayList();
			
			columns.add(String.valueOf(admin.getRownum()));
			
			columns.add(admin.getAdmin_id());
			//관리자구분
			switch(admin.getAdmin_type()) {
				case "10":
					columns.add("최고관리자");
					break;
				case "20":
					columns.add("승인관리자");
					break;
				case "30":
					columns.add("검수관리자");
					break;
				default:
					columns.add("승인관리자");
			}
			
			columns.add(admin.getAdmin_nm());
			columns.add(dateUtils.getDate(admin.getReg_date(), "yyyy-MM-dd"));
			
			if(admin.getGrade_charge().size() > 0){
				
				StringBuffer sb = new StringBuffer();
				for(int i=0; i < admin.getGrade_charge().size(); i++){
					
					GradeCharge d = admin.getGrade_charge().get(i);
					
					switch (d.getStudent_type()) {
						case 10:
							sb.append("초등 ");
							break;
						case 20:
							sb.append("중등 ");
							break;
						case 30:
							sb.append("고등 ");
							break;
	
						default:
							sb.append("초등 ");
							break;
					}
					
					sb.append(d.getStudent_grade() == 0 ? "전체" : d.getStudent_grade());
					
					if(i < (admin.getGrade_charge().size()-1)) sb.append(", ");
				}
				
				columns.add(sb.toString());
				
			}else{
				columns.add("-");
			}
			
			columns.add(admin.getManaged_cnt()+" 명");
			
			results.add(columns);
		}
		
		return results;
	}
}
