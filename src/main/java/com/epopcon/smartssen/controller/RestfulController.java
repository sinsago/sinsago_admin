package com.epopcon.smartssen.controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.epopcon.smartssen.domain.Note;
import com.epopcon.smartssen.domain.Teacher;
import com.epopcon.smartssen.mapper.TeacherMapper;
import com.epopcon.smartssen.service.AnswerNoteService;
import com.epopcon.smartssen.service.FileService;
import com.epopcon.smartssen.service.NotificationService;
import com.google.common.collect.Maps;

@RestController
public class RestfulController {

	private static final Logger logger = LoggerFactory.getLogger(RestfulController.class);

	@Autowired
	private FileService fileService;
	
	@Autowired
	private TeacherMapper teacherMapper;
	
	@Autowired
	private AnswerNoteService answerNoteService;
	
	@SuppressWarnings("unused")
	@Autowired
	private NotificationService notificationService;
	
	@RequestMapping(value = "/api/note", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> addNote(Note note) {
		
		logger.info("=======================================");
		logger.info("* api add note [{}]", note.toString());
		logger.info("=======================================");
		
		String next = "";
		Integer resultCode = 20;
		String resultDesc = "학습자의 답안이 등록이 실패했습니다";
		
		try {
			
			String answerImagePath = fileService.saveImage(note);
			
			logger.info("* note image url [{}]", note.getFile_note());
			logger.info("* note save path [{}]", answerImagePath);
			
			note.setAn_img_path(answerImagePath);
			
			next = getNextStId(note.getStudent_type(), note.getStudent_grade());
			note.setSt_id(next);
			note.setAn_state(0);	//첨삭대기
			
			//Save Database
			answerNoteService.saveByNote(note);
			
			resultCode = 10;
			resultDesc = "학습자의 답안이 등록되였습니다";
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Map<String, Object> result = Maps.newHashMap();
		result.put("rt_cd", resultCode);
		result.put("rt_desc", resultDesc);
		result.put("rt_teacher_id", next);
		
		return result;
	}
	
	@RequestMapping(value = "/resource/**", method = RequestMethod.GET, produces = "image/png")
	public @ResponseBody byte[] resources(HttpServletRequest request) {
		
		logger.info("=======================================");
		
		String uri = request.getRequestURI();
		
		String resource = fileService.getFilePathFromUri(uri);
		
		Boolean isExists = new File(resource).exists(); 
		
		if(!isExists){
			resource = fileService.getNoImageFile();
		}
		
		logger.info("* get resource uri [{}]", uri);
		logger.info("* get resource path [{}]", resource);
		logger.info("* get resource exists [{}]", isExists);
		logger.info("=======================================");
		
		InputStream is = null;
		ByteArrayOutputStream bao = null;
		try {
	        // Retrieve image from the classpath.
			is = new FileInputStream(resource);

	        // Prepare buffered image.
	        BufferedImage img = ImageIO.read(is);

	        // Create a byte array output stream.
	        bao = new ByteArrayOutputStream();

	        // Write to output stream
	        ImageIO.write(img, "png", bao);
	        
	        if(is != null){
	    		is.close();
	    	}
	    } catch (Exception e) {
	        //throw new RuntimeException(e);
	    	logger.error("* resource [{}]", e.getMessage());
	    }
		
		return bao.toByteArray();
	}

	private String getNextStId(Integer type, Integer grade) {
		
		//get last punch ticket
		Long sequence = teacherMapper.findByLastPunchTicket();
		
		//근무중이며 내부직원(30)이 아닌 교사
		Teacher nextSequence = teacherMapper.findByTeacherSeq(sequence, type, grade);
		
		logger.info("* next teacher {}", nextSequence);
		
		teacherMapper.updateByPunchTicket(nextSequence.getSt_seq());
		
		return nextSequence.getSt_id();
	}
	
	
	
	
}


