package com.epopcon.smartssen.controller;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.epopcon.smartssen.domain.Admin;
import com.epopcon.smartssen.domain.Inspect;
import com.epopcon.smartssen.domain.InspectLog;
import com.epopcon.smartssen.domain.Note;
import com.epopcon.smartssen.mapper.ManagerMapper;
import com.epopcon.smartssen.mapper.NoteMapper;
import com.epopcon.smartssen.service.ExcelBuilder;
import com.epopcon.smartssen.service.LoginUserDetailsService;
import com.google.common.base.Stopwatch;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

@Controller
public class InspectController {

	
	private static final Logger logger = LoggerFactory.getLogger(InspectController.class);
	
	@Autowired
    private LoginUserDetailsService details;
	
	@Autowired
    private ManagerMapper mapper;
	
	@Autowired
	private NoteMapper noteMapper;
	
    @Value("${properties.inspect.excel-filename}")
    private String EXCEL_FILENAME;
    
    @Value("${properties.note-url.base}")
    private String NOTE_BASE_URL;
	
	@RequestMapping("/inspect")
	public String inspect(){
		return "/inspect/list";
	}
	
	
	@RequestMapping("/inspect/search")
    public @ResponseBody Map<String, Object> search(

    		@RequestParam(value = "dateType", required = false) Integer dateType
            , @RequestParam(value = "searchStart", required = false) String searchStart
            , @RequestParam(value = "searchEnd", required = false) String searchEnd

            , @RequestParam(value = "studentType", required = false) Integer studentType
            , @RequestParam(value = "gradeType", required = false) Integer gradeType
            
            , @RequestParam(value = "searchInspectType", required = false) Integer inspectType
            
            , @RequestParam(value = "searchType", required = false) Integer searchType
            , @RequestParam(value = "searchWord", required = false) String searchWord

            , @RequestParam(value = "page", required = false, defaultValue = "0") Integer page
            , @RequestParam(value = "size", required = false, defaultValue = "10") Integer size) {

        Stopwatch stopwatch = Stopwatch.createStarted();

        Integer startCnt = ((page + 1) - 1) * size + 1;
        Integer endCnt = (page + 1) * size;

        logger.info("=======================================");
        logger.info("* search 검색타입[{}] 검색어[{}]", searchType, searchWord);
        logger.info("* date type [{}] start [{}] end[{}]", dateType, searchStart, searchEnd);
        logger.info("* student type [{}] grade [{}]", studentType, gradeType);
        logger.info("* paging page[{}] 시작[{}] 종료[{}]", page, startCnt, endCnt);
        logger.info("=======================================");

        Admin admin = details.getUserInfo();
        logger.info("session Id[{}] Name[{}] Type[{}]", admin.getAdmin_id(), admin.getAdmin_nm(), admin.getAdmin_type());

        List<Note> list = Lists.newArrayList();
        list = noteMapper.findInspectByNotes(
        		searchType
        		, searchWord
        		, searchStart
        		, searchEnd
        		, startCnt
        		, endCnt
        		, dateType
        		, studentType
        		, gradeType
        		, inspectType);

        Map<String, Object> result = Maps.newLinkedHashMap();
        result.put("recordsTotal", list.size() > 0 ? list.get(0).getTotal() : 0);
        result.put("recordsFiltered", list.size() > 0 ? list.get(0).getTotal() : 0);
        result.put("data", list);

        stopwatch.stop();
        logger.info(">>> elapsed time  [{}]", stopwatch);

        return result;
    }
	
	@RequestMapping(value = "/inspect/excel", method = RequestMethod.GET)
	public ModelAndView excel(
			
			HttpServletRequest request
			, HttpServletResponse response
			
			, @RequestParam(value = "dateType", required = false) Integer dateType
			, @RequestParam(value = "searchStart", required = false) String searchStart
			, @RequestParam(value = "searchEnd", required = false) String searchEnd
			
			, @RequestParam(value = "studentType", required = false) Integer studentType
			, @RequestParam(value = "gradeType", required = false) Integer gradeType
			
			, @RequestParam(value = "searchInspectType", required = false) Integer inspectType
			
			, @RequestParam(value = "searchType", required = false) Integer searchType
			, @RequestParam(value = "searchWord", required = false) String searchWord) throws Exception {
		
		Stopwatch stopwatch = Stopwatch.createStarted();
		
		Integer startCnt = 0;
		Integer endCnt = 0;
		
		logger.info("=======================================");
		logger.info("* search 검색타입[{}] 검색어[{}]", searchType, searchWord);
		logger.info("* date type [{}] start [{}] end[{}]", dateType, searchStart, searchEnd);
		logger.info("* student type [{}] grade [{}]", studentType, gradeType);
		logger.info("=======================================");
		
		List<Note> list = Lists.newArrayList();
		list = noteMapper.findInspectByNotes(
				searchType
				, searchWord
				, searchStart
				, searchEnd
				, startCnt
				, endCnt
				, dateType
				, studentType
				, gradeType
				, inspectType);
		
		Map<String, Object> model = Maps.newHashMap();
        
        //Sheet name
		model.put("sheetname", "검수리스트");
		
		//Headers List
		List<String> headers = Lists.newArrayList();
		headers.add("No.");
		headers.add("답안ID");
		headers.add("학년");
		headers.add("교재명");
		headers.add("페이지");
		headers.add("아이디");
		headers.add("학습자");
		headers.add("첨삭요청일");
		headers.add("승인요청일");
		headers.add("승인완료일");
		headers.add("담당교사");
		headers.add("검수여부");
		headers.add("검수사유");
		
		model.put("headers", headers);
		
		//Results Table
		model.put("results", resultsHandler(list));
		
		response.setContentType("application/Msexcel");
		response.setHeader( "Content-disposition", "attachment; filename="+EXCEL_FILENAME);
		
		stopwatch.stop();
        logger.info(">>> elapsed time  [{}]", stopwatch);
		
		return new ModelAndView(new ExcelBuilder(), model);
	}
	
	@RequestMapping("/inspect/config")
	public @ResponseBody Map<String, Object> config() {
		
		Stopwatch stopwatch = Stopwatch.createStarted();
		
		logger.info("=======================================");
		logger.info(">>> 검수조건 ");
		logger.info("=======================================");
		
		Inspect data = mapper.findByInspectConfig();
		
		Map<String, Object> result = Maps.newLinkedHashMap();
		result.put("data", data);
		
		logger.info("* INSPECT {}", data.toString());

		stopwatch.stop();
		logger.info(">>> elapsed time  [{}]", stopwatch);
		
		return result;
	}
	
	@RequestMapping("/inspect/config/save")
	public @ResponseBody Map<String, Object> configSave(Inspect req) {
		
		Stopwatch stopwatch = Stopwatch.createStarted();
		
		logger.info("=======================================");
		logger.info(">>> 검수조건 type[{}] rate[{}] fee[{}]");
		logger.info("=======================================");
		
		Integer count = mapper.updateInspectByConfig(req);
		
		logger.info(">>> INSPECT UPDATE COUNT[{}]", count);
		
		Map<String, Object> result = Maps.newLinkedHashMap();
		result.put("data", count);
		
		stopwatch.stop();
		logger.info(">>> elapsed time  [{}]", stopwatch);
		
		return result;
	}
	
	private List<List<Object>> resultsHandler(List<Note> data) throws ParseException {

		List<List<Object>> results = Lists.newArrayList();
		for(Note note : data){
			
			List<Object> columns = Lists.newArrayList();
			
			columns.add(String.valueOf(note.getRownum()));
			columns.add(note.getAn_id());
			
			switch (note.getStudent_type()) {
				case 10:
					columns.add("초등 " + note.getStudent_grade());
					break;
				case 20:
					columns.add("중등 " + note.getStudent_grade());
					break;
				case 30:
					columns.add("고등 " + note.getStudent_grade());
					break;
				default:
					break;
			}
			
			columns.add(note.getProduct_nm());
			columns.add(note.getAn_page());
			columns.add(note.getUser_id());
			columns.add(note.getStudent_nm());
			
			columns.add(getDate(note.getReg_date()));
			columns.add(getDate(note.getAn_request_date()));
			columns.add(getDate(note.getAn_approve_date()));
			
			columns.add(note.getSt_nm());
			
			switch (note.getInspect_state()) {
				case 10:
					columns.add("미확인");
					break;
				case 20:
					columns.add("검수완료");
					break;
				case 30:
					columns.add("검수거절");
					break;
				default:
					break;
			}

			columns.add(note.getInspect_reason());
			
			results.add(columns);
		}
		
		return results;
	}

	private String getDate(String date) throws ParseException {
		if(StringUtils.isBlank(date)){
			return "-";
		}else{
			return DateFormatUtils.format(DateUtils.parseDate(date, new String[]{"yyyy-MM-dd HH:mm:ss.sss"}), "yyyy-MM-dd HH:mm");
		}
	}
	
	@RequestMapping("/inspect/logs")
    public @ResponseBody List<InspectLog> findByNoteLogs(String anId) throws Exception {

        logger.info("=======================================");
        logger.info("* findByNoteLogs anId[{}] ", anId);
        logger.info("=======================================");

        List<InspectLog> logs = Lists.newArrayList();
        logs = noteMapper.findInspectByLogs(anId);

        logger.info("* logs {}", logs.toString());

        return logs;
    
	}
	
	@RequestMapping("/inspect/detail")
    public @ResponseBody Map<String, Object> findByNoteDetail(
    		@RequestParam("anId") String anId) throws Exception {
    	
    	Stopwatch stopwatch = Stopwatch.createStarted();
    	
    	logger.info("=======================================");
    	logger.info("* findByNoteDetail anId[{}] ", anId);
    	logger.info("=======================================");
    	
    	Note note = noteMapper.findByAnId(anId);

        logger.info("* note {}", note.toString());

        String path = "";
        if (note.getAn_state() < 1) {
            path = NOTE_BASE_URL + note.getAn_img_path();
        } else {
            path = NOTE_BASE_URL + note.getWr_img_path();

            if (StringUtils.isBlank(path)) {
                path = NOTE_BASE_URL;
            }
        }
        
        //검수사유
        Inspect inspect = noteMapper.findInspectByNoteId(anId);

        Map<String, Object> result = Maps.newLinkedHashMap();
		result.put("note", note);
		result.put("path", path);
		result.put("inspect", inspect);
		
		stopwatch.stop();
		logger.info(">>> elapsed time  [{}]", stopwatch);
    	
    	return result;
    	
    }
	
	@RequestMapping("/inspect/remove")
    public @ResponseBody Map<String, Object> remove(
    		@RequestParam(value = "anId", required = true) String id) {
    	
    	logger.info("* INSPECT REMOVE [{}]", id);
    	
    	Integer count = noteMapper.deleteInspect(id);
    	
    	Map<String, Object> result = Maps.newLinkedHashMap();
    	result.put("data", count);
    	
    	return result;
    	
    }
	
	@RequestMapping("/inspect/save")
	public @ResponseBody Map<String, Object> save(Inspect req) {
		
		Stopwatch stopwatch = Stopwatch.createStarted();
		
		logger.info("=======================================");
		logger.info(">>> 검수조건 type[{}] rate[{}] fee[{}]");
		logger.info("=======================================");
		
		Admin admin = details.getUserInfo();
		req.setAdmin_id(admin.getAdmin_id());
		
		Integer count = mapper.updateInspect(req);
		logger.info(">>> INSPECT UPDATE COUNT[{}]", count);
		
		Map<String, Object> result = Maps.newLinkedHashMap();
		result.put("data", count);
		
		List<Inspect> log = Lists.newArrayList();
		log.add(req);
		mapper.insertInspectLog(log);
		
		stopwatch.stop();
		logger.info(">>> elapsed time  [{}]", stopwatch);
		
		return result;
	}

}
