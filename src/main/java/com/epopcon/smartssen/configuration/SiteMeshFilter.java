package com.epopcon.smartssen.configuration;

import org.sitemesh.builder.SiteMeshFilterBuilder;
import org.sitemesh.config.ConfigurableSiteMeshFilter;
import org.sitemesh.content.tagrules.html.Sm2TagRuleBundle;

public class SiteMeshFilter extends ConfigurableSiteMeshFilter {

	@Override
	protected void applyCustomConfiguration(SiteMeshFilterBuilder builder) {
		builder.addDecoratorPath("/*", "/WEB-INF/sitemesh/decorator.jsp");
		
		// Exclude path from decoration.
		builder.addExcludedPath("/login");
		builder.addExcludedPath("/login/*");
		
		builder.addExcludedPath("/api");
		builder.addExcludedPath("/api/*");
		
		builder.addTagRuleBundle(new Sm2TagRuleBundle());
	}

}
