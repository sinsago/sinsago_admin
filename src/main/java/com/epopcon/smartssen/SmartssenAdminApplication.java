package com.epopcon.smartssen;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.epopcon.smartssen.mapper")
public class SmartssenAdminApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmartssenAdminApplication.class, args);
	}
}
