package com.epopcon.smartssen.service;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.epopcon.smartssen.domain.Note;
import com.epopcon.smartssen.domain.PushToken;
import com.epopcon.smartssen.mapper.NoteMapper;
import com.epopcon.smartssen.mapper.TeacherMapper;
import com.google.android.gcm.server.InvalidRequestException;
import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;

@Service
public class NotificationService {
	
	private static final Logger logger = LoggerFactory.getLogger(NotificationService.class);
	
	@Value("${properties.push.gcm_server_key}")
	private String GCM_SERVER_KEY;
	
	@Value("${properties.push.gcm_api_url}")
	private String GCM_API_URL;
	
	@Value("${properties.push.title}")
	private String PUSH_TITLE;
	
	@Value("${properties.push.message.waiting}")
	private String PUSH_MESSAGE_WAITING;
	
	@Value("${properties.push.message.running}")
	private String PUSH_MESSAGE_RUNNING;
	
	@Value("${properties.push.message.pending}")
	private String PUSH_MESSAGE_PENDING;
	
	@Value("${properties.push.message.blocked}")
	private String PUSH_MESSAGE_BLOCKED;
	
	@Value("${properties.push.message.approve}")
	private String PUSH_MESSAGE_APPROVE;
	
	@Autowired
	private TeacherMapper teacherMapper;
	
	@Autowired
	private NoteMapper noteMapper;
	
	@Autowired
	private AnswerNoteService noteService;
	
	public void sendMessageByAssignTeacher(String stId) {
		
		PushToken pushToken = new PushToken(); 
		pushToken = teacherMapper.findByPushToken(stId);
		
		if(pushToken != null){
			logger.info("* Send Push to {}", pushToken.toString());
			
			try {
				
				boolean isSend = false;
				
				isSend = pushNotificationToGCM(pushToken.getGcm_token(), PUSH_TITLE, PUSH_MESSAGE_WAITING);
				
				if(isSend){
					saveSendMessageLog(stId, PUSH_MESSAGE_WAITING);
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}else{
			logger.info("* none registrationId - teacher {}", stId);
		}
	}
	
	public void sendMessageByStId(String stId, String message) {
		try {
			PushToken pushToken = new PushToken(); 
			pushToken = teacherMapper.findByPushToken(stId);
			
			if(pushToken != null){
				logger.info("* Send Push to {}", pushToken.toString());
				
				boolean isSend = false;
				
				isSend = pushNotificationToGCM(pushToken.getGcm_token(), PUSH_TITLE, message);
				
				if(isSend){
					saveSendMessageLog(stId, message);
				}
				
				
			}else{
				logger.info("* none registrationId - teacher {}", stId);
			}
		} catch (Exception e) {
			//e.printStackTrace();
			
			logger.info("* send message fail [{}]", e.getCause());
		}
	}

	private void saveSendMessageLog(String stid, String message) {
		
		logger.info("* Saved Push Message Log...");
		logger.info("* Saved Push Stid [{}] Message [{}]", stid, message);
		
		try {
			
			noteMapper.savePushMessage(stid, message);
			
		} catch (Exception e) {
			logger.info("* send message fail [{}]", e.getCause());
		}
		
	}
	
	public boolean pushNotificationToGCM(String registrationId, String title, String message){
        
        final int retries = 3;
        
        Sender sender = new Sender(GCM_SERVER_KEY);
        Message msg = new Message.Builder()
        		.addData("title", title)
        		.addData("message",message)
        		.build();
        
        logger.info("* push message [{}]", msg.toString());

        try {
            if(StringUtils.isNotBlank(registrationId)) {
            	
                Result result = sender.send(msg, registrationId, retries);
                /**
                * if you want to send to multiple then use below method
                * send(Message message, List<String> regIds, int retries)
                **/

                if (StringUtils.isEmpty(result.getErrorCodeName())) {
                    logger.info("* GCM Notification is sent successfully [{}]", result.toString());
                    return true;
                }

                logger.info("* Error occurred while sending push notification : {}", result.getErrorCodeName());

            }
        } catch (InvalidRequestException e) {
        	logger.info("* Invalid Request");
        } catch (IOException e) {
        	logger.info("* IO Exception");
        }
        return false;
    }

	public void sendMessageByNoteState(String anId, Integer anState) {
		
		//send push message
		
		Note note = noteMapper.findByAnId(anId);
		
		if(ObjectUtils.isEmpty(note)){
			logger.info("* not found note info...");
		}else{
			String message = "";
			
			switch (anState) {
			case 0:
				//message = PUSH_MESSAGE_WAITING;
				//sendMessageByStId(note.getSt_id(), message);
				
				break;
			case 1:
				//message = PUSH_MESSAGE_RUNNING;
				//sendMessageByStId(note.getSt_id(), message);
				
				break;
			case 2:
				//message = PUSH_MESSAGE_PENDING;
				//sendMessageByStId(note.getSt_id(), message);
				
				break;
			case 3:
				message = PUSH_MESSAGE_BLOCKED;
				sendMessageByStId(note.getSt_id(), message);
				
				break;
			case 4:
				//message = PUSH_MESSAGE_APPROVE;
				//sendMessageByStId(note.getSt_id(), message);
				
				try {
					noteService.sendRestfulApprove(note);
				} catch (IOException e) {
					logger.error("* [WR image] [{}]", e.getMessage());
				}
				
				break;
			default:
				//message = PUSH_MESSAGE_WAITING;
				//sendMessageByStId(note.getSt_id(), message);
				
				break;
			}
		}
	}
}
