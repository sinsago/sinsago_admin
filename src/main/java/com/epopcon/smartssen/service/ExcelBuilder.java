package com.epopcon.smartssen.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ObjectUtils;
import org.springframework.web.servlet.view.document.AbstractExcelView;

public class ExcelBuilder extends AbstractExcelView {

	private static final Logger logger = LoggerFactory.getLogger(ExcelBuilder.class);
	
	@Override
	protected void buildExcelDocument(
			Map<String, Object> model
			, HSSFWorkbook workbook
			, HttpServletRequest request
			, HttpServletResponse response) throws Exception {
		
		//VARIABLES REQUIRED IN MODEL
        String sheetName = (String)model.get("sheetname");
        
        List<Object> headers = (List<Object>)model.get("headers");
        List<List<Object>> results = (List<List<Object>>)model.get("results");
        
        //BUILD DOC
        HSSFSheet sheet = workbook.createSheet(sheetName);
        sheet.setDefaultColumnWidth((short) 12);
        
        int currentRow = 0;
        short currentColumn = 0;
        
        //CREATE STYLE FOR HEADER
        HSSFCellStyle headerStyle = workbook.createCellStyle();
        HSSFFont headerFont = workbook.createFont();
        headerFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        headerStyle.setFont(headerFont); 
        
        //POPULATE HEADER COLUMNS
        HSSFRow headerRow = sheet.createRow(currentRow);
        for(Object header : headers){
            HSSFRichTextString text = new HSSFRichTextString(String.valueOf(header));
            HSSFCell cell = headerRow.createCell(currentColumn); 
            cell.setCellStyle(headerStyle);
            cell.setCellValue(text);            
            currentColumn++;
        }
        
        //POPULATE VALUE ROWS/COLUMNS
        currentRow++;//exclude header
        for(List<Object> result: results){
            currentColumn = 0;
            HSSFRow row = sheet.createRow(currentRow);
            for(Object value : result){//used to count number of columns
                HSSFCell cell = row.createCell(currentColumn);
                
                //logger.debug(">>> Rows Data [{}]", value);
                
                if(ObjectUtils.isEmpty(value)) value = "N/N";
                
                HSSFRichTextString text = new HSSFRichTextString(String.valueOf(value));                
                cell.setCellValue(text);
                currentColumn++;
            }
            currentRow++;
        }
    }
}
