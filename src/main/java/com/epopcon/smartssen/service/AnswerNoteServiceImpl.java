package com.epopcon.smartssen.service;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.epopcon.smartssen.domain.Admin;
import com.epopcon.smartssen.domain.Article;
import com.epopcon.smartssen.domain.Comment;
import com.epopcon.smartssen.domain.Note;
import com.epopcon.smartssen.domain.NoteLog;
import com.epopcon.smartssen.domain.UserTypeEnum;
import com.epopcon.smartssen.mapper.NoteMapper;

@Service
public class AnswerNoteServiceImpl implements AnswerNoteService {

	private static final Logger logger = LoggerFactory.getLogger(AnswerNoteServiceImpl.class);
	
	@Autowired
	private NoteMapper noteMapper;
	
	@Autowired
	private NotificationService nfs;
	
	@Autowired
	private FileService fileService;
	
	@Value("${properties.note.update-uri}")
	private String NOTE_UPDATE_URI;
	
	@Override
	public Long saveByAnswerNoteLog(String anId, Integer anState) {
		
		logger.info("* addAnswerNoteLog Answer Note Id[{}] State[{}]", anId, anState);
		
		NoteLog log; 
		
		if(anState < 1){
			log = new NoteLog(anId, anState, UserTypeEnum.ADMIN.getValue(), "");
		}else{
			LoginUserDetails user = (LoginUserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			Admin admin = user.getUser();

			logger.info("session Id[{}] Name[{}]", admin.getAdmin_id(), admin.getAdmin_nm());
			log = new NoteLog(anId, anState, UserTypeEnum.ADMIN.getValue(), admin.getAdmin_id());
		}
		
        Long count = noteMapper.saveByNoteLog(log);
        
        return count;
	}

	@Override
	public void updateStateByNote(Note note) {
		updateStateByIds(new String[]{note.getAn_id()}, note.getAn_state(), note.getSt_id());
	}
	
	private Long updateStateByIds(String[] anIds, Integer state, String stId) {
		
		Long result = 0L;
		
		result = noteMapper.updateStateAndStIdByIds(anIds, state, stId);
		
		for(String id : anIds){
			
			//add log
			saveByAnswerNoteLog(id, state);
			
			//send push message
			nfs.sendMessageByNoteState(id, state);
		}
		
		return result;
		
	}

	@Override
	public Long updateStateByIds(String[] anIds, Integer state) {
		
		Long result = 0L;
		result = noteMapper.updateStateByIds(anIds, state);
		
		for(String id : anIds){
			
			//add log
			saveByAnswerNoteLog(id, state);
			
			//send push message
			nfs.sendMessageByNoteState(id, state);
		}
		
		return result;
	}
	
	@Override
	public Long changeTeacher(String[] anIds, Integer state, String stId) {
		
		Long result = 0L;
		result = noteMapper.updateNoteTeacherByStId(anIds, stId);
		
		for(String id : anIds){
			
			//add log
			saveByAnswerNoteLog(id, state);
			
			//send push message
			nfs.sendMessageByNoteState(id, state);
		}
		
		return result;
	}

	@Override
	public void saveByNote(Note note) {
		
		noteMapper.saveByNote(note);
		
		//add log
		saveByAnswerNoteLog(note.getAn_id(), note.getAn_state());
		
		//send push message
		nfs.sendMessageByNoteState(note.getAn_id(), note.getAn_state());
		
	}

	@Override
	public void sendRestfulApprove(Note note) throws IOException {

		logger.info("============================");
		logger.info("* Send Restful Approve Note [{}]", note.toString());

		// get file

		URI uri = URI.create(NOTE_UPDATE_URI);

		// Path path =
		// Paths.get("D:\\epopcon\\images\\smartssen\\201610\\27\\1610-S000001-13968786024930228.png");
		Path path = fileService.getWrNoteFile(note);

		logger.info("* file exists [{}]", path.toFile().exists());

		MultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();
		map.add("an_id", note.getAn_id());
		// map.add("wr_img_file", file); // MultipartFile

		ByteArrayResource contentsAsResource = new ByteArrayResource(Files.readAllBytes(path)) {
			@Override
			public String getFilename() {
				return path.getFileName().toString();
			}
		};
		map.add("wr_img_file", contentsAsResource);

		RestTemplate restTemplate = new RestTemplate();
		Article response = restTemplate.postForObject(uri, map, Article.class);

		logger.info("* response [{}]", response.getResult());

		Comment comment = response.getResult();

		logger.info("* code [{}]", comment.getCode());
		logger.info("* message [{}]", comment.getMessage());
		logger.info("* server date [{}]", comment.getServer_date());

		logger.info("============================");
	}
}
