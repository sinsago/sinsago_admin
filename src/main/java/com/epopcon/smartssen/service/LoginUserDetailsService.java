package com.epopcon.smartssen.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.epopcon.smartssen.domain.Admin;
import com.epopcon.smartssen.mapper.ManagerMapper;

@Service
public class LoginUserDetailsService implements UserDetailsService {
	
	@Autowired 
	private ManagerMapper managerMapper;

	@Override
	public UserDetails loadUserByUsername(String id) throws UsernameNotFoundException {
		
		Admin user = managerMapper.findManagerById(id);
		if(user == null){
			throw new UsernameNotFoundException("The requested user is not found.");
		}
		return new LoginUserDetails(user);
	}
	
	public Admin getUserInfo(){
		
		LoginUserDetails user = (LoginUserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Admin admin = user.getUser();
		
		return admin;
	}
}
