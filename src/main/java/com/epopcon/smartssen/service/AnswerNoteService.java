package com.epopcon.smartssen.service;

import java.io.IOException;

import com.epopcon.smartssen.domain.Note;

public interface AnswerNoteService {

	public Long saveByAnswerNoteLog(String an_id, Integer an_state);

	public void updateStateByNote(Note note);

	public Long updateStateByIds(String[] anIds, Integer state);

	public void saveByNote(Note note);

	public void sendRestfulApprove(Note note) throws IOException;

	public Long changeTeacher(String[] anIds, Integer state, String stId);
}
