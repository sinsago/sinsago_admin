package com.epopcon.smartssen.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epopcon.smartssen.domain.Admin;
import com.epopcon.smartssen.domain.GradeCharge;
import com.epopcon.smartssen.mapper.ManagerMapper;
import com.google.common.collect.Lists;

@Component
public class ManagementService {
	
	private static final Logger logger = LoggerFactory.getLogger(ManagementService.class);
	
	@Autowired
	private ManagerMapper managerMapper;
	
	public List<GradeCharge> findGradeCharge(String id) {
		
		List<GradeCharge> result = Lists.newArrayList();
		result = managerMapper.findGradeCharge(new GradeCharge(id));
		
		logger.info(">>> find grade charge admin[{}] [{}]", id, result.toString());
		
		return result;
	}

	public void gradeCharge(Admin admin, List<GradeCharge> list) {
		
		//delete all
		Integer dCount = 
		managerMapper.deleteGradeCharge(admin.getAdmin_id());
		
		//insert all
		Integer iCount = 
		managerMapper.insertGradeCharge(admin.getAdmin_id(), list);
		
		
		
	}
}