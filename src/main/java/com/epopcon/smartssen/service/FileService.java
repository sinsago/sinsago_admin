package com.epopcon.smartssen.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.epopcon.smartssen.domain.Note;

@Service
public class FileService {
	
	private static final Logger logger = LoggerFactory.getLogger(FileService.class);
	
	@Value("${properties.note-url.base}")
	private String NOTE_BASE_URL;
	
	@Value("${properties.note-url.resources}")
	private String NOTE_RESOURCE_URL;
	
	@Value("${properties.note-file.base}")
	private String NOTE_BASE_PATH;
	
	@Value("${properties.app.base}")
	private String APP_BASE_PATH;
	
	@Value("${properties.app.url}")
	private String APP_BASE_URL;
	
	public String storeOnDisk(MultipartFile multipartFile) throws IllegalStateException, IOException {
		
		
		String fileName = String.valueOf(System.currentTimeMillis());

		//save file
		//D:\epopcon\images\smartssen\201610\17\1476673466357.png
		File storePath = storeFile(multipartFile, fileName);
		
		//System.out.println(storePath.toURI().toString().replace("file:/D:/epopcon/images/smartssen/", "http://127.0.0.1:8088/resource/"));
		String urlPath = storePath.toURI().toString().replace(NOTE_RESOURCE_URL, "");
		
		logger.info("* note store path [{}]", storePath);
		logger.info("* note url path [{}]", urlPath);
		
		return urlPath;
	}
	
	public String storeByApplication(MultipartFile multipartFile) throws IllegalStateException, IOException {
		
		
		//String prefix = String.valueOf(System.currentTimeMillis())+File.separator;
		String prefix = DateFormatUtils.format(new Date(), "yyyyMMdd")+"/";
		
		File parent = new File(APP_BASE_PATH+prefix);
		if(!parent.exists()){
			parent.mkdirs();
		}
		
		//String fileName = String.valueOf(System.currentTimeMillis());
		String fileName = multipartFile.getOriginalFilename();

		//fin file
		Path path = Paths.get(parent.getAbsolutePath(),fileName);
		
		//store
		multipartFile.transferTo(path.toFile());
		
		return prefix+fileName;
	}
	
	private File storeFile(MultipartFile multipartFile, String fileName) throws IllegalStateException, IOException {
		/*
		String yyyyMM = getDateFormat("yyyyMM");
		String dd = getDateFormat("dd");
		*/
		String yyyyMM = DateFormatUtils.format(new Date(), "yyyyMM");
		String dd = DateFormatUtils.format(new Date(), "dd");
		
		//has yyyyMM/dd folder?
		File parent = Paths.get(NOTE_BASE_PATH, yyyyMM, dd).toFile();
		if(!parent.exists()){
			parent.mkdirs();
		}
		
		//fin file
		File file = new File(parent,fileName);
		
		//store
		multipartFile.transferTo(file);
		
		return file;
		
	}

	public String getFilePathFromUri(String uri) {
		uri = uri.replace("/resource/", NOTE_BASE_PATH);
		return Paths.get(uri).toString();
	}
	
	public Path getWrNoteFile(Note note) {
		return Paths.get(NOTE_BASE_PATH+note.getWr_img_path());
	}

	public String getNoImageFile() {
		
		return NOTE_BASE_PATH+"noImage.png";
	}

	public String saveImage(String imageUrl) throws IOException {
		
		
		String fileName = String.valueOf(System.currentTimeMillis());
		
		String yyyyMM = DateFormatUtils.format(new Date(), "yyyyMM");
		String dd = DateFormatUtils.format(new Date(), "dd");
		
		//has yyyyMM/dd folder?
		File parent = Paths.get(NOTE_BASE_PATH, yyyyMM, dd).toFile();
		if(!parent.exists()){
			parent.mkdirs();
		}
		
		String path = parent.getAbsolutePath()+"\\"+fileName;
		
		URL url = new URL(imageUrl);
		InputStream is = url.openStream();
		OutputStream os = new FileOutputStream(path);

		byte[] b = new byte[2048];
		int length;

		while ((length = is.read(b)) != -1) {
			os.write(b, 0, length);
		}

		is.close();
		os.close();
		
		
		
		
		return "/"+yyyyMM+"/"+dd+"/"+fileName;
	}
	
	public String saveImage(Note note) throws Exception {
		
		String fileName = note.getAn_id()+"-"+String.valueOf(System.currentTimeMillis()+".png");
		
		String yyyyMM = DateFormatUtils.format(new Date(), "yyyyMM");
		String dd = DateFormatUtils.format(new Date(), "dd");
		
		//has yyyyMM/dd folder?
		File parent = Paths.get(NOTE_BASE_PATH, yyyyMM, dd).toFile();
		if(!parent.exists()){
			parent.mkdirs();
		}
		
		String path = parent.getAbsolutePath()+"\\"+fileName;
		
		URL url = new URL(note.getFile_note());
		InputStream is = url.openStream();
		OutputStream os = new FileOutputStream(path);

		byte[] b = new byte[2048];
		int length;

		while ((length = is.read(b)) != -1) {
			os.write(b, 0, length);
		}

		is.close();
		os.close();
		
		return "/"+yyyyMM+"/"+dd+"/"+fileName;
	}
	
	
	
}
