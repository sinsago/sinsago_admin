package com.epopcon.smartssen.service;

import org.springframework.security.core.authority.AuthorityUtils;

import com.epopcon.smartssen.domain.Admin;

public class LoginUserDetails extends org.springframework.security.core.userdetails.User {

	private static final long serialVersionUID = 1L;

	private final Admin admin;
	
	public LoginUserDetails(Admin admin) {
		super(admin.getAdmin_id(), admin.getAdmin_pwd(), AuthorityUtils.createAuthorityList("ROLE_USER"));
		this.admin = admin;
	}

	public Admin getUser() {
		return this.admin;
	}
}