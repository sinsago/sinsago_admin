package com.epopcon.smartssen.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.epopcon.smartssen.domain.Period;
import com.epopcon.smartssen.domain.TeacherTest;
import com.epopcon.smartssen.domain.Test;
import com.epopcon.smartssen.domain.TestRating;
import com.epopcon.smartssen.mapper.TeacherMapper;
import com.google.common.collect.Lists;

@Service
public class ScheduledService {
	
	private static final Logger logger = LoggerFactory.getLogger(ScheduledService.class);
	
	@Autowired
	private TeacherMapper mapper;

	/*
	 * 일별 평가 수수료
	 */
	public void daily() {
		
		logger.debug(">>> daily fee >>>");
		mapper.saveDaily();
	}

	/*
	 * 교사 등급 평가
	 */
	public void rating() {
		
		List<TeacherTest> fl = Lists.newArrayList();
		fl = mapper.findTeacherTest();
		
		logger.info("* Teacher Test Task size [{}]", fl.size());
		
		if(fl.size() > 0){
			
			List<TestRating> tl = Lists.newArrayList(); 
			for(TeacherTest t : fl){

				TestRating rating = new TestRating();
				rating = mapper.findByTeacherTest(t);
				
				if(!ObjectUtils.isEmpty(rating)){
					tl.add(rating);
				}
			}
			
			if(tl.size() > 0){
				//make test info
				mapper.saveTeacherTest(makeTeacherTestRating(tl));
			}
		}
		
	}
	
	
	private List<Test> makeTeacherTestRating(List<TestRating> tl) {
		
		List<Test> result = Lists.newArrayList();
		
		for(TestRating t : tl){
			
			Test bean = new Test();
			
			
			bean.setSt_id(t.getSt_id());
			bean.setTest_date(t.getNext_day());
			
			String rating = testRatingHandler(t);
			bean.setTest_rating(rating);
			
			bean.setApprove_cnt(t.getCount_a());
			bean.setKeep_deadline_cnt(t.getCount_b());
			bean.setOver_deadline_cnt(t.getCount_c());
			bean.setGive_back_cnt(t.getCount_e());
			
			Integer fee = testFeeHandler(rating);
			bean.setTest_fee(fee);
			
			bean.setApply_date(t.getApply_day());
			
			
			result.add(bean);
			
		}
		
		
		
		return result;
	}
	
	private Integer testFeeHandler(String rating) {
		
		Integer fee = 0;
		switch (rating) {
		case "A":
			fee = 50;
			break;
		case "B":
			fee = 0;
			break;
		case "C":
			fee = -50;
			break;
		default:
			fee = 0;
			break;
		}
		
		
		return fee;
	}

	private String testRatingHandler(TestRating t) {
		
		Integer score = 0;
		if(t.getKeep_rate() == 100) {
			score = 3;
		}else if(t.getKeep_rate() < 100 && t.getKeep_rate() >= 97){
			score = 2;
		}else if(t.getKeep_rate() < 97 && t.getKeep_rate() >= 94){
			score = 1;
		}
		
		if(t.getApprove_rate() >= 95) {
			score  += 3;
		}else if(t.getApprove_rate() < 95 && t.getApprove_rate() >= 85){
			score += 2;
		}else if(t.getApprove_rate() < 85 && t.getApprove_rate() >= 75){
			score += 1;
		}
		
		String rating;
		if(score <= 6 && score >=  5) {
			rating = "A";
		}else if(score == 4){
			rating = "B";
		}else if(score <= 3 && score >=  2){
			rating = "C";
		}else{
			rating = "F";
		}
		
		return rating;
	}

	public void updateEvaluation() {
		
		List<Test> tests = Lists.newArrayList();
		tests = mapper.findTeacherByTestFee();
		
		logger.info("* Find Teacher By Test Fees Size [{}]", tests.size());
		
		Integer count = 0;
		if(tests.size() > 0){
			
			count = mapper.saveTeacherByTestFee(tests);
			
			logger.info("* Find Teacher By Test Fees... [{}]", tests.toString());
			logger.info("* Save Teacher By Test Fees Count [{}]", count);
			
			//mapper.saveTeacherPeriodFeeByState(periods);
			
		}else{
			logger.info("* Not Found Teacher By Tests Fees ...");
		}
		
	}

	public void updatePeriod() {
		
		// 적용일이 오늘인 리스트를 가져온다
		List<Period> periods = Lists.newArrayList();
		periods = mapper.findTeacherByPeriodFee();
		
		logger.info("* Find Teacher By Period Fees Size [{}]", periods.size());
		
		Integer count = 0;
		if(periods.size() > 0){
			
			count = mapper.saveTeacherByPeriodFee(periods);
			
			logger.info("* Find Teacher By Period Fees... [{}]", periods.toString());
			logger.info("* Save Teacher By Period Fees Count [{}]", count);
			
			mapper.saveTeacherPeriodFeeByState(periods);
			
			
		}else{
			logger.info("* Not Found Teacher By Period Fees ...");
		}
		
	}

	public void period() {
		
		Integer count;
		//365일 근무 기준
		count = mapper.saveByWorkingPeriodTeacher();
		
		logger.info("* save working days teacher count[{}]", count);
		
	}

}
