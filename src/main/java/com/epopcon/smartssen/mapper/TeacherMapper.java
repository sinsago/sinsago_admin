package com.epopcon.smartssen.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.epopcon.smartssen.domain.AddedFee;
import com.epopcon.smartssen.domain.Fee;
import com.epopcon.smartssen.domain.Leave;
import com.epopcon.smartssen.domain.NoteStatistics;
import com.epopcon.smartssen.domain.Period;
import com.epopcon.smartssen.domain.PushToken;
import com.epopcon.smartssen.domain.Teacher;
import com.epopcon.smartssen.domain.TeacherTest;
import com.epopcon.smartssen.domain.Test;
import com.epopcon.smartssen.domain.TestRating;
import com.epopcon.smartssen.domain.TodaysFee;

public interface TeacherMapper {

	List<Teacher> findByTeachers(
		@Param("searchType") Integer searchType
		, @Param("searchWord") String searchWord
		, @Param("startCnt") Integer startCnt
		, @Param("endCnt") Integer endCnt
		, @Param("mngrId") String mngrId
	);

	Integer countById(String id);

	Integer countByName(String name);

	Long createByTeacher(Teacher teacher);
	
	Long updateByTeacher(Teacher teacher);

	Teacher findByStId(String stId);

	List<Period> findByPeriod(String stId);

	List<Test> findByTest(String stId);

	List<Teacher> findAllTeachers();

	Long findByLastPunchTicket();

	Teacher findByTeacherSeq(
			@Param("seq") Long seq
			, @Param("type") Integer type
			, @Param("grade") Integer grade);

	void updateByPunchTicket(Long seq);

	PushToken findByPushToken(String stId);

	NoteStatistics findByNoteStatistic(NoteStatistics noteStatistics);

	List<Period> findTeacherByPeriod(
			@Param("standard") Integer standard
			, @Param("increase") Integer increase);

	Integer saveTeacherByPeriod(List<Period> periods);

	List<Period> findTeacherByPeriodFee();

	Integer saveTeacherByPeriodFee(List<Period> periods);

	Integer saveTeacherPeriodFeeByState(List<Period> periods);

	List<Teacher> findTeachersByState(Integer state);

	List<TeacherTest> findTeacherTest();

	Integer saveTeacherTest(List<Test> list);

	List<Fee> findTeacherMonthlyFee();

	Integer saveTeacherMonthlyFee(List<Fee> fl);

	void saveTeacherLeave(Teacher teacher);

	Leave findTeacherLeave(Teacher teacher);

	void updateByTeacherLeave(Teacher teacher);

	List<Test> findTeacherByTestFee();

	Integer saveTeacherByTestFee(List<Test> tests);

	Integer saveByWorkingPeriodTeacher();

	TestRating findByTeacherTest(TeacherTest t);

	Integer saveAddedFee(AddedFee added);

	void updateByAddedFeeState(AddedFee added);

	TodaysFee findByTodaysFee(String stId);

	void saveDaily();

	void deleteDailyFee(AddedFee added);

	void updateDailyFee(AddedFee added);
	
}
