package com.epopcon.smartssen.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.epopcon.smartssen.domain.Inspect;
import com.epopcon.smartssen.domain.InspectLog;
import com.epopcon.smartssen.domain.Note;
import com.epopcon.smartssen.domain.NoteLog;
import com.epopcon.smartssen.domain.Refuse;

public interface NoteMapper {

	List<Note> findByNotes(
		@Param("state") Integer state
		, @Param("searchType") Integer searchType
		, @Param("searchWord") String searchWord
		, @Param("searchStart") String searchStart
		, @Param("searchEnd") String searchEnd
		, @Param("startCnt") Integer startCnt
		, @Param("endCnt") Integer endCnt);
	
	Map<String, Long> countByNoteState(
		@Param("state") Integer state
		, @Param("searchType") Integer searchType
		, @Param("searchWord") String searchWord
		, @Param("searchStart") String searchStart
		, @Param("searchEnd") String searchEnd
		, @Param("startCnt") Integer startCnt
		, @Param("endCnt") Integer endCnt);

	Note findByAnId(String anId);

	List<NoteLog> findByNoteLogs(String anId);

	Long updateByNote(Note note);

	Long saveByNoteLog(NoteLog log);

	Long saveByNote(Note note);

	Long updateStateByIds(
		@Param("anIds") String[] anIds
		, @Param("state") Integer state);

	Long updateStateAndStIdByIds(
			@Param("anIds") String[] anIds
			, @Param("state") Integer state
			, @Param("stId") String stId);

	List<Note> findByNotes(
			@Param("state") Integer state
			, @Param("searchType") Integer searchType
			, @Param("searchWord") String searchWord
			, @Param("searchStart") String searchStart
			, @Param("searchEnd") String searchEnd
			, @Param("startCnt") Integer startCnt
			, @Param("endCnt") Integer endCnt
			, @Param("mngrId") String mngrId);

	Map<String, Long> countByNoteState(
			@Param("state") Integer state
			, @Param("searchType") Integer searchType
			, @Param("searchWord") String searchWord
			, @Param("searchStart") String searchStart
			, @Param("searchEnd") String searchEnd
			, @Param("startCnt") Integer startCnt
			, @Param("endCnt") Integer endCnt
			, @Param("mngrId") String mngrId);

	Integer savePushMessage(
			@Param("stId") String stid
			, @Param("message") String message);

	List<Note> findByNotes(
			@Param("state") Integer state
			, @Param("searchType") Integer searchType
			, @Param("searchWord") String searchWord
			, @Param("searchStart") String searchStart
			, @Param("searchEnd") String searchEnd
			, @Param("startCnt") Integer startCnt
			, @Param("endCnt") Integer endCnt
			, @Param("mngrId") String mngrId
			
			, @Param("dateType") Integer dateType
			, @Param("studentType") Integer studentType
			, @Param("gradeType") Integer gradeType);

		Map<String, Long> countByNoteState(
			@Param("state") Integer state
			, @Param("searchType") Integer searchType
			, @Param("searchWord") String searchWord
			, @Param("searchStart") String searchStart
			, @Param("searchEnd") String searchEnd
			, @Param("startCnt") Integer startCnt
			, @Param("endCnt") Integer endCnt
			, @Param("mngrId") String mngrId
			, @Param("dateType") Integer dateType
			, @Param("studentType") Integer studentType
			, @Param("gradeType") Integer gradeType);
		
		Long updateNoteTeacherByStId(
				@Param("anIds") String[] anIds
				, @Param("stId") String stId);

		List<Refuse> findByNoteRefuse(Refuse refuse);

		Integer deleteRefuse(Refuse refuse);
		
		Integer updateRefuse(Refuse refuse);
		
		Integer insertRefuse(Refuse refuse);

		List<Note> findInspectByNotes(
				@Param("searchType") Integer searchType
				, @Param("searchWord") String searchWord
				, @Param("searchStart") String searchStart
				, @Param("searchEnd") String searchEnd
				, @Param("startCnt") Integer startCnt
				, @Param("endCnt") Integer endCnt
				, @Param("dateType") Integer dateType
				, @Param("studentType") Integer studentType
				, @Param("gradeType") Integer gradeType
				, @Param("inspectType") Integer inspectType);

		List<InspectLog> findInspectByLogs(String anId);

		Inspect findInspectByNoteId(String anId);

		Integer deleteInspect(String id);

		Integer insertRefuseLog(
				@Param("anId") String anId
				, @Param("adminId") String adminId
				, @Param("state") Integer state);
		
}
