package com.epopcon.smartssen.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.epopcon.smartssen.domain.Admin;
import com.epopcon.smartssen.domain.App;
import com.epopcon.smartssen.domain.ApproveManager;
import com.epopcon.smartssen.domain.Fee;
import com.epopcon.smartssen.domain.GradeCharge;
import com.epopcon.smartssen.domain.Inspect;
import com.epopcon.smartssen.domain.InspectFee;
import com.epopcon.smartssen.domain.Teacher;


public interface ManagerMapper{

	Admin findManagerById(String adminId);
	
	List<Admin> findAllManagers();
	
	List<Admin> findByManagers(
		@Param("startCnt") Integer startCnt
		, @Param("endCnt") Integer endCnt);

	Integer countById(String stId);

	Integer countByName(String stName);

	Long updateByManager(Admin admin);

	Long createByManager(Admin admin);

	List<Teacher> findByManagedTeachers(
			@Param("managerId") String managerId
			, @Param("startCnt") Integer startCnt
			, @Param("endCnt") Integer endCnt);

	Long deleteByManager(@Param("id") String id);
	
	List<App> findByAppliction(
			@Param("startCnt") Integer startCnt
			, @Param("endCnt") Integer endCnt);

	Long createByAppliction(App app);
	
	Long deleteByAppliction(@Param("seq") Long seq);

	void updateByApplictionInactive();

	List<Fee> findByMonthlyFee(
			@Param("month") String month
			, @Param("mngrId") String mngrId);

	Long updateByFee(String month);

	String findAppPathBySeq(Long seq);

	List<Fee> findByTodayFee(
			@Param("mngrId") String mngrId
		);

	List<GradeCharge> findGradeCharge(GradeCharge charge);

	Integer deleteGradeCharge(String id);

	Integer insertGradeCharge(
			@Param("id") String id
			, @Param("list")  List<GradeCharge> list);

	Inspect findByInspectConfig();

	Integer updateInspectByConfig(Inspect req);

	Integer insertInspect(List<Inspect> list);

	Integer insertInspectLog(List<Inspect> list);

	List<Inspect> findInspectByRate(Inspect inspect);

	Integer updateInspect(Inspect req);

	List<InspectFee> findByMonthlyApproveFee(
			@Param("month") String month
			, @Param("mngrId") String mngrId);

	List<InspectFee> findByApproveFee(
			@Param("month") String month
			, @Param("mngrId") String mngrId);

	ApproveManager findApproveByState(String month);

	Long insertApproveManager(String month);

	List<Inspect> findInspectByStudentRate();

	List<Fee> findByMonthlyClosing(
			@Param("month") String month
			, @Param("mngrId") String mngrId);

	String findByMonthlyClosingComplete(String month);

	Long insertMonthlyAccounting(String month);

	List<String> findByMonthlyAccounting(String startDate);
	
}
