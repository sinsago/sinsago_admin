package com.epopcon.smartssen.scheduler;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.epopcon.smartssen.domain.Fee;
import com.epopcon.smartssen.domain.Inspect;
import com.epopcon.smartssen.mapper.ManagerMapper;
import com.epopcon.smartssen.mapper.TeacherMapper;
import com.epopcon.smartssen.service.ScheduledService;
import com.google.common.collect.Lists;

@EnableScheduling
@Component
public class ScheduledTasks {
	
	private static final Logger logger = LoggerFactory.getLogger(ScheduledTasks.class);
	
	@Autowired
	private TeacherMapper mapper;
	
	@Autowired
	private ManagerMapper managerMapper;
	
	@Autowired
	private ScheduledService service;
	
	@Value("${properties.fee.period.increase}")
	private Integer FEE_PERIOD_INCREASE;
	
	@Value("${properties.fee.period.standard}")
	private Integer FEE_PERIOD_STANDARD;
	
	@Value("${properties.inspect.state.ready}")
	private Integer INSPECT_STATE_READY;
	
	@Value("${properties.schedule.enabled}")
	private Boolean enable;
	
	@Scheduled(initialDelay=1000*60, fixedRate = 1000*60*60)
	private void testTask(){
		if(enable){
			logger.info(">>>>>>>>>>>>>> Health Check Up - TASK...");
		}
	}
	
	@Scheduled(cron = "0 0 1 * * ?")
	private void dailyTeacherFeeTask(){
		
		if(enable){
			logger.info("* Daily Teacher Fee Task... start >>> ");
			try {
				// 교사 근속평가
				service.period();
				
				// 교사 등급평가
				service.rating();
				
				// 일별 평가 수수료
				service.daily();
				
			} catch (Exception e) {
				logger.info("* Exception... Daily Teacher Fee Task Check! [{}]", e.getMessage());
			}
			logger.info("* Daily Teacher Fee Task... end <<< ");
		}
	}
	
	@Scheduled(cron = "0 0 2 1 * ?")
	private void updateTeacherFeeTask(){
		
		if(enable){
			logger.info("* Update Teacher-FeeTask... start >>> ");
			try {
				// 평가 수수료 업데이트
				service.updateEvaluation();
				
				// 근속 수수료 업데이트
				service.updatePeriod();
				
			} catch (Exception e) {
				logger.info("* Exception... Update Teacher-FeeTask Check! [{}]", e.getMessage());
			}
			logger.info("* Update Teacher-FeeTask... end <<< ");
		}
	}
	
	@Scheduled(cron = "0 0 3 1 * ?")
	private void teacherMonthlyFeeTask(){
		
		if(enable){
			logger.info("* Teacher MonthlyFee Task... start >>> ");
			try {
				List<Fee> fl = Lists.newArrayList();
				fl = mapper.findTeacherMonthlyFee();
				
				logger.info("* Teacher MonthlyFee Task size [{}]", fl.size());
				
				if(fl.size() > 0){
					Integer count = 0;
					count = mapper.saveTeacherMonthlyFee(fl);
					
					logger.info("* Teacher MonthlyFee Task saved count [{}]", count);
				}
				
			} catch (Exception e) {
				logger.info("* Exception... Teacher MonthlyFee Task Check! [{}]", e.getMessage());
				e.printStackTrace();
			}
			
			logger.info("* Teacher MonthlyFee Task... end <<< ");
		}
	}
	
	@Scheduled(cron = "0 30 3 * * *")
	private void inspectTask(){
		if(enable){
			logger.info(">>> Inspect Scheduled... start >>> ");
			
			try {
				logger.info(">>> Task...");
				
				Inspect config = managerMapper.findByInspectConfig();
				logger.info(">>> INSPECT TYPE[{}]...", config.getInspect_type());
				
				Integer count;
				
				List<Inspect> list = Lists.newArrayList();
				
				//전체
				if(config.getInspect_type() == 10){
					list = managerMapper.findInspectByRate(new Inspect(config.getInspect_rate(), INSPECT_STATE_READY, null, null));
				}else{
					
					//학년별
					List<Inspect> students = managerMapper.findInspectByStudentRate();
					for(Inspect inspect : students){
						
						List<Inspect> student = Lists.newArrayList();
						student = managerMapper.findInspectByRate(new Inspect(config.getInspect_rate(), INSPECT_STATE_READY, inspect.getStudent_type(), inspect.getStudent_grade()));
						
						logger.info(">>> INSPECT STUDENTS [{}]", student.toString());
						
						list.addAll(student);
					}
					
				}
				logger.info(">>> INSPECT SIZE[{}]...", list.size());
				
				
				if(list.size() > 0){
					//add inspect
					count = managerMapper.insertInspect(list);
					logger.info(">>> INSPECT INSERT COUNT[{}]...", count);
					
					//add inspect log
					count = managerMapper.insertInspectLog(list);
					logger.info(">>> INSPECT INSERT LOG COUNT[{}]...", count);
				}
				
			} catch (Exception e) {
				logger.info("* Exception... Inspect Task Check! [{}]", e.getMessage());
				e.printStackTrace();
			}
			logger.info("* Inspect Task... end <<< ");
		}
	}
}