package com.epopcon.smartssen.component;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.stereotype.Component;

@Component
public class DateUtils {
	
	public String getDateFormat(String format) {
		DateFormat dateFormat = new SimpleDateFormat(format);
		Date currentTime = new Date();
		return dateFormat.format(currentTime);
	}
	
	public String getDate(String date, String format) throws ParseException {
		if(StringUtils.isBlank(date)){
			return "-";
		}else{
			return DateFormatUtils.format(org.apache.commons.lang3.time.DateUtils.parseDate(date, new String[]{"yyyy-MM-dd HH:mm:ss.sss"}), format);
		}
	}

}
