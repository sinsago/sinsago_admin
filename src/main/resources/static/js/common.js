/**
 * 
 */

(function($){
	
	//DataTable error popup 제거
	$.fn.dataTable.ext.errMode = 'none';
	
	$(document).ajaxError(function(event, jqxhr, settings, thrownError) {
		
		var message;
		
		if(jqxhr.status != 200){
			switch(jqxhr.status) {
			
				case 500:
					message = "세션이 종료되었거나 서버 오류입니다. 다시 로그인해주세요.";
					break;
				
				case 302:
					message = "세션이 종료되었습니다. 다시 로그인해주세요.";
					break;
					
				case 404:
					message = "요청하신 페이지로 이동할 수 없습니다.";
					break;
					
				case 401:
					message = "해당 메뉴의 접근 권한이 없습니다.";
					break;
					
				default:
					message = "관리자에게 문의해주시기 바랍니다.";
					break;
			}
			
			$.notify({ 
				message: message
				,icon: 'fa fa-times-circle-o'
			},{
				type: 'danger'
			});
		}
	});
	
	$.fnComplateOfClosedMonth = function(startDate) {
		
		console.log("===================================================================================================================");
		console.log(startDate);
		console.log("===================================================================================================================");
		
		/*
		var data = [
			"2017-09"
			, "2017-10"
		];
		$.each(data, function (i, month) {
			
			start = moment(month).startOf('month');
			end = moment(month).endOf('month');
			
			range = moment().range(start, end);
			
			array = range.toArray('days');
			
			$.each(array, function(i, e) {
				console.log(moment(e).format("YYYY-MM-DD"));
			});
			
			$.merge(result, array);
		});
		return result;
		*/
		
		var result = [];
		$.ajax({
			type : 'POST',
			url : '/teacher/complateOfClosedMonth',
			data : {
				startDate : startDate.format('YYYY-MM')
			},
			async : true,
			dataType : 'json',
			beforeSend : function(xhr) {},
			success : function(data) {
				
				
				var start, end, range, array;
				
				$.each(data, function (i, month) {
					
					start = moment(month).startOf('month');
					end = moment(month).endOf('month');
					
					range = moment().range(start, end);
					array = range.toArray('days');
					
					
					$.each(array, function(i, e) {
						//console.log(moment(e).format("YYYY-MM-DD"));
						
						result.push(moment(e).format("YYYY-MM-DD"));
					});
					
					
					//$.merge(result, array);
				});
				
				
			},
			error : function(jqXHR, textStatus, errorThrown) {},
			complete : function(e) {}
		});
		
		return result;
	}

	$.fnWarning = function(message){
		$.notify({message: '&nbsp;' + message, icon: 'fa fa-warning'},{type: 'danger'});
	};
	
	$.isBlank = function(obj){
		return(!obj || $.trim(obj) === "");
	};
	
	$.defaultIfBlank = function(obj, val) {
		if($.isBlank(obj)){
			return val;
		}else{
			return obj;
		}
	};
	
	$.fnFeeTabs = function(index) {
		
		switch(index) {
		
			case 0:
				
				$.fnMonthlyFee();
				
				break;
				
			case 1:
				
				//승인정산관리
				$.fnMonthlyApproveFee();
				
				break;
				
			default:
				break;
		}
		
	}
	
	//교사 근무 상태
	$.fnSetTeachersStatus = function(code) {
		switch(code) {
			case 0:
				return '<span class="label label-default">근무종료</span>';
				break;
			case 1:
				return '<span class="label label-success">근무중</span>';
				break;
			case 2:
				return '<span class="label label-warning">휴직중</span>';
				break;
			default:
				return '<span class="label label-default">근무중</span>';
		}
	};
	
	//교사 근무 상태
	$.fnChangeOption = function(code, obj, type) {
		
		//첨삭대기 인경우에만 담당 교사를 변경한다
	    if(code < 1){
	    	$('#btn-change').removeClass("hide");
	    	$('#teacher-type').removeClass("hide");
	    }else{
	    	$('#btn-change').addClass("hide");
	    	$('#teacher-type').addClass("hide");
	    }
	    
	    //관리자 타입이 최고관리자(10)가 아니면 버튼 제외 
	    if(type !== 10){
			$('#btn-change').addClass("hide");
			$('#teacher-type').addClass("hide");
		};
	    
		//reset option hidden
		$("#date-type").find("option").removeClass("hidden");
		
		switch(code) {
		
			case 0:
				$("#date-type").find("option[value='30']").addClass("hidden");
				$("#date-type").find("option[value='40']").addClass("hidden");
				break;
				
			case 1:
				$("#date-type").find("option[value='30']").addClass("hidden");
				$("#date-type").find("option[value='40']").addClass("hidden");
				break;
				
			case 2:
				$("#date-type").find("option[value='20']").addClass("hidden");
				$("#date-type").find("option[value='40']").addClass("hidden");
				break;
				
			case 3:
				$("#date-type").find("option[value='20']").addClass("hidden");
				$("#date-type").find("option[value='40']").addClass("hidden");
				break;
				
			case 4:
				$("#date-type").find("option[value='20']").addClass("hidden");
				break;
				
			default:
				$("#date-type").find("option").removeClass("hidden");
				break;
		}
	};
	
	//학년구분
	$.fnSetStudent = function(row, icon) {
		
		var code = row.student_type;
		var result;
		
		switch(code) {
			case 10:
			
				result = '<span class="label label-success">초등&nbsp;'+(row.student_grade > 0 ? row.student_grade : "전체")+'</span> ';
				if(icon) result = 
					'<span class="label label-success">'
						+'초등&nbsp;'+ (row.student_grade > 0 ? row.student_grade : "전체") 
						+'&nbsp;<a href="#" onclick="$.fnGradeRemove(this);" data-grade="'+row.grade_charge+'"><span class="fa fa-times" style="color:black;"></span></a>'
					+'</span>&nbsp;';
				break;
				
			case 20:
				result = '<span class="label label-warning">중등&nbsp;'+(row.student_grade > 0 ? row.student_grade : "전체")+'</span>&nbsp;';
				if(icon) result = 
					'<span class="label label-warning">'
						+'중등&nbsp;'+ (row.student_grade > 0 ? row.student_grade : "전체")
						+'&nbsp;<a href="#" onclick="$.fnGradeRemove(this);" data-grade="'+row.grade_charge+'"><span class="fa fa-times" style="color:black;"></span></a>'
					+'</span>&nbsp;';
				break;
			case 30:
				result = '<span class="label label-primary">고등&nbsp;'+(row.student_grade > 0 ? row.student_grade : "전체")+'</span>&nbsp;';
				if(icon) result = 
					'<span class="label label-primary">'
						+'고등&nbsp;'+ (row.student_grade > 0 ? row.student_grade : "전체")
						+'&nbsp;<a href="#" onclick="$.fnGradeRemove(this);" data-grade="'+row.grade_charge+'"><span class="fa fa-times" style="color:black;"></span></a>'
					+'</span>&nbsp;';
				break;
			default:
				result = '<span class="label label-success">초등&nbsp;'+(row.student_grade > 0 ? row.student_grade : "전체")+'</span>&nbsp;';
				if(icon) result = 
					'<span class="label label-success">'
						+'초등&nbsp;'+ (row.student_grade > 0 ? row.student_grade : "전체")
						+'&nbsp;<a href="#" onclick="$.fnGradeRemove(this);" data-grade="'+row.grade_charge+'"><span class="fa fa-times" style="color:black;"></span></a>'
					+'</span>&nbsp;';
		}
		
		return result;
	};
	
	//학년구분
	$.fnDrawStudent = function(obj, data) {
		
		obj.empty();
		
		if(data.length < 1) return;
		
		var html = "";
 		$.each(data, function (i, row) {
 			html += $.fnSetStudent(row, true);
		});
 		
 		obj.append(html);
	};
	
	//학년 삭제
	$.fnGradeRemove = function(obj) {
		_GRADE = $.grep(_GRADE, function(row, i) {
			return row.grade_charge != $(obj).data('grade');
		});
		$.fnDrawStudent($('#grade-charge'), _GRADE);
	};
	
	//교사 타입
	$.fnSetTeachersType = function(code) {
		switch(code) {
			case 10:
				return '<span class="label label-default">교사</span>';
				break;
			case 20:
				return '<span class="label label-success">아르바이트</span>';
				break;
			case 30:
				return '<span class="label label-warning">내부직원</span>';
				break;
			default:
				return '<span class="label label-default">교사</span>';
		}
	};
	
	//교사 타입
	$.fnSetTeachersTypeText = function(code) {
		switch(code) {
			case 10:
				return '교사';
				break;
			case 20:
				return '아르바이트';
				break;
			case 30:
				return '내부직원';
				break;
			default:
				return '교사';
		}
	};
	
	//관리자 타입
	$.fnSetManagerType = function(code) {
		
		switch(code) {
			case 10:
				$('#type-10').parent().addClass('active');
				return '<span class="label label-danger">최고관리자</span>';
				break;
			case 20:
				$('#type-20').parent().addClass('active');
				return '<span class="label label-success">승인관리자</span>';
				break;
			case 30:
				$('#type-30').parent().addClass('active');
				return '<span class="label label-warning">검수관리자</span>';
				break;
			default:
				return '<span class="label label-success">승인관리자</span>';
		}
	};
	
	//교사 근무 상태
	$.fnSetInspectStatus = function(code) {
		switch(code) {
			case 10:
				return '<span class="label label-primary">미확인</span>';
				break;
			case 20:
				return '<span class="label label-success">검수완료</span>';
				break;
			case 30:
				return '<span class="label label-danger">검수거절</span>';
				break;
			default:
				return '<span class="label label-primary">미확인</span>';
		}
	};
	
	//교사 근무상태
	$.fnSetActiveState = function(code) {
		switch(code) {
			case 0:
				$('#state-0').parent().addClass('active');
				break;
			case 1:
				$('#state-1').parent().addClass('active');
				break;
			case 2:
				$('#state-2').parent().addClass('active');
				break;
			case 3:
				$('#state-2').parent().addClass('active');
				break;
			case 4:
				$('#state-2').parent().addClass('active');
				break;
			default:
				$('#state-0').parent().addClass('active');
		}
	};
	
	//교사 타입
	$.fnSetActiveType = function(code) {
		switch(code) {
			case 10:
				$('#type-10').parent().addClass('active');
				break;
			case 20:
				$('#type-20').parent().addClass('active');
				break;
			case 30:
				$('#type-30').parent().addClass('active');
				break;
			default:
				$('#type-10').parent().addClass('active');
		}
	};
	
	//답안 상태 제어
	$.fnNoteStateHandler = function(code, refuse) {
		
		$('#state-0').parent().removeClass('active');
		$('#state-1').parent().removeClass('active');
		$('#state-2').parent().removeClass('active');
		$('#state-3').parent().removeClass('active');
		$('#state-4').parent().removeClass('active');
		
		$("#st_id").attr("disabled", true);
		$("#note-state label").attr("disabled", true);
		$("#note-state :input").attr("disabled", true);
		
		//반려사유 입력란 
		$('#panel-refuse-add').addClass('hidden');
		
		switch(code) {
			case 0:	//첨삭대기
				$('#state-0').parent().addClass('active');
				
				//담당 선생님 변경 가능
				$("#st_id").attr("disabled", false);
				
				break;
			case 1:	//첨삭중
				$('#state-1').parent().addClass('active');
				break;
			case 2:	//승인대기
				$('#state-2').parent().addClass('active');
				
				//반려상태, 승인완료 활성화
				$('#state-3').parent().attr("disabled", false);
				$('#state-3').attr("disabled", false);
				
				$('#state-4').parent().attr("disabled", false);
				$('#state-4').attr("disabled", false);
				
				if(refuse.length > 0 || !$.isBlank(refuse)){

					$('#panel-refuse').removeClass('hidden');
					
					//SETTING REFUSE
					$('#refuse').empty();
					$('#tmpl-refuse-status-2')
						.tmpl(refuse)
						.appendTo($('#refuse'));
				}else{
					$('#panel-refuse').addClass('hidden');
				}
				
				break;
			case 3:	//반려
				$('#state-3').parent().addClass('active');
				
				if(refuse.length > 0 || !$.isBlank(refuse)){

					$('#panel-refuse').removeClass('hidden');
					
					//SETTING REFUSE
					$('#refuse').empty();
					$('#tmpl-refuse-status-3')
						.tmpl(refuse)
						.appendTo($('#refuse'));
				}else{
					$('#panel-refuse').addClass('hidden');
				}
				
				break;
			case 4:	//승인완료
				$('#state-4').parent().addClass('active');
				break;
			default:
				$('#state-0').parent().addClass('active');
		}
	};
	
	//첨삭 리스트 탭의 상태별 카운트 세팅
	$.fnSetAnswerNoteBadge = function(tabs) {
		
		$('#tabs-waiting').text(tabs.waiting);
		$('#tabs-blocked').text(tabs.blocked);
		$('#tabs-running').text(tabs.running);
		$('#tabs-pending').text(tabs.pending);
		$('#tabs-approve').text(tabs.approve);
		
	};
	
	$.fnGetNoteStatistic = function(stId, startDate, endDate){
		
		$.ajax({
			type : 'POST',
			url : '/teacher/note/statistic',
			data: {
				stId : stId
				, startDate : startDate
				, endDate : endDate
			},
			async : true,
			dataType : 'json',
			beforeSend : function(xhr) {
				$.blockUI();
			},
			success : function(data) {
				
				$('#tb-note-statistic').empty();
				
				$('#tmpl-note-statistic')
				.tmpl(data)
				.appendTo($('#tb-note-statistic'));
				
			},
			error : function(jqXHR, textStatus, errorThrown) {
				$.notify({message: jqXHR.responseJSON.message,icon: 'fa fa-warning'},{type: 'danger'});
			},
			complete : function(e) {
				$.unblockUI();
			}
		});
		//.ajax
	}
	
	$.fnGetGrade = function(id){
		
		$.ajax({
			type : 'POST',
			url : '/manager/grade',
			data: {
				id : id
			},
			async : true,
			dataType : 'json',
			beforeSend : function(xhr) {
				$.blockUI();
			},
			success : function(d) {
				_GRADE = d.data;
				$.fnDrawStudent($('#grade-charge'), _GRADE);
			},
			error : function(jqXHR, textStatus, errorThrown) {
				$.notify({message: jqXHR.responseJSON.message,icon: 'fa fa-warning'},{type: 'danger'});
			},
			complete : function(e) {
				$.unblockUI();
			}
		});
		//.ajax
	}
	
	$.fnCheckStudentGrade = function(obj, type, grade){
		
		var charge = type + "-" + grade;
		if($.fnFindByStudentType(obj, charge)){
			$.notify({message: "이미 선택되었습니다.", icon: 'fa fa-warning'}, {type: 'danger'});
		}else{
			if($.isBlank(type)) {
				if (confirm('학년을 전체로 등록하는 경우, 기존에 선택된 학년이 모두 삭제됩니다.\n전체 학년으로 변경할까요?')) {
					obj = [];
				}
			}else{

				if(grade == 0) {
						obj = $.grep(obj, function (row) { 
							return row.student_type != type; 
						});
				}else{
					
					var index = obj.map(function(o) {
						return o.grade_charge;
					}).indexOf(type+"-0");
					
					if(index > -1){
						obj.splice(index,1);
					}
				}
				
				//push
				$.fnPushOfStudentCharge(obj, type, grade, charge);
			}
		}
		
		
		return obj;
	}
	
	// 선택학년 등록
	$.fnPushOfStudentCharge = function(obj, type, grade, charge){
		
		var data = new Object();
		
		data.student_type = parseInt(type);
		data.student_grade = parseInt(grade);
		data.grade_charge = charge;

		obj.push(data);
	}
	
	// 학년구분 코드 존재 여부
	$.fnFindByStudentType = function(obj, val){
		var index = obj.map(function(o) {
			return o.grade_charge;
		}).indexOf(val);
		return index > -1 ? true : false;
	}
	
	// 학년 코드 존재 여부
	$.fnFindByStudentGrade = function(obj, val){
		
		var index = obj.map(function(o) {
			return (o.student_grade).toString();
		}).indexOf(val);
		
		return index > -1 ? true : false;
	}

	$.fnCommaSeparateNumber = function(val) {
		while (/(\d+)(\d{3})/.test(val.toString())){
			val = val.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
		}
		return $.number(val);
	};
	
	$.fnDataTableRenderEllipsis = function(data, cutoff) {
		return data.length > cutoff ? data.substr(0, cutoff) + '…' : data;
	};
	
	$.fnSplitStRRN = function(rrn) {
		var split = rrn.split('-');
		$('#st_rrn1').val(split[0]);
		$('#st_rrn2').val(split[1]);
	};
	
	$.fnSplitStRRNPassword = function(rrn) {
		var split = rrn.split('-');
		return split[0] + '-' + '*******'; 
	};
	
	$.fnConcatStRRN = function(rrn1, rrn2) {
		return rrn1+'-'+rrn2;
	};
	
	$('#btn-history-back').on('click', function(e){
		e.preventDefault();
		history.back(1);
		return false;
	});
	
	/**
	 * only number
	 */
	$('#st_rrn, #st_tel').keyup(function () { 
	    this.value = this.value.replace(/[^0-9\.]/g,'');
	});
	
	// ===================================================== //
	// override these in your code to change the default behavior and style
	$.blockUI.defaults = {
		// message displayed when blocking (use null for no message)
		message:  '<h1>Please wait...</h1>',

		title: null,		// title string; only used when theme == true
		draggable: true,	// only used when theme == true (requires jquery-ui.js to be loaded)

		theme: false, // set to true to use with jQuery UI themes

		// styles for the message when blocking; if you wish to disable
		// these and use an external stylesheet then do this in your code:
		// $.blockUI.defaults.css = {};
		css: {
			padding:	'15px',
			margin:		0,
			width:		'30%',
			top:		'40%',
			left:		'35%',
			textAlign:	'center',
			color: 		'#fff',
			border: 	'none',
	        opacity: 	.5,
			cursor:		'wait',
			backgroundColor: '#000',
			'-webkit-border-radius': '10px', 
	        '-moz-border-radius': '10px' 
		},
		
		// minimal style set used when themes are used
		themedCSS: {
			width:	'30%',
			top:	'40%',
			left:	'35%'
		},

		// styles for the overlay
		overlayCSS:  {
			backgroundColor:	'#000',
			opacity:			0.6,
			cursor:				'wait'
		},

		// style to replace wait cursor before unblocking to correct issue
		// of lingering wait cursor
		cursorReset: 'default',

		// styles applied when using $.growlUI
		growlCSS: {
			width:		'350px',
			top:		'10px',
			left:		'',
			right:		'10px',
			border:		'none',
			padding:	'5px',
			opacity:	0.6,
			cursor:		'default',
			color:		'#fff',
			backgroundColor: '#000',
			'-webkit-border-radius':'10px',
			'-moz-border-radius':	'10px',
			'border-radius':		'10px'
		},

		// IE issues: 'about:blank' fails on HTTPS and javascript:false is s-l-o-w
		// (hat tip to Jorge H. N. de Vasconcelos)
		/*jshint scripturl:true */
		iframeSrc: /^https/i.test(window.location.href || '') ? 'javascript:false' : 'about:blank',

		// force usage of iframe in non-IE browsers (handy for blocking applets)
		forceIframe: false,

		// z-index for the blocking overlay
		baseZ: 1000,

		// set these to true to have the message automatically centered
		centerX: true, // <-- only effects element blocking (page block controlled via css above)
		centerY: true,

		// allow body element to be stetched in ie6; this makes blocking look better
		// on "short" pages.  disable if you wish to prevent changes to the body height
		allowBodyStretch: true,

		// enable if you want key and mouse events to be disabled for content that is blocked
		bindEvents: true,

		// be default blockUI will supress tab navigation from leaving blocking content
		// (if bindEvents is true)
		constrainTabKey: true,

		// fadeIn time in millis; set to 0 to disable fadeIn on block
		fadeIn:  200,

		// fadeOut time in millis; set to 0 to disable fadeOut on unblock
		fadeOut:  400,

		// time in millis to wait before auto-unblocking; set to 0 to disable auto-unblock
		timeout: 0,

		// disable if you don't want to show the overlay
		showOverlay: true,

		// if true, focus will be placed in the first available input field when
		// page blocking
		focusInput: true,

        // elements that can receive focus
        focusableElements: ':input:enabled:visible',

		// suppresses the use of overlay styles on FF/Linux (due to performance issues with opacity)
		// no longer needed in 2012
		// applyPlatformOpacityRules: true,

		// callback method invoked when fadeIn has completed and blocking message is visible
		onBlock: null,

		// callback method invoked when unblocking has completed; the callback is
		// passed the element that has been unblocked (which is the window object for page
		// blocks) and the options that were passed to the unblock call:
		//	onUnblock(element, options)
		onUnblock: null,

		// callback method invoked when the overlay area is clicked.
		// setting this will turn the cursor to a pointer, otherwise cursor defined in overlayCss will be used.
		onOverlayClick: null,

		// don't ask; if you really must know: http://groups.google.com/group/jquery-en/browse_thread/thread/36640a8730503595/2f6a79a77a78e493#2f6a79a77a78e493
		quirksmodeOffsetHack: 4,

		// class name of the message block
		blockMsgClass: 'blockMsg',

		// if it is already blocked, then ignore it (don't unblock and reblock)
		ignoreIfBlocked: false
	};
	// ===================================================== //
	
	
})(jQuery);


$.notifyDefaults({

	// settings
	element : 'body',
	position : null,
	type : "info",
	allow_dismiss : true,
	newest_on_top : false,
	showProgressbar : false,
	placement : {
		from : "top",
		align : "right"
	},
	offset : 20,
	spacing : 10,
	z_index : 99999,
	delay : 5000,
	timer : 1000,
	url_target : '_blank',
	mouse_over : null,
	animate : {
		enter: 'animated fadeInRight',
		exit: 'animated fadeOutRight'
	},
	onShow : null,
	onShown : null,
	onClose : null,
	onClosed : null,
	icon_type : 'class'
});