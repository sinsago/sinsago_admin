/**
 * 
 */

(function($) {

	var map;
	var bounds;
	var markers = [];
	
	$.initialize = function(centerLat, centerLng, markersData) {
		
		bounds = new google.maps.LatLngBounds();
		
		var mapProp = {
			center : new google.maps.LatLng(centerLat, centerLng),
			zoom : 10,
			scrollwheel: false,
			mapTypeId : google.maps.MapTypeId.ROADMAP
		};

		map = new google.maps.Map(document.getElementById("map-canvas"),mapProp);

		// a new Info Window is created
		infoWindow = new google.maps.InfoWindow();

		// Event that closes the InfoWindow with a click on the map
		google.maps.event.addListener(map, 'click', function() {
			infoWindow.close();
		});

		$.displayMarkers(markersData);
		
		map.fitBounds(bounds);
	};
	
	$.fnMappingMap = function(centerLat, centerLng) {
		
		bounds = new google.maps.LatLngBounds();
		
		var mapProp = {
			center : new google.maps.LatLng(centerLat, centerLng),
			zoom : 10,
			scrollwheel: true,
			mapTypeId : google.maps.MapTypeId.ROADMAP
		};

		map = new google.maps.Map(document.getElementById("map-canvas"),mapProp);

		// a new Info Window is created
		infoWindow = new google.maps.InfoWindow();

		// Event that closes the InfoWindow with a click on the map
		google.maps.event.addListener(map, 'click', function() {
			infoWindow.close();
		});
		
		//$.displayMarkers(markersData);
		
		//map.fitBounds(bounds);

		
	}
	
	// creating markers with createMarker function
	$.displayMarkers = function(markersData) {
		
		if(!$.isBlank(markersData)){
			$.each(markersData, function(i, o){
				var obj = jQuery.parseJSON(o);
				
				var latlng = new google.maps.LatLng(obj.lat, obj.lng);
				var name = obj.name;
				var address1 = obj.address1;
				var address2 = obj.address2;
				var postalCode = obj.postalCode;
				
				$.createMarker(i, latlng, name, address1, address2, postalCode);
				
				bounds.extend(latlng);
			});
		}
	}
	
	// This function creates each marker and sets their Info Window content
	$.createMarker = function(i, latlng, name, address1, address2, postalCode) {
		
		var marker = new google.maps.Marker({
			map : map,
			position : latlng,
			title : name,
			label : ''+(i+1)
		});

		google.maps.event.addListener(marker, 'click', function() {
			// Variable to define the HTML content to be inserted in the infowindow
			var iwContent = '<div id="iw_container">'
					+ '<div class="iw_title">' + name + '</div>'
					+ '<div class="iw_content">' + address1 + '<br />'
					+ address2 + '<br />' + postalCode + '</div></div>';

			// including content to the infowindow
			infoWindow.setContent(iwContent);

			// opening the infowindow in the current map and at the current marker location
			infoWindow.open(map, marker);
		});
	}

	$.addMarkers = function(
			latitude
			, longitude
			, name
			, address
			, tel
			, icon) {
		
		var latlng = new google.maps.LatLng(latitude, longitude);
		var marker = new google.maps.Marker({
			map : map,
			position : latlng,
			title : name,
			label : '',
			icon : icon
		});
		
		// This event expects a click on a marker 
		// When this event is fired the infowindow content is created and the infowindow is opened
		google.maps.event.addListener(marker, 'click', function() {
			// Variable to define the HTML content to be inserted in the infowindow
			var iwContent = '<div id="iw_container">'
								+ '<div class="iw_title">' + name + '</div>'
								+ '<div class="iw_content">' + address + '<br />'
								+ tel + '</div></div>';

			// including content to the infowindow
			infoWindow.setContent(iwContent);

			// opening the infowindow in the current map and at the current marker location
			infoWindow.open(map, marker);
		});
		
		// Marker’s Lat. and Lng. values are added to bounds variable
		bounds.extend(latlng);
		
		// Finally the bounds variable is used to set the map bounds
		// with API’s fitBounds() function
		map.fitBounds(bounds);

	}

})(jQuery);