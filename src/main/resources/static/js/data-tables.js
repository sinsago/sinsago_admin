/**
 *
 */

(function ($) {

    //교사 리스트
    $.fnDataTablesTeacher = function () {

        var dataTable;
        var page = 0;
        var size = 0;

        dataTable = $('#dt-teacher').DataTable({
            "pagingType": "simple_numbers",
            "paging": true,
            "bLengthChange": false,
            "processing": true,
            "serverSide": true,
            "ordering": false,
            "info": true,
            "searching": false,
            "responsive": true,
            "deferRender": true,
            "stateSave": true,
            "bStateSave": true,
            "language": {
                "url": "http://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Korean.json"
            },
            "ajax": {
                "type": "POST",
                "url": '/teacher/search',
                "data": function (d) {
                    var page;
                    var size;
                    if ($.type(dataTable) === 'undefined') {
                        page = 0;
                        size = 10;
                    } else {
                        page = dataTable.page.info().page;
                        size = dataTable.page.info().length;
                    }
                    d.page = page;
                    d.size = size;

                    d.searchType = $('#search-type :checked').val();
                    d.searchWord = $('#search-word').val();

                },
                "dataSrc": function (json) {
                    return json.data;
                }
            },
            "columnDefs": [
                {
                    "targets": 0,
                    "data": "rownum",
                    "className": "dt-center",
                    "render": function (data, type, row, meta) {
                        return data;
                    }
                },
                {
                    "targets": 1,
                    "data": "st_id",
                    "render": function (data, type, row, meta) {
                        return '<a href="/teacher/form?stId=' + data + '">' + data + '</a>';
                    }
                },
                {
                    "targets": 2,
                    "data": "st_nm",
                    "render": function (data, type, row, meta) {
                        return data;
                    }
                },
                {
                    "targets": 3,
                    "data": "st_type",
                    "render": function (data, type, row, meta) {
                        return $.fnSetTeachersType(data);
                    }
                },
                {
                    "targets": 4,
                    "data": "reg_date",
                    "render": function (data, type, row, meta) {
                        return $.format.date(data, "yyyy-MM-dd");
                    }
                },
                {
                    "targets": 5,
                    "data": "end_date",
                    "render": function (data, type, row, meta) {
                        if ($.isBlank(data)) {
                            return '';
                        } else {
                            return $.format.date(data, "yyyy-MM-dd");
                        }
                    }
                },
                {
                    "targets": 6,
                    "data": "tot_fee",
                    "render": function (data, type, row, meta) {
                        return data + ' 원';
                    }
                },
                {
                    "targets": 7,
                    "data": "mngr_nm",
                    "render": function (data, type, row, meta) {
                        return data;
                    }
                },
                {
                    "targets": 8,
                    "data": "st_state",
                    "render": function (data, type, row, meta) {
                        return $.fnSetTeachersStatus(data);
                    }
                }
            ]
        });
        return dataTable;
    };

    //관리자 리스트
    $.fnDataTablesManager = function () {

        var dataTable;
        var page = 0;
        var size = 0;

        dataTable = $('#dt-manager').DataTable({
            "pagingType": "simple_numbers",
            "paging": true,
            "bLengthChange": false,
            "processing": true,
            "serverSide": true,
            "ordering": false,
            "info": true,
            "searching": false,
            "responsive": true,
            "deferRender": true,
            "stateSave": true,
            "bStateSave": true,
            "language": {
                "url": "http://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Korean.json"
            },
            "ajax": {
                "type": "POST",
                "url": '/manager/search',
                "data": function (d) {
                    var page;
                    var size;
                    if ($.type(dataTable) === 'undefined') {
                        page = 0;
                        size = 10;
                    } else {
                        page = dataTable.page.info().page;
                        size = dataTable.page.info().length;
                    }
                    d.page = page;
                    d.size = size;
                },
                "dataSrc": function (json) {
                    return json.data;
                }
            },
            "columnDefs": [
                {
                    "targets": 0,
                    "data": "rownum",
                    "className": "dt-center",
                    "render": function (data, type, row, meta) {
                        return data;
                    }
                },
                {
                    "targets": 1,
                    "data": "admin_id",
                    "render": function (data, type, row, meta) {
                        return '<a href="/manager/form?adminId=' + data + '">' + data + '</a>';
                    }
                },
                {
                    "targets": 2,
                    "data": "admin_type",
                    "render": function (data, type, row, meta) {
                        return $.fnSetManagerType(parseInt(data));
                    }
                },
                {
                    "targets": 3,
                    "data": "admin_nm",
                    "render": function (data, type, row, meta) {
                        return data;
                    }
                },
                {
                    "targets": 4,
                    "data": "reg_date",
                    "render": function (data, type, row, meta) {
                        return $.format.date(data, "yyyy-MM-dd");
                    }
                },
                {
                	"targets": 5,
                	"data": "grade_charge",
                	"render": function (data, type, row, meta) {
                		
                		var grade = "";
                		$.each(data, function (i, row) {
                			grade += $.fnSetStudent(row, false);
						});
                		
                		return grade;
                	}
                },
                {
                    "targets": 6,
                    "data": "managed_cnt",
                    "render": function (data, type, row, meta) {
                        return data + '&nbsp;<small>명</small>';
                    }
                }]
        });
        return dataTable;
    };

    //정답노트 리스트
    $.fnDataTablesNotes = function () {
    	
        var dataTable;
        var page = 0;
        var size = 0;

        dataTable = $('#dt-note').DataTable({
            "pagingType": "simple_numbers",
            "paging": true,
            "bLengthChange": false,
            "processing": true,
            "serverSide": true,
            "ordering": false,
            "info": true,
            "searching": false,
            "responsive": true,
            "deferRender": true,
            "stateSave": true,
            "bStateSave": true,
            "language": {
                "url": "http://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Korean.json"
            },
            "select": {
                "style": 'multi',
                "selector": 'td:first-child'
            },
            "ajax": {
                "type": "POST",
                "url": '/answer/search',
                "data": function (d) {
                    var page;
                    var size;
                    if ($.type(dataTable) === 'undefined') {
                        page = 0;
                        size = 10;
                    } else {
                        page = dataTable.page.info().page;
                        size = dataTable.page.info().length;
                    }
                    d.page = page;
                    d.size = size;

                    d.dateType = $('#date-type').val();
                    
                    d.searchStart = $('#search-start').val();
                    d.searchEnd = $('#search-end').val();
                    
                    d.studentType = $('#student-type').val();
                    d.gradeType = $('#grade-type').val();
                    
                    d.searchType = $('#search-type').val();
                    d.searchWord = $('#search-word').val();

                    //정답노트 상태
                    d.state = $('.nav-tabs .active').index();
                },
                "dataSrc": function (json) {

                    //첨삭 리스트 탭의 상태별 카운트 세팅
                    $.fnSetAnswerNoteBadge(json.tabs);

                    //checkbox 초기화
                    $('#btn-checkbox-all').removeClass('active');

                    //선택승인 버튼 활성여부
                    if (json.recordsTotal > 0 && $('.nav-tabs .active').index() == 2) {
                        $('#btn-approve').attr('disabled', false);

                        $('#btn-approve').addClass('animated flash btn btn-primary')
                            .on("webkitAnimationEnd", function () {
                                $(this).removeClass('animated flash');
                            });
                    } else {
                        $('#btn-approve').attr('disabled', true);
                    }
                    
                    
                    var index = $('.nav-tabs .active').index();
                    
                    // Get the column API object
                	switch(index) {
	        			case 0:	//첨삭대기
	        				
	        				// Toggle the visibility
	        				dataTable.columns([10,11]).visible(false, false);
	        				dataTable.columns([9]).visible(true, false);
	        				
	        				break;
	        			case 1:	//첨삭중
	        				
	        				// Toggle the visibility
	        				dataTable.columns([10,11]).visible(false, false);
	        				dataTable.columns([9]).visible(true, false);
	        				
	        				break;
	        			case 2:	//승인대기
	        				
	        				// Toggle the visibility
	                    	dataTable.columns([9,11]).visible(false, false);
	        				dataTable.columns([10]).visible(true, false);
	                    	
	        				break;
	        			case 3:	//반려
	        				
	        				// Toggle the visibility
	        				dataTable.columns([9,11]).visible(false, false);
	        				dataTable.columns([10]).visible(true, false);
	        				
	        				break;
	        			case 4:	//승인완료
	        				
	        				// Toggle the visibility
	        				dataTable.columns([9]).visible(false, false);
	        				dataTable.columns([10,11]).visible(true, false);
	        				
	        				break;
	        			default:
        					dataTable.columns([9,11]).visible(true, false);
	        				break;
                	}
                	
                	return json.data;
                }
            },
            "columnDefs": [
                {
                    "targets": 0,
                    "data": "",
                    "width": "1%",
                    "className": "select-checkbox",
                    "render": function (data, type, row, meta) {
                        return "";
                    }
                },
                {
                    "targets": 1,
                    "data": "rownum",
                    "width": "1%",
                    "className": "dt-center",
                    "render": function (data, type, row, meta) {
                        return data;
                    }
                },
                {
                    "targets": 2,
                    "data": "an_id",
                    "width": "14%",
                    "className": "dt-center",
                    "render": function (data, type, row, meta) {
                        var searchStart = $('#search-start').val();
                        var searchEnd = $('#search-end').val();
                        var searchType = $('#search-type :checked').val();
                        var searchWord = $('#search-word').val();
                        
						return '<a href="#" data-toggle="modal" data-target="#modal-detail" data-id="'+data+'" class="">'+data+'</a>';
                    }
                },
                {
                    "targets": 3,
                    "data": "student_grade",
                    "width": "3%",
                    "className": "dt-center",
                    "render": function (data, type, row, meta) {
                        return $.fnSetStudent(row);
                    }
                },
                {
                    "targets": 4,
                    "data": "product_nm",
                    "width": "12%",
                    "className": "dt-center",
                    "render": function (data, type, row, meta) {
                        return data;
                    }
                },
                {
                    "targets": 5,
                    "data": "an_page",
                    "width": "5%",
                    "className": "dt-center",
                    "render": function (data, type, row, meta) {
                        return data;
                    }
                },
                {
                    "targets": 6,
                    "data": "user_id",
                    "width": "8%",
                    "className": "dt-center",
                    "render": function (data, type, row, meta) {
                        return data;
                    }
                },
                {
                    "targets": 7,
                    "data": "student_nm",
                    "width": "8%",
                    "className": "dt-center",
                    "render": function (data, type, row, meta) {
                        return data;
                    }
                },
                {
                    "targets": 8,
                    "data": "reg_date",
                    "width": "10%",
                    "className": "dt-center",
                    "render": function (data, type, row, meta) {
                        return $.format.date(data, "yyyy-MM-dd HH:mm");
                    }
                },
                {
                    "targets": 9,
                    "data": "an_deadline_date",
                    "width": "10%",
                    "className": "dt-center",
                    "render": function (data, type, row, meta) {
                    	return $.format.date(data, "yyyy-MM-dd HH:mm");
                    }
                },
                {
                	"targets": 10,
                	"data": "an_request_date",
                	"width": "10%",
                	"className": "dt-center",
                	"render": function (data, type, row, meta) {
                		return $.format.date(data, "yyyy-MM-dd HH:mm");
                	}
                },
                {
                	"targets": 11,
                	"data": "an_approve_date",
                	"width": "10%",
                	"className": "dt-center",
                	"render": function (data, type, row, meta) {
                		return $.format.date(data, "yyyy-MM-dd HH:mm");
                	}
                },
                {
                    "targets": 12,
                    "data": "st_nm",
                    "width": "8%",
                    "className": "dt-center",
                    "render": function (data, type, row, meta) {
                        return data;
                    }
                }
            ]
        });
        return dataTable;
    };
    
    //정답노트 검수
    $.fnDataTablesInspect = function () {
    	
    	var dataTable;
    	var page = 0;
    	var size = 0;
    	
    	dataTable = $('#dt-inspect').DataTable({
    		"pagingType": "simple_numbers",
    		"paging": true,
    		"bLengthChange": false,
    		"processing": true,
    		"serverSide": true,
    		"ordering": false,
    		"info": true,
    		"searching": false,
    		"responsive": true,
    		"deferRender": true,
    		"stateSave": true,
    		"bStateSave": true,
    		"language": {
    			"url": "http://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Korean.json"
    		},
    		"ajax": {
    			"type": "POST",
    			"url": '/inspect/search',
    			"data": function (d) {
    				var page;
    				var size;
    				if ($.type(dataTable) === 'undefined') {
    					page = 0;
    					size = 10;
    				} else {
    					page = dataTable.page.info().page;
    					size = dataTable.page.info().length;
    				}
    				d.page = page;
    				d.size = size;
    				
    				d.dateType = $('#date-type').val();
    				
    				d.searchStart = $('#search-start').val();
    				d.searchEnd = $('#search-end').val();
    				
    				d.studentType = $('#student-type').val();
    				d.gradeType = $('#grade-type').val();
    				
    				d.searchInspectType = $('#seach-inspect-type').val();
    				
    				d.searchType = $('#search-type').val();
    				d.searchWord = $('#search-word').val();
    			},
    			"dataSrc": function (json) {
    				return json.data;
    			}
    		},
    		"columnDefs": [
    		               
				{
					"targets": 0,
					"data": "rownum",
					"width": "1%",
					"className": "dt-center",
					"render": function (data, type, row, meta) {
					   return data;
				   }
				},
				{
				    "targets": 1,
					"data": "an_id",
					"width": "14%",
					"className": "dt-center",
					"render": function (data, type, row, meta) {
					return '<a href="#" data-toggle="modal" data-target="#modal-detail" data-id="'+data+'" class="">'+data+'</a>';
				   }
				},
				{
					"targets": 2,
					"data": "student_grade",
					"width": "3%",
					"className": "dt-center",
					"render": function (data, type, row, meta) {
						   return $.fnSetStudent(row);
					   }
				},
				{
				   "targets": 3,
					"data": "product_nm",
					"width": "12%",
					"className": "dt-center",
					"render": function (data, type, row, meta) {
						   return data;
					   }
				},
				{
				   "targets": 4,
					"data": "an_page",
					"width": "5%",
					"className": "dt-center",
					"render": function (data, type, row, meta) {
						   return data;
					   }
				},
				{
				   "targets": 5,
					"data": "user_id",
					"width": "8%",
					"className": "dt-center",
					"render": function (data, type, row, meta) {
						   return data;
					   }
				},
				{
				   "targets": 6,
					"data": "student_nm",
					"width": "8%",
					"className": "dt-center",
					"render": function (data, type, row, meta) {
						   return data;
					   }
				},
				{
				   "targets": 7,
					"data": "reg_date",
					"width": "10%",
					"className": "dt-center",
					"render": function (data, type, row, meta) {
					return $.format.date(data, "yyyy-MM-dd HH:mm");
					   }
				},
				{
				   "targets": 8,
					"data": "an_request_date",
					"width": "10%",
					"className": "dt-center",
					"render": function (data, type, row, meta) {
					return $.format.date(data, "yyyy-MM-dd HH:mm");
				   }
				},
				{
				   "targets": 9,
					"data": "an_approve_date",
					"width": "10%",
					"className": "dt-center",
					"render": function (data, type, row, meta) {
					return $.format.date(data, "yyyy-MM-dd HH:mm");
				   }
				},
				{
				   "targets": 10,
					"data": "st_nm",
					"width": "8%",
					"className": "dt-center",
					"render": function (data, type, row, meta) {
					   return data;
				   }
				},
				{
				   "targets": 11,
					"data": "inspect_state",
					"width": "8%",
					"className": "dt-center",
					"render": function (data, type, row, meta) {
						return $.fnSetInspectStatus(data);
					}
				}
			]
    	});
    	return dataTable;
    };
    
    //정산관리 > 첨삭정산 관리
    $.fnDataTablesMonth = function () {
    	
    	var dataTable;

    	dataTable = $('#dt-month').DataTable({
    		"paging":false,
    		"info":false,
    		"processing": true,
    		"serverSide": true,
    		"searching": false,
    		"responsive": true,
    		"ordering": false,
    		"language": {
    			"url": "http://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Korean.json"
    		},
    		"ajax": {
    			"type": "POST",
    			"url": '/fee/month',
    			"data": function (d) {
    				d.month = $('#search-start').val();
    			},
    			"dataSrc": function (json) {
    				
    				var totalOfFee = 0;
    				var totalOfApprove = 0;
    				
    				if(json.data.length > 0){
    					for(var i=0; i<json.data.length; i++) {
        					totalOfApprove += json.data[i].an_approve_cnt;
        					totalOfFee += json.data[i].sub_sum;
        				}
    					
    					//active excel
						$('#btn-excel-fee').attr('disabled', false);
						
						//change btn
						if (json.isComplete == 'Y') {
							$('#btn-save-fee').attr('disabled', true);
							$('#btn-save-fee').html('<span class="fa fa-krw"></span> 정산완료');
						} else {
							$('#btn-save-fee').attr('disabled', false);
							$('#btn-save-fee').html('<span class="fa fa-krw"></span> 정산대기');
							
							if ($('#search-start').val() == moment().format('YYYY-MM')) {
								$('#btn-save-fee').attr('disabled', true);
							}
						}
    				}else{
    					//active excel
						$('#btn-excel-fee').attr('disabled', true);
						
						$('#btn-save-fee').attr('disabled', true);
						$('#btn-save-fee').html('<span class="fa fa-krw"></span> 정산대기');
    				}
    				
    				$('#approve-tot').text($.number(totalOfApprove));
    				$('#fee-tot').text($.number(totalOfFee));
    				
    				return json.data;
    			}
    		},
    		"columnDefs": [
    			
    			{
    				"targets": 0,
    				"data": "rownum",
    				"width": "1%",
    				"className": "dt-center",
    				"render": function (data, type, row, meta) {
    					return data;
    				}
    			},
    			{
    				"targets": 1,
    				"data": "st_nm",
    				"className": "dt-center",
    				"render": function (data, type, row, meta) {
    					return data;
    				}
    			},
    			{
    				"targets": 2,
    				"data": "st_id",
    				"className": "dt-center",
    				"render": function (data, type, row, meta) {
    					return data;
    				}
    			},
    			{
    				"targets": 3,
    				"data": "reg_date",
    				"className": "dt-center",
    				"render": function (data, type, row, meta) {
    					return $.format.date(data, "yyyy-MM-dd");
    				}
    			},
    			{
    				"targets": 4,
    				"data": "an_approve_cnt",
    				"className": "dt-center",
    				"render": function (data, type, row, meta) {
    					return $.number(data);
    				}
    			},
    			{
    				"targets": 5,
    				"data": "calc_fee",
    				"className": "dt-center",
    				"render": function (data, type, row, meta) {
    					return $.number(data);
    				}
    			},
    			{
    				"targets": 6,
    				"data": "apply_date",
    				"className": "dt-center",
    				"render": function (data, type, row, meta) {
    					return $.format.date(data, "yyyy-MM-dd");
    				}
    			},
    			{
    				"targets": 7,
    				"data": "sub_sum",
    				"className": "dt-center",
    				"render": function (data, type, row, meta) {
    					if(data == row.tot_sum){
    						return '';
    					}else{
    						return $.number(data);
    					}
    				}
    			},
    			{
    				"targets": 8,
    				"data": "tot_sum",
    				"className": "dt-center",
    				"render": function (data, type, row, meta) {
    					return $.number(data);
    				}
    			}],
    			rowsGroup: [
    				1,2,3,8
		        ]
    	});
    	return dataTable;
    };

    //관리교사 리스트
    $.fnDataTablesManaged = function (managerId) {

        var dataTable;
        var page = 0;
        var size = 0;

        dataTable = $('#dt-managed').DataTable({
            "pagingType": "simple_numbers",
            "paging": true,
            "bLengthChange": false,
            "processing": true,
            "serverSide": true,
            "ordering": false,
            "info": true,
            "searching": false,
            "responsive": true,
            "deferRender": true,
            "language": {
                "url": "http://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Korean.json"
            },
            "ajax": {
                "type": "POST",
                "url": '/manager/managed',
                "data": function (d) {
                    var page;
                    var size;
                    if ($.type(dataTable) === 'undefined') {
                        page = 0;
                        size = 10;
                    } else {
                        page = dataTable.page.info().page;
                        size = dataTable.page.info().length;
                    }
                    d.page = page;
                    d.size = size;

                    d.managerId = managerId;

                },
                "dataSrc": function (json) {

                    //관리하는 교사가 없는 경우 삭제할 수 있다
                    if (!$.isBlank(managerId) && json.data.length < 1) {
                        $('#btn-remove').removeClass('disabled');
                    }
                    ;

                    return json.data;
                }
            },
            "columnDefs": [
                {
                    "targets": 0,
                    "data": "rownum",
                    "className": "dt-center",
                    "render": function (data, type, row, meta) {
                        return data;
                    }
                },
                {
                    "targets": 1,
                    "data": "st_id",
                    "render": function (data, type, row, meta) {
                        return '<a href="/teacher/form?stId=' + data + '">' + data + '</a>';
                    }
                },
                {
                    "targets": 2,
                    "data": "st_nm",
                    "render": function (data, type, row, meta) {
                        return data;
                    }
                },
                {
                    "targets": 3,
                    "data": "st_type",
                    "render": function (data, type, row, meta) {
                        return $.fnSetTeachersType(data);
                    }
                },
                {
                    "targets": 4,
                    "data": "reg_date",
                    "render": function (data, type, row, meta) {
                        return $.format.date(data, "yyyy-MM-dd");
                    }
                },
                {
                    "targets": 5,
                    "data": "end_date",
                    "render": function (data, type, row, meta) {
                        return $.format.date(data, "yyyy-MM-dd");
                    }
                },
                {
                    "targets": 6,
                    "data": "tot_fee",
                    "render": function (data, type, row, meta) {
                    	return data + ' 원';
                    }
                },
                {
                    "targets": 7,
                    "data": "mngr_nm",
                    "render": function (data, type, row, meta) {
                        return data;
                    }
                },
                {
                    "targets": 8,
                    "data": "st_state",
                    "render": function (data, type, row, meta) {
                        return $.fnSetTeachersStatus(data);
                    }
                }]
        });
        return dataTable;
    };

    //APP 리스트
    $.fnDataTablesApp = function () {

        var dataTable;
        var page = 0;
        var size = 0;

        dataTable = $('#dt-app').DataTable({
            "pagingType": "simple_numbers",
            "paging": true,
            "bLengthChange": false,
            "processing": true,
            "serverSide": true,
            "ordering": false,
            "info": true,
            "searching": false,
            "responsive": true,
            "deferRender": true,
            "language": {
                "url": "http://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Korean.json"
            },
            "ajax": {
                "type": "POST",
                "url": '/app/search',
                "data": function (d) {
                    var page;
                    var size;
                    if ($.type(dataTable) === 'undefined') {
                        page = 0;
                        size = 10;
                    } else {
                        page = dataTable.page.info().page;
                        size = dataTable.page.info().length;
                    }
                    d.page = page;
                    d.size = size;
                },
                "dataSrc": function (json) {
                    return json.data;
                }
            },
            "columnDefs": [
                {
                    "targets": 0,
                    "data": "rownum",
                    "className": "dt-center",
                    "render": function (data, type, row, meta) {
                        return data;
                    }
                },
                {
                    "targets": 1,
                    "data": "app_version",
                    "render": function (data, type, row, meta) {
                        return data;
                    }
                },
                {
                    "targets": 2,
                    "data": "reg_date",
                    "render": function (data, type, row, meta) {
                        return $.format.date(data, "yyyy-MM-dd HH:mm");
                    }
                },
                {
                    "targets": 3,
                    "data": "app_path",
                    "render": function (data, type, row, meta) {
                        return '<a href="/apk/' + row.app_seq + '">다운로드</a>';
                    }
                },
                {
                    "targets": 4,
                    "data": "is_active",
                    "render": function (data, type, row, meta) {

                        if (data == 0) {
                            return '<a href="javascript:$.fnDeleteApp(' + row.app_seq + ');" class="btn btn-primary"><span class="fa fa-trash-o"></span> 삭제</a>';
                        } else {
                            return '';
                        }

                    }
                }]
        });
        return dataTable;
    };
})(jQuery);