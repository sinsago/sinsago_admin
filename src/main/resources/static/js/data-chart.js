/**
 * 
 */

(function($) {
	
	$.fnServiceStatisticsDays = function() {
		
		$.ajax({
			type : 'POST',
			url : '/dashboard/getServiceStatisticsDays',
			async : true,
			dataType : 'json',
			beforeSend : function(xhr) {
				
				$('#get-time').text('Loading...');
				
				$('#cumulativeUser').text('Loading...');
				$('#today-user-count').text('Loading...');
				
				$('#creditCard').text('Loading...');
				$('#cumulativeMessage').text('Loading...');
				$('#cumulativeCompany').text('Loading...');
				
			},
			success : function(data) {
				
				$('#get-time').text($.format.date(data.uptDt, "yyyy-MM-dd HH:mm:ss"));
						
				//결제사용자/전체사용자
				$('#cumulativeUser').text(
					$.number(data.allPaymentUser)
					+'/'
					+ $.number(data.allJoinUser)
				);
				
				//Today
				$("#today-user-count").text($.number(data.todayJoinUser));
				
				//평균결제카드
				$('#creditCard').text(
					$.number(data.avgPaymentCard,2)
				);
				
				//평균결제/전체결제메시지
				$('#cumulativeMessage').text(
					$.number((data.avgPaymentMessage))
					+'/'
					+$.number(data.allPaymentMessage)
				);
				
				//평균방문상점/전체상점
				$('#cumulativeCompany').text(
					$.number(data.avgCompanyUsed,2)
					+'/'
					+ $.number(data.allPaymentCompany)
				);
				
				
			},
			error : function(jqXHR, textStatus, errorThrown) {
				
			},
			complete : function(e) {},
			timeout : 30000
		});
		//.ajax
	};
	
	//결제사용자/전체사용자
	$.fnAmountUsedByUser = function(subCard) {
		
		$.ajax({
			type : 'POST',
			url : '/dashboard/amountUsedByUser',
			async : true,
			dataType : 'json',
			beforeSend : function(xhr) {
				$('#cumulativeUser').text('Loading...');
				$("#today-user-count").text(0);
			},
			success : function(data) {
				
				//
				$('#cumulativeUser').text(
						$.number(data.countByUsedUser) 
						+'/'
						+ $.number(data.countByAllUser)
					);
				
				//Today
				$("#today-user-count").text($.number(data.countByTodayUser));
				
				if(subCard){
					//평균결제카드
					$.fnAmountUsedByCreditCard(data.countByUsedUser);
					
					//평균결제/전체결제메시지
					$.fnAmountUsedByPaymentMessage(data.countByUsedUser);
					
					//평균방문상점/전체상점
					$.fnAmountUsedByCompany(data.countByUsedUser);
				}
				
			},
			error : function(jqXHR, textStatus, errorThrown) {
				$('#cumulativeUser').text(0);
			},
			complete : function(e) {},
			timeout : 30000
		});
		//.ajax
	};
	
	//평균결제카드
	$.fnAmountUsedByCreditCard = function(countByUsedUser) {
		//서비스 사용 누적 데이터-카드
		$.ajax({
			type : 'POST',
			url : '/dashboard/amountUsedByCreditCard',
			data : {},
			async : true,
			dataType : 'json',
			beforeSend : function(xhr) {
				$('#creditCard').text('Loading...');
			},
			success : function(data) {
				
				$('#creditCard').text(
						$.number(data.countByPaymentCreditCardCompany/countByUsedUser,2)
					);
			},
			error : function(jqXHR, textStatus, errorThrown) {
				$('#creditCard').text(0);
			},
			complete : function(e) {}
		});
		//.ajax
	}
	
	//평균결제/전체결제메시지
	$.fnAmountUsedByPaymentMessage = function(countByUsedUser) {
		//서비스 사용 누적 데이터-결제메시지
		$.ajax({
			type : 'POST',
			url : '/dashboard/amountUsedByPaymentMessage',
			data : {},
			async : true,
			dataType : 'json',
			beforeSend : function(xhr) {
				$('#cumulativeMessage').text('Loading...');
			},
			success : function(data) {
				
				$('#cumulativeMessage').text(
						$.number((data.countByPaymentMessage / countByUsedUser))
						+'/'
						+$.number(data.countByPaymentMessage)
					);
			},
			error : function(jqXHR, textStatus, errorThrown) {
				$('#cumulativeMessage').text(0);
			},
			complete : function(e) {}
		});
		//.ajax
	}
	
	//평균방문상점/전체상점
	$.fnAmountUsedByCompany = function(countByUsedUser) {
		//서비스 사용 누적 데이터-상점
		$.ajax({
			type : 'POST',
			url : '/dashboard/amountUsedByCompany',
			data : {},
			async : true,
			dataType : 'json',
			beforeSend : function(xhr) {
				$('#cumulativeCompany').text('Loading...');
			},
			success : function(data) {
				$('#cumulativeCompany').text(
						$.number(data.countByPaymentCompanyForUsedUser/countByUsedUser,2)
						+'/'
						+ $.number(data.countByPaymentCompany)
					);
				/*
				$('#cumulativeCompanySub').text(
						data.countByPaymentCompanyForUsedUser + '/' + countByUsedUser
						);
				*/
			},
			error : function(jqXHR, textStatus, errorThrown) {
				$('#cumulativeCompany').text(0);
			},
			complete : function(e) {}
		});
		//.ajax
	}

	//일별 메시지 수집 건수
	$.fnPaymentMessages = function() {
		
		//서비스 지표1
		$.ajax({
			type : 'POST',
			url : '/dashboard/paymentMessages',
			data : {},
			async : true,
			dataType : 'json',
			beforeSend : function(xhr) {
				$("#message-area-chart").html(
					'<a href="#" class="list-group-item">'
						+ '<div class="loading">'
							+ '<i class="fa fa-connectdevelop fa-2x gly-spin"></i>'
						+ '</div>'
					+'<div>'
				);
				
			},
			success : function(obj) {
				
				$("#message-area-chart").empty();
				
				if(obj.data.length < 1){
					obj.data = [{
						'allPaymentMessage': 0
						, 'todayPaymentMessage': 0
						,'regDt': $.format.date($.now(), "yyyy-MM-dd")
					}];
				}else{
					$.each(obj.data, function(i, o) {
						o.allPaymentMessage = o.allPaymentMessage/100 | 2;
						//o.used = o.used;
						//o.used = o.used/1000 | 2;
						o.regDt = $.format.date(o.regDt, "yyyy-MM-dd");
					});
				}
				
				//morris-data
				var message = Morris.Area({
			        element: 'message-area-chart',
			        behaveLikeLine: true,
			        data: obj.data,
			        xkey: 'regDt',
			        xLabelFormat: function(d) {
			        	var days="";
			        	if(d.getDay() == 0){
			        		days = "(일)";
			        	}else if(d.getDay() == 6){
			        		days = "(토)";
			        	};
			            return moment(d).format("MM-DD") + days;
			        },
			        
			        ykeys: ['allPaymentMessage','todayPaymentMessage'],
			        labels: ['누적메시지','수집메시지'],
			        xLabelAngle: 60,
			        pointSize: 2,
			        hideHover: 'auto',
			        resize: true,
			        smooth: true,
			        xLabels: 'day',
			        fillOpacity: 0.1,
			        lineColors: ['#0B62A4', '#FF4000'],
			        hoverCallback: function (index, options, content, row) {
			        	
			        	content = 
			        	'<div class="morris-hover-row-label">'
			        		+ row.regDt
			        	+ '</div>'
			        	
			        	+ '<div class="morris-hover-point" style="color: #0B62A4">'
			        		+ '누적메시지:' + $.number(row.allPaymentMessage) + ' (X100)'
		        		+ '</div>'
			        	
			        	+ '<div class="morris-hover-point" style="color: #FF4000">'
			        	  + '수집메시지:' + $.number(row.todayPaymentMessage)
			        	+ '</div>';
			        	
			        	
						return content;
					},
			        	
			    });
				
				$('#message-legend').empty();
				message.options.labels.forEach(function(label, i){
				    var legendItem = $('<span></span>').text(label).css('color', message.options.lineColors[i]);
				    $('#message-legend').append(legendItem);
				})
				
				
			},
			error : function(jqXHR, textStatus, errorThrown) {},
			complete : function(e) {}
		});
		//.ajax
	};
	
	$.fnPaymentMessageUpload = function() {
		
		$.ajax({
			type : 'POST',
			url : '/dashboard/statistics',
			data : {},
			async : true,
			dataType : 'json',
			beforeSend : function(xhr) {
				
				/****************************************************/
				$("#statistics-area-chart").html(
					'<a href="#" class="list-group-item">'
						+ '<div class="loading">'
							+ '<i class="fa fa-connectdevelop fa-2x gly-spin"></i>'
						+ '</div>'
					+'<div>'
				);
				
				//$("#current-install-count").text("0");
				/****************************************************/
				$("#paymentMessageUpload-area-chart").html(
					'<a href="#" class="list-group-item">'
						+ '<div class="loading">'
							+ '<i class="fa fa-connectdevelop fa-2x gly-spin"></i>'
						+ '</div>'
					+'<div>'
				);
				/****************************************************/
			},
			success : function(obj) {
				
				/****************************************************/
				$("#statistics-area-chart").empty();
				$("#paymentMessageUpload-area-chart").empty();
				/****************************************************/
				
				if(obj.data.length < 1){
					obj.data = [{
						'installed': 0
						,'used': 0
						,'deleted': 0
						,'regDt': $.format.date($.now(), "yyyy-MM-dd")
					}];
				}else{
					$.each(obj.data, function(i, o) {
						o.regDt = $.format.date(o.regDt, "yyyy-MM-dd");
					});
				}
				
				/****************************************************/
				//morris-data
				var statisticsAreaChart = Morris.Area({
			        element: 'statistics-area-chart',
			        behaveLikeLine: true,
			        data: obj.data,
			        xkey: 'regDt',
			        xLabelFormat: function(d) {
			        	
			        	var days="";
			        	
			        	if(d.getDay() == 0){
			        		days = "(일)";
			        	}else if(d.getDay() == 6){
			        		days = "(토)";
			        	};
			        	
			            return moment(d).format("MM-DD") + days;  
			        },
			        ykeys: ['todayJoinUser','todayInstalledUsed','todayInstalledDeleted'],
			        labels: ['설치','사용','삭제'],
			        xLabelAngle: 60,
			        pointSize: 2,
			        hideHover: 'auto',
			        resize: true,
			        smooth: true,
			        xLabels: 'day',
			        fillOpacity: 0.1,
			        lineColors: ['#0B62A4', '#5CB85C', '#F0AD4E']
			        	
			    });
				
				$('#statistics-legend').empty();
				statisticsAreaChart.options.labels.forEach(function(label, i){
				    var legendItem = $('<span></span>').text(label).css('color', statisticsAreaChart.options.lineColors[i]);
				    $('#statistics-legend').append(legendItem);
				});
				/****************************************************/
				
				/****************************************************/
				//morris-data
				var paymentMessageUpload = Morris.Area({
			        element: 'paymentMessageUpload-area-chart',
			        behaveLikeLine: true,
			        data: obj.data,
			        xkey: 'regDt',
			        xLabelFormat: function(d) {
			        	var days="";
			        	if(d.getDay() == 0){
			        		days = "(일)";
			        	}else if(d.getDay() == 6){
			        		days = "(토)";
			        	};
			            return moment(d).format("MM-DD") + days;  
			        },
			        xLabels: 'day',
			        xLabelAngle: 60,
			        ykeys: ['todayJoinUser','todayPaymentUpload','todayPaymentNotUpload'],
			        labels: ['설치','등록','미등록'],
			        pointSize: 2,
			        hideHover: 'auto',
			        resize: true,
			        smooth: true,
			        fillOpacity: 0.1,
			        lineColors: ['#0B62A4', '#9966FF', '#FF4000']
			    });
				
				$('#paymentMessageUpload-legend').empty();
				paymentMessageUpload.options.labels.forEach(function(label, i){
				    var legendItem = $('<span></span>').text(label).css('color', paymentMessageUpload.options.lineColors[i]);
				    $('#paymentMessageUpload-legend').append(legendItem);
				});
				/****************************************************/
				
			},
			error : function(jqXHR, textStatus, errorThrown) {
				console.log(errorThrown);
			},
			complete : function(e) {}
		});
	};
	
	//1인당 카드 보유 통계 
	$.fnCreditCardHoldStatistics = function() {
		
		$.ajax({
			type : 'POST',
			url : '/dashboard/creditCardHolds',
			data : {},
			async : true,
			dataType : 'json',
			beforeSend : function(xhr) {
				$("#creditCardHolds-chart").html(
					'<a href="#" class="list-group-item">'
						+ '<div class="loading">'
							+ '<i class="fa fa-connectdevelop fa-2x gly-spin"></i>'
						+ '</div>'
					+'<div>'
				);
			},
			success : function(data) {
				
				$("#creditCardHolds-chart").empty();
				
				if(data.length < 1){
					data = [{
						'totalCnt': 0
						,'useCnt': 0
					}];
				}
				
				//morris-data
				var statistics = Morris.Bar({
					element: 'creditCardHolds-chart',
					data: data,
					xkey: 'id',
					xLabelFormat: function(d) {
						var label = d.label;
						if(label < 5){
							return label + '장';
						}else{
							return label + '장 이상';
						}
			        },
			        xLabelAngle: 60,
					ykeys: ['used'],
					labels: ['보유수'],
					axes: true,
					grid: true,
					resize: true
			    });
			},
			error : function(jqXHR, textStatus, errorThrown) {},
			complete : function(e) {}
		});
	};
	
	//1인당 카드 보유 통계 - pie chart 
	$.fnCreditCardHoldStatisticsPieChart = function() {
		
		$.ajax({
			type : 'POST',
			url : '/dashboard/creditCardHolds',
			data : {},
			async : true,
			dataType : 'json',
			beforeSend : function(xhr) {
				$("#creditCardHolds-chart").html(
					'<a href="#" class="list-group-item">'
						+ '<div class="loading">'
							+ '<i class="fa fa-connectdevelop fa-2x gly-spin"></i>'
						+ '</div>'
					+'<div>'
				);
			},
			success : function(data) {
				
				$("#creditCardHolds-chart").empty();
				
				var pieData = [];
				var total = 0;
				
				$.each(data, function(i,d){
					
					var label = d.id;
					if(label < 5){
						label = label + '장';
					}else{
						label = label + '장 이상';
					}
					
					var count = d.used;
					total += count;

					var item = {
						label : label
						, value : count
					}
					pieData.push(item);
				});
				
				//morris-data
				var statistics = Morris.Donut({
					element: 'creditCardHolds-chart',
					
					data: pieData,
					formatter: function (value, data) {
						return $.number((value/total * 100),2) + '%'; 
					}
				});
			},
			error : function(jqXHR, textStatus, errorThrown) {},
			complete : function(e) {}
		});
	};
	
	//카드사별 카드수 
	$.fnCreditCardCompanyStatistics = function() {
		
		$.ajax({
			type : 'POST',
			url : '/dashboard/creditCardCompany',
			data : {},
			async : true,
			dataType : 'json',
			beforeSend : function(xhr) {
				$("#creditCardCompany-bar-chart").html(
					'<a href="#" class="list-group-item">'
						+ '<div class="loading">'
							+ '<i class="fa fa-connectdevelop fa-2x gly-spin"></i>'
						+ '</div>'
					+'<div>'
				);
			},
			success : function(obj) {
				
				$("#creditCardCompany-bar-chart").empty();
				
				if(obj.data.length < 1){
					obj.data = [{
						'totalCnt': 0
						,'useCnt': 0
					}];
				}
				
				var total = 0;
				$.each(obj.data, function(i,d){
					total += d.paymentCount;
				});
				
				//morris-data
				var statistics = Morris.Bar({
					element: 'creditCardCompany-bar-chart',
					data: obj.data,
					xkey: 'companyName',
					xLabelFormat: function(d) {
						return d.label.substring(0, 2);
			        },
			        xLabelAngle: 60,
					ykeys: ['paymentCount'],
					labels: ['카드수'],
					hoverCallback: function (index, options, content, row) {
						
						content = 
						'<div class="morris-hover-row-label">'
							+row.companyName
						+'</div>'
						+'<div class="morris-hover-point" style="color: #0b62a4">'
							+'카드수:'+$.number(row.paymentCount)+'('+$.number((row.paymentCount/total * 100),2)+'%'+')'
						+'</div>'
						
						
						return content;
					},
					axes: true,
					grid: true,
					resize: true
				});
			},
			error : function(jqXHR, textStatus, errorThrown) {},
			complete : function(e) {}
		});
	};
	
	//기간별 카드사용 통계
	$.fnCreditCardUsedStatistics = function() {
		
		$.ajax({
			type : 'POST',
			url : '/dashboard/creditCardUsed',
			data : {
				startDate : $('#startDate').val(),
				endDate : $('#endDate').val()
			},
			async : true,
			dataType : 'json',
			beforeSend : function(xhr) {
				
				$("#creditCardUsedCount-bar-chart").html(
					'<a href="#" class="list-group-item">'
						+ '<div class="loading">'
							+ '<i class="fa fa-connectdevelop fa-2x gly-spin"></i>'
						+ '</div>'
					+'<div>'
				);
				
				$("#creditCardUsedPrice-bar-chart").html(
					'<a href="#" class="list-group-item">'
						+ '<div class="loading">'
							+ '<i class="fa fa-connectdevelop fa-2x gly-spin"></i>'
						+ '</div>'
					+'<div>'
				);
				
			},
			success : function(obj) {
				
				$('#creditCardUsedCount-bar-chart').empty();
				$('#creditCardUsedPrice-bar-chart').empty();
				
				var countTotal = 0;
				var priceTotal = 0;
				
				if(obj.data.length < 1){
					obj.data = [{
						'companyName': 'N/N'
						,'paymentCount': 0
					}];
				}else{
					$.each(obj.data, function(i, o) {
						
						//10,000 단위로 표시
						o.paymentPrice = (o.paymentPrice/10000 | 0);
						
						countTotal += o.paymentCount;
						priceTotal += o.paymentPrice;
						
						
					});
				}
				
				//morris-data
				var statistics = Morris.Bar({
					element: 'creditCardUsedCount-bar-chart',
					data: obj.data,
					xkey: 'companyName',
					xLabelFormat: function(d) {
						return d.label.substring(0, 2);
			        },
			        xLabelAngle: 60,
					ykeys: ['paymentCount'],
					labels: ['결재건수'],
					
					hoverCallback: function (index, options, content, row) {
						
						content = 
						'<div class="morris-hover-row-label">'
							+row.companyName
						+'</div>'
						+'<div class="morris-hover-point" style="color: #0b62a4">'
							+'결제건수:'+$.number(row.paymentCount)+'('+$.number((row.paymentCount/countTotal * 100),2)+'%'+')'
						+'</div>'
						
						
						return content;
					},
					axes: true,
					grid: true,
					resize: true
				});
				
				//morris-data
				var statistics = Morris.Bar({
					element: 'creditCardUsedPrice-bar-chart',
					data: obj.data,
					xkey: 'companyName',
					xLabelFormat: function(d) {
						return d.label.substring(0, 2);
			        },
			        xLabelAngle: 60,
					ykeys: ['paymentPrice'],
					labels: ['결재금액'],
					
					hoverCallback: function (index, options, content, row) {
						
						content = 
						'<div class="morris-hover-row-label">'
							+row.companyName
						+'</div>'
						+'<div class="morris-hover-point" style="color: #0b62a4">'
							+'결제금액:'+$.number(row.paymentPrice)+'('+$.number((row.paymentPrice/priceTotal * 100),2)+'%'+')'
						+'</div>'
						
						
						return content;
					},
					
					axes: true,
					grid: true,
					stacked: true,
					resize: true
				});
			},
			error : function(jqXHR, textStatus, errorThrown) {},
			complete : function(e) {}
		});
	};

})(jQuery);