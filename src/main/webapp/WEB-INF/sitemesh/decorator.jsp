<%@ page session="true" language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		
		<title>SMARTSSEN ADMINISTRATOR</title>
		
		<!-- Bootstrap Core CSS -->
	    <link href="/webjars/bootstrap/3.3.6/css/bootstrap.css" rel="stylesheet">
	    
	    <!-- startbootstrap-sb-admin-2 -->
	    <link href="/webjars/startbootstrap-sb-admin-2/1.0.8/css/sb-admin-2.css" rel="stylesheet">
	    
	    <!-- Font-awesome -->
	    <link href="/webjars/font-awesome/4.6.2/css/font-awesome.css" rel="stylesheet" type="text/css">
	    
	    <!-- DataTables CSS -->
		<link href="/webjars/datatables/1.10.15/css/dataTables.bootstrap.css" rel="stylesheet">
		<link href="/js/plugin/datatables-select/1.2.0/css/select.bootstrap.min.css" rel="stylesheet">
		
		<!-- Bootstrap-select CSS -->
		<link href="/webjars/eonasdan-bootstrap-datetimepicker/4.17.37/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
		
		<!-- Animate CSS -->
		<link href="/webjars/animate.css/3.5.1/animate.css" rel="stylesheet">
		
		<!-- Common CSS -->
		<link href="/css/common.css" rel="stylesheet">
		
	    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	    <!--[if lt IE 9]>
	        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	    <![endif]-->
	    
		<sitemesh:write property="head" />
	</head>
	
	<body>
		<div id="wrapper">
			
			<!-- Navigation -->
			<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
				<div class="container">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-navbar-collapse">
							<span class="sr-only">Toggle navigation</span> 
							<span class="icon-bar"></span> 
							<span class="icon-bar"></span> 
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="/answer">
							<img class="logo" src="../image/common/logo_1.png">
						</a>
					</div>
					
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="bs-navbar-collapse">
						<ul class="nav navbar-nav">
							
							<c:set var="adminLevel" value="${currentUser.getUser().getAdmin_type()}" />
							
							<c:choose>
								
								<c:when test="${adminLevel eq 20}">
									<li>
										<a href="/answer">첨삭관리</a>
									</li>
									<li>
										<a href="/teacher">교사관리</a>
									</li>
									
									<!-- 
									<li>
										<a href="/manager">관리자관리</a>
									</li>
									<li>
										<a href="/inspect">검수관리</a>
									</li>
									-->
									<li>
										<a href="/fee">정산관리</a>
									</li>
									<!--
									<li>
										<a href="/app">APP관리</a>
									</li>
									-->
									<li>
										<a href="/logout">로그아웃</a>
									</li>
								</c:when>
								
								<c:when test="${adminLevel eq 30}">
									<li>
										<a href="/answer">첨삭관리</a>
									</li>
									<li>
										<a href="/teacher">교사관리</a>
									</li>
									
									<!-- 
									<li>
										<a href="/manager">관리자관리</a>
									</li>
									-->
									<li>
										<a href="/inspect">검수관리</a>
									</li>
									<!--
									<li>
										<a href="/fee">정산관리</a>
									</li>
									
									<li>
										<a href="/app">APP관리</a>
									</li>
									-->
									<li>
										<a href="/logout">로그아웃</a>
									</li>
								</c:when>
								
								<c:otherwise>
									<li>
										<a href="/answer">첨삭관리</a>
									</li>
									<li>
										<a href="/teacher">교사관리</a>
									</li>
									<li>
										<a href="/manager">관리자관리</a>
									</li>
									<li>
										<a href="/inspect">검수관리</a>
									</li>
									<li>
										<a href="/fee">정산관리</a>
									</li>
									<li>
										<a href="/app">APP관리</a>
									</li>
									<li>
										<a href="/logout">로그아웃</a>
									</li>
								</c:otherwise>
							</c:choose>
							
							
							
							
						</ul>
						
						<ul class="nav navbar-nav navbar-right">
							<li>
								<a href="#">
			                        <span class="glyphicon glyphicon-user"></span> 
			                        <strong>${currentUser.getUser().getAdmin_nm()}(${currentUser.getUser().getAdmin_id()})</strong>
			                    </a>
							</li>
							<!-- 
			                <li class="dropdown">
			                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
			                        <span class="glyphicon glyphicon-user"></span> 
			                        <strong>Nombre</strong>
			                        <span class="glyphicon glyphicon-chevron-down"></span>
			                    </a>
			                    <ul class="dropdown-menu">
			                        <li>
			                            <div class="navbar-login">
			                                <div class="row">
			                                    <div class="col-lg-4">
			                                        <p class="text-center">
			                                            <span class="glyphicon glyphicon-user icon-size"></span>
			                                        </p>
			                                    </div>
			                                    <div class="col-lg-8">
			                                        <p class="text-left"><strong>Nombre Apellido</strong></p>
			                                        <p class="text-left small">correoElectronico@email.com</p>
			                                        <p class="text-left">
			                                            <a href="#" class="btn btn-primary btn-block btn-sm">Actualizar Datos</a>
			                                        </p>
			                                    </div>
			                                </div>
			                            </div>
			                        </li>
			                        <li class="divider"></li>
			                        <li>
			                            <div class="navbar-login navbar-login-session">
			                                <div class="row">
			                                    <div class="col-lg-12">
			                                        <p>
			                                            <a href="#" class="btn btn-danger btn-block">Cerrar Sesion</a>
			                                        </p>
			                                    </div>
			                                </div>
			                            </div>
			                        </li>
			                    </ul>
			                </li>
			                 -->
			            </ul>
					</div>
					<!-- /.navbar-collapse -->
					
					
				</div>
				<!-- /.container -->
			</nav>
			
			<sitemesh:write property="body" />
			
		</div>
		<!-- /#wrapper -->
		
		<!-- jQuery -->
		<script src="/webjars/jquery/2.1.4/dist/jquery.min.js"></script>
		
		<!-- Bootstrap Core JavaScript -->
		<script src="/webjars/bootstrap/3.3.6/js/bootstrap.js"></script>
		
		<!-- jquery-validation -->
		<script src="/webjars/jquery-validation/1.14.0/dist/jquery.validate.js"></script>
		<script src="/webjars/jquery-validation/1.14.0/dist/additional-methods.js"></script>
		<script src="/webjars/jquery-validation/1.14.0/src/localization/messages_ko.js"></script>
		
		<!-- DataTables JavaScript -->
		<script src="/webjars/datatables/1.10.15/js/jquery.dataTables.js"></script>
		<script src="/webjars/datatables/1.10.15/js/dataTables.bootstrap.js"></script>
		
		<script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
		<script src="https://cdn.datatables.net/responsive/2.1.0/js/responsive.bootstrap.min.js"></script>
		
		<!-- moment -->
		<script src="/webjars/moment/2.12.0/moment.js"></script>
		<script src="/webjars/moment/2.12.0/locale/ko.js"></script>
		<script src="/webjars/moment-range/2.2.0/dist/moment-range.js"></script>
				
		<!-- bootstrap-datetimepicker -->
		<!-- bootstrap-datetimepicker requires Moment.js to be loaded first -->
		<script src="/webjars/eonasdan-bootstrap-datetimepicker/4.17.37/build/js/bootstrap-datetimepicker.min.js"></script>
		
		<!-- jquery-blockui -->
	    <script src="/webjars/jquery-blockui/2.65/jquery.blockUI.js"></script>
	    
	    <!-- bootstrap-notify -->
		<script src="/webjars/bootstrap-growl/3.1.3/bootstrap-notify.js"></script>
		
		<script src="/js/jquery.tmpl.js"></script>
		
		<script src="/js/jquery-dateFormat.min.js"></script>
		
		<script src="/js/jquery.number.min.js"></script>
		
		<script src="/js/plugin/datatables-select/1.2.0/js/dataTables.select.min.js"></script>
		<script src="/js/plugin/datatables-rowsgroup/dataTables.rowsGroup.js"></script>
		
		<!-- jquery.redirect.js -->
		<script src="/js/jquery.redirect.js"></script>
		
		<!-- jquery.fileDownload.js -->
		<script src="/js/jquery.fileDownload.js"></script>

		<!-- Common JavaScript -->
		<script src="/js/common.js"></script>
		
		<!-- Common DataTables -->
		<script src="/js/data-tables.js"></script>
		<script src="/js/jquery.cookie.js"></script>
		
		<script id="tmpl-nodata" type="text/x-jquery-tmpl">
			<tr>
				<td colspan=9 class="text-center">데이터가 없습니다</td>
			</tr>
		</script>

	    <sitemesh:write property="page.local_script"/>
	</body>
	
	
	<div id="footer">
		<img src="/image/common/copyright.png">
	</div>
	<!-- ./ footer -->
	
</html>