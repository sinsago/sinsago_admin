<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head></head>
<body>
	<!-- Page Content -->
	<div class="container">
		<div class="panel panel-info">
			<div class="panel-heading">정산검색</div>
			<div class="panel-body">
				
				<div class="form-group form-inline">
					<label class="control-label col-sm-1">기간</label>
					
					<div class='input-group date' id='datetimepicker-start'>
						<input type='text' class="form-control" id="search-start" /> 
						<span class="input-group-addon"> 
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>
					
					<button type="button" class="btn btn-primary" id="btn-search">
						<span class="fa fa-search"></span> 조회
					</button>
				</div>
			</div>
		</div>
		
		<!-- /.panel 버튼 -->
		<div class="panel panel-success">
			<div class="panel-heading">
				<div class="btn-group pull-right">
					<button type="button" class="btn btn-primary" id="btn-excel-fee" disabled="disabled">
						<span class="fa fa-file-excel-o"></span> 엑셀저장
					</button>
				
					<button type="button" class="btn btn-primary" id="btn-save-fee"  disabled="disabled">
						<span class="fa fa-krw"></span> 정산대기
					</button>
				</div>
		         
				<div class="clearfix"></div>
	        	
	        </div>

			<div class="panel-body">
				
				<ul class="nav nav-tabs" id="tabs" style="margin-bottom: 10px;">
					<li class="active">
						<a href="#tab1" data-toggle="tab">첨삭 정산 관리</a>
					</li>
					<li>
						<a href="#tab2" data-toggle="tab">승인 정산 관리</a>
					</li>
	            </ul>
	            
	            <div class="tab-content">
					<div id="tab1" class="tab-pane fade in active">
						<div class="table-responsive">
							<table class="table table-bordered table-hover table-striped" id="dt-month" width="100%">
								<thead>
									<tr>
										<th class="dt-center" width="30px">No.</th>
										<th class="dt-center">교사명</th>
										<th class="dt-center">교사ID</th>
										<th class="dt-center">등록일</th>
										<th class="dt-center">승인완료 건수</th>
										<th class="dt-center">적용 수수료</th>
										<th class="dt-center">수수료 적용일</th>
										<th class="dt-center">소계</th>
										<th class="dt-center">정산금액</th>
									</tr>
									<tr>
										<th class="dt-center">총계</th>
										<th>-</th>
										<th>-</th>
										<th>-</th>
										<th>
											<span id='approve-tot'>0</span>
										</th>
										<th>-</th>
										<th>-</th>
										<th colspan="2" class="dt-center">
											<span id='fee-tot'>0</span>
										</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
					
					<div id="tab2" class="tab-pane fade">
						<div class="table-responsive">
							<table class="table table-bordered table-hover table-striped" width="100%">
								<thead>
									<tr>
										<th class="dt-center">No.</th>
										<th>관리자ID</th>
										<th>관리자명</th>
										<th>등록일</th>
										<th>승인완료 건수</th>
										<th>적용 수수료</th>
										<th>정산금액</th>
									</tr>
								</thead>
								<tbody id="tb-approve"></tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.panel 교사 리스트 -->
	</div>
	<!-- /.container -->
	
	<content tag="local_script">

		<script>
		    $(document).ready(function () {
				
		    	var _DATA_TABLE;
		    	
		        $('#datetimepicker-start').datetimepicker({
		            locale: 'ko'
		            , format: 'YYYY-MM'
		            , defaultDate: moment().format('YYYY-MM')
		            , viewDate: false
		            , viewMode: 'months'
		        });
		
		        /*************************************/
		        //정산 테이이블 생성
				//$.fnMonthlyFee();
		        
				// tmpl to datatables
				//DATA TABLE
				_DATA_TABLE = $.fnDataTablesMonth();
				/*************************************/
				
				$.fnFeeTabs = function(index) {
					switch(index) {
						case 0:
							
							//$.fnMonthlyFee();
							//$.fnDataTablesMonth();
							_DATA_TABLE.draw();
							
							break;
							
						case 1:
							
							//승인정산관리
							$.fnMonthlyApproveFee();
							
							break;
							
						default:
							break;
					}
					
				}
		        
				//검색
				$('#btn-search').on('click', function (e) {
					var index = $('.nav-tabs .active').index();
					$.fnFeeTabs(index);
				});
		
		        //check datetimepicker
		        $("#datetimepicker-start").on("dp.change", function (e) {
		        	var index = $('.nav-tabs .active').index();
					$.fnFeeTabs(index);
		        });
		        
		      	//tab click event
				$('.nav-tabs a').click(function (e) {
					$(this).tab('show');
					var index = $('.nav-tabs .active').index();
					$.fnFeeTabs(index);
				});
		
		        $('#btn-excel-fee').on('click', function (e) {
		        	var index = $('.nav-tabs .active').index();
		        	switch(index) {
		        		case 0:
							$.fileDownload("/fee/excel/download?month=" + $('#search-start').val())
								.done(function (e) {
									$.notify({message: "다운로드가 완료되었습니다", icon: 'fa fa-info-circle'}, {type: 'success'});
								})
								.fail(function (e) {
									$.notify({message: jqXHR.responseText, icon: 'fa fa-warning'}, {type: 'danger'});
								})
								return false; //this is critical to stop the click event which will trigger a normal file download!
							break;
							
						case 1:
							//승인정산관리
							$.fileDownload("/fee/approve/excel/download?month=" + $('#search-start').val())
								.done(function (e) {
									$.notify({message: "다운로드가 완료되었습니다", icon: 'fa fa-info-circle'}, {type: 'success'});
								})
								.fail(function (e) {
									$.notify({message: jqXHR.responseText, icon: 'fa fa-warning'}, {type: 'danger'});
								})
								return false; //this is critical to stop the click event which will trigger a normal file download!
							break;
						default:
							break;
					}
				});
		
		        //정산 처리
		        $('#btn-save-fee').on('click', function (e) {
		
		            if (confirm('정산을 완료하시겠습니까?')) {
		            	
		            	var index = $('.nav-tabs .active').index();
		            	
		            	var url;
		            	switch(index) {
			        		case 0:
			        			url = '/fee/save'
								break;
								
							case 1:
								//승인정산관리
								url = '/fee/approve/save'
								break;
							default:
								break;
						}
		            	
		                $.ajax({
		                    type: 'POST',
		                    url: url,
		                    data: {
		                        month: $('#search-start').val()
		                    },
		                    async: true,
		                    dataType: 'json',
		                    beforeSend: function (xhr) {
		                        $.blockUI();
		                    },
		                    success: function (data) {
		                        $.notify({message: "정산이 완료 되었습니다", icon: 'fa fa-info-circle'}, {type: 'success'});
		    					$.fnFeeTabs(index);
		                    },
		                    error: function (jqXHR, textStatus, errorThrown) {
		                        $.notify({message: jqXHR.responseJSON.message, icon: 'fa fa-warning'}, {type: 'danger'});
		                    },
		                    complete: function (e) {
		                        $.unblockUI();
		                    }
		                });
		                //.ajax
		            };
		        });
		    });
			
		    
			//정산 리스트
			$.fnMonthlyFee = function () {
				$.ajax({
				    type: 'POST',
				    url: '/fee/month',
				    data: {
						month: $('#search-start').val()
				    },
				    async: true,
				    dataType: 'json',
				    beforeSend: function (xhr) {
						$.blockUI();
					},
					success: function (data) {
						
					    $('#tb-fee').empty();
						if (data.length > 1) {
							$('#tmpl-fee')
								.tmpl(data)
								.appendTo($('#tb-fee'));
							 
							//active excel
							$('#btn-excel-fee').attr('disabled', false);
							
							
							console.log(">>> "+data.isComplete);
							
							//change btn
							if (data[1].calc_state > 0) {
								$('#btn-save-fee').attr('disabled', true);
								$('#btn-save-fee').html('<span class="fa fa-krw"></span> 정산완료');
							} else {
							
								$('#btn-save-fee').attr('disabled', false);
								$('#btn-save-fee').html('<span class="fa fa-krw"></span> 정산대기');
								
								if (data[0].thisMonth) {
									$('#btn-save-fee').attr('disabled', true);
								}
							}
						} else {
							$('#tmpl-nodata')
								.tmpl()
								.appendTo($('#tb-fee'));
							
							//active excel
							$('#btn-excel-fee').attr('disabled', true);
							
							$('#btn-save-fee').attr('disabled', true);
							$('#btn-save-fee').html('<span class="fa fa-krw"></span> 정산대기');
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {},
					complete: function (e) {
						$.unblockUI();
					}
				});
			};
			
			//승인정산관리
			$.fnMonthlyApproveFee = function () {
				$.ajax({
				    type: 'POST',
				    url: '/fee/approve/month',
				    data: {
						month: $('#search-start').val()
				    },
				    async: true,
				    dataType: 'json',
				    beforeSend: function (xhr) {
						$.blockUI();
					},
					success: function (d) {
						
						var data = d.data;
						var state = d.state;
						
					    $('#tb-approve').empty();
					    
						if (data.length > 1) {
							$('#tmpl-approve')
								.tmpl(data)
								.appendTo($('#tb-approve'));
							
							//active excel
							$('#btn-excel-fee').attr('disabled', false);
							
							//change btn
							if (!$.isBlank(state)) {
								
								if(state.approve_state > 0){
									$('#btn-save-fee').attr('disabled', true);
									$('#btn-save-fee').html('<span class="fa fa-krw"></span> 정산완료');
								}else{
									$('#btn-save-fee').attr('disabled', false);
									$('#btn-save-fee').html('<span class="fa fa-krw"></span> 정산대기');
								}
								
							} else {
								$('#btn-save-fee').attr('disabled', false);
								$('#btn-save-fee').html('<span class="fa fa-krw"></span> 정산대기');
							}
							
						} else {
							$('#tmpl-nodata')
								.tmpl()
								.appendTo($('#tb-approve'));
							
							//active excel
							$('#btn-excel-fee').attr('disabled', true);
							$('#btn-save-fee').attr('disabled', true);
							$('#btn-save-fee').html('<span class="fa fa-krw"></span> 정산대기');
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {},
					complete: function (e) {
						$.unblockUI();
					}
				});
			};
		</script>
	
		<script id="tmpl-fee" type="text/x-jquery-tmpl">

			{{if fee_seq == 0}}
				<tr>
					<td class="dt-center">총계</td>
					<td>-</td>
					<td>-</td>
					<td>-</td>
					<td>-</td>
					<td>-</td>
					<td>\${$.number(an_approve_cnt)}</td>
					<td>-</td>
					<td>\${$.number(calc_amount)}</td>
				</tr>
			{{else}}
				<tr>
					<td class="dt-center">\${rownum-1}</td>
					<td>\${st_id}</td>
					<td>\${st_nm}</td>
					<td>\${$.fnSetTeachersTypeText(st_type)}</td>
					<td>\${$.format.date(reg_date, "yyyy-MM-dd")}</td>
					<td>\${st_rrn}</td>
					<td>\${$.number(an_approve_cnt)}</td>
					<td>\${$.number(calc_fee)}</td>
					<td>\${$.number(calc_amount)}</td>
				</tr>
			{{/if}}
		</script>
		
		<script id="tmpl-approve" type="text/x-jquery-tmpl">

			{{if total == 0}}
				<tr>
					<td class="dt-center">총계</td>
					<td>-</td>
					<td>-</td>
					<td>-</td>
					<td>\${$.number(approve_cnt)}</td>
					<td>-</td>
					<td>\${$.number(calc_amount)}</td>
				</tr>
			{{else}}
				<tr>
					<td class="dt-center">\${rownum}</td>
					<td>\${admin_id}</td>
					<td>\${admin_nm}</td>
					<td>\${$.format.date(reg_date, "yyyy-MM-dd")}</td>
					<td>\${$.number(approve_cnt)}</td>
					<td>\${$.number(calc_fee)}</td>
					<td>\${$.number(calc_amount)}</td>
				</tr>
			{{/if}}
		</script>
	</content>
</body>
</html>