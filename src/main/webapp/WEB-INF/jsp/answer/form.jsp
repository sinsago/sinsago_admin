	<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
	<head></head>
	<body>
		<!-- Page Content -->
		<div class="container">
    		<div class="panel-group">
				<div class="panel panel-default">
					<div class="panel-heading">첨삭 상세</div>
					<div class="panel-body">
						<form class="form-horizontal" role="form">
							<div class="form-group">
								<label class="control-label col-sm-1" for="an_id">답안ID</label>
								<div class="col-sm-5">
									<h4 class="text-info" id="an_id" data-value="${nInfo.an_id}">${nInfo.an_id}</h4>
								</div>
								
								<label class="control-label col-sm-1" for="st_pwd">학습자</label>
								<div class="col-sm-5">
									<h4 class="text-info" id="student_nm"> ${nInfo.student_nm} </h4>
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-sm-12">
									
									<div class="well well-sm">
										<img alt="" class="img-thumbnail img-responsive center-block" src="${nImage}">
									</div>
									
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label col-sm-1" for="st_id">담당교사</label>
								<div class="col-sm-4">
									<select class="form-control" id="st_id" name="st_id"></select>
								</div>
								
							</div>
							
							<div class="form-group">
								<label class="control-label col-sm-1" for="an_state">상태</label>
								<div class="col-sm-10">
									<div class="btn-group" id="note-state" data-toggle="buttons">
									
										<label class="btn btn-default btn-set-or-raise">
											<input type="radio" id="state-0" name="an_state" value=0>첨삭대기
										</label>
		
										<label class="btn btn-default">
											<input type="radio" id="state-1" name="an_state" value=1>첨삭중
										</label>
		
										<label class="btn btn-default">
											<input type="radio" id="state-2" name="an_state" value=2>승인대기
										</label>
										
										<label class="btn btn-default">
											<input type="radio" id="state-3" name="an_state" value=3>반려
										</label>
										
										<label class="btn btn-default">
											<input type="radio" id="state-4" name="an_state" value=4>승인완료
										</label>
										
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<!-- /.panel 첨삭 상세 -->
				
				<div class="panel panel-default">
					<div class="panel-body">
					
						<!-- <a href="/answer" class="btn btn-primary"> -->
						<a href="javascript: history.back(1);" class="btn btn-primary">
							<span class="fa fa-table"></span> 목록
						</a>
						<div class="btn-group pull-right">
							<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-logs">
								<span class="fa fa-stack-exchange"></span> 로그
							</button>
							<button type="button" class="btn btn-primary" id="btn-save">
								<span class="fa fa-floppy-o"></span> 저장
							</button>
						</div>
					</div>
				</div>
				<!-- /.panel 버튼 -->
  			</div>
		</div>
	    <!-- /.container -->
	    
	    <div id="modal-logs" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">로그</h4>
					</div>
					<div class="modal-body">
						<div class="table-responsive">
							<table class="table table-bordered table-hover table-striped">
								<thead>
									<tr>
										<th>일자</th>
										<th>내용</th>
									</tr>
								</thead>
								<tbody id="tb-logs"></tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.modal -->
		
		<content tag="local_script">
			
			<script>
				$(document).ready(function() {
					
					//GET TEACHERS
					$.fnTeachers('${nInfo.st_id}');
					
					//SET PAGE-SETTING
					//$('#st_id').val('${nInfo.st_id}');
					var state = ("${nInfo.an_state}");
					
					$('input:radio[name=an_state][value='+state+']').attr('checked', true);
					$.fnNoteStateHandler(parseInt(state));
					
					/**********************************/
					//TODO STATE BUTTON DISABLE SETTING
					/**********************************/
					
					//btn-save 첨삭상태 저장
					$('#btn-save').on('click', function(e){
						e.preventDefault();
						
						if(confirm('저장하시겠습니까?')) {
							
							$.ajax({
								type : 'POST',
								url : '/answer/save',
								data: {
									an_id : $('#an_id').data("value")
									, st_id : $('#st_id').val()
									, an_state : $('input[name=an_state]:checked').val()
								},
								async : true,
								dataType : 'json',
								beforeSend : function(xhr) {
									$.blockUI();
								},
								success : function(data) {
									//$.redirect("/answer");
									$.notify({message: "저장되었습니다",icon: 'fa fa-info-circle'},{type: 'success'});
									
									location.reload();
									
								},
								error : function(jqXHR, textStatus, errorThrown) {
									$.notify({message: jqXHR.responseJSON.message,icon: 'fa fa-warning'},{type: 'danger'});
								},
								complete : function(e) {
									$.unblockUI();
								}
							});
							//.ajax
						};
					});
					
					//modal logs
					$('#modal-logs').on("show.bs.modal", function(e) {
						
						//call modal target
						var rt = $(e.relatedTarget); 
						
						$.ajax({
							type : 'POST',
							url : '/answer/logs',
							data: {
								anId : $('#an_id').data("value")
							},
							async : true,
							dataType : 'json',
							beforeSend : function(xhr) {
								$.blockUI(null);
								
								/* $('#tb-logs').empty();
								$('#tmpl-logs')
								.tmpl()
								.appendTo($('#tb-logs')); */
							},
							success : function(data) {
								
								$('#tb-logs').empty();
								if(data.length > 0){
									$('#tmpl-logs')
									.tmpl(data)
									.appendTo($('#tb-logs'));
								}else{
									$('#tmpl-nodata')
									.tmpl()
									.appendTo($('#tb-logs'));
								}
							},
							error : function(jqXHR, textStatus, errorThrown) {
								$('#modal-logs').modal('hide');
								$.notify({message: jqXHR.responseJSON.message,icon: 'fa fa-warning'},{type: 'danger'});
							},
							complete : function(e) {
								$.unblockUI();
							}
						});
						//.ajax
					});
				});
				
				//GET Teachers
				$.fnTeachers = function(stId) {
					
					$.ajax({
						type : 'POST',
						url : '/teacher/teachers',
						data : {},
						async : true,
						dataType : 'json',
						beforeSend : function(xhr) {},
						success : function(data) {

							$('#st_id').empty();

							$("#tmpl-teachers")
							.tmpl(data)
							.appendTo($('#st_id'));
							
							//setting teacher
							$('#st_id').val(stId);
							
						},
						error : function(jqXHR, textStatus, errorThrown) {},
						complete : function(e) {}
					});
				};
			</script>
			
			<script id="tmpl-teachers" type="text/x-jquery-tmpl">
				<option value="\${st_id}">\${st_nm}</option>
			</script>
			
			<script id="tmpl-logs" type="text/x-jquery-tmpl">
				<tr>
					<td>
						\${$.format.date(reg_date, "yyyy-MM-dd HH:mm")}
					</td>
					<td>
						
						{{if writer_id != ''}}
							<span class="badge">
		    					<span>\${writer_nm}</span>
		    				</span>
						{{/if}}

 
						{{if writer_type == 10}}
		    				<span class="badge">
		    					<span>관리자</span>
		    				</span>
						{{else writer_type == 20}}
		    				<span class="badge">
		    					<span>교사</span>
		    				</span>
						{{/if}}

						
						{{if an_state == 0}}
							{{if rownum == total}}
								<span class="badge">
		    						<span>답안 최초 등록</span>
		    					</span>						
							{{else rownum != total && writer_nm != ''}}
								<span class="badge">
		    						<span>\${writer_nm} 최초 열람</span>
		    					</span>
							{{/if}}
		    				
						{{else an_state == 1}}
		    				<span class="badge">
		    					<span>첨삭중 상태로 저장</span>
		    				</span>
						{{else an_state == 2}}
		    				<span class="badge">
		    					<span>승인요청</span>
		    				</span>
						{{else an_state == 3}}
		    				<span class="badge">
		    					<span>반려</span>
		    				</span>
						{{else an_state == 4}}
		    				<span class="badge">
		    					<span>승인완료</span>
		    				</span>
						{{/if}}
					</td>
				</tr>
			</script>
			
			<script id="tmpl-nodata" type="text/x-jquery-tmpl">
				<tr>
					<td colspan=10 class="text-center">데이터가 없습니다</td>
				</tr>
			</script>
		</content>
	</body>
</html>