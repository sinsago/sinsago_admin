<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head></head>
<body>
	<!-- Page Content -->
	<div class="container">
        <div class="panel panel-info">
            <div class="panel-heading">첨삭 검색</div>
            <div class="panel-body">
                <form class="form-horizontal" role="form">
                    <!-- 기간 -->
                    <div class="form-group form-inline">
                        <label class="control-label col-sm-1">기간</label>
                        <div class="col-sm-8">

                            <select class="form-control" id="date-type">
                                <option value="10">첨삭요청일</option>
                                <option value="20" class="hidden">첨삭목표일</option>
                                <option value="30">승인요청일</option>
                                <option value="40" class="hidden">승인완료일</option>
                            </select>
                            
                            <div class='input-group date' id='datetimepicker-start'>
                                <input type='text' class="form-control" id="search-start"/>
                                <span class="input-group-addon">
                                	<i class="glyphicon glyphicon-calendar"></i>
                                </span>
                            </div>

                            <span class="label label-default">~</span>

                            <div class='input-group date' id='datetimepicker-end'>
                                <input type='text' class="form-control" id="search-end"/>
                                <span class="input-group-addon">
                                	<i class="glyphicon glyphicon-calendar"></i>
								</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-inline">
                        <label class="control-label col-sm-1">학년</label>
                        <div class="col-sm-6">
                            <select class="form-control" id="student-type">
                                <option value="10">초등</option>
                                <option value="20">중등</option>
                                <option value="30">고등</option>
                            </select>
                            <select class="form-control" id="grade-type">
                                <option value="0">전체</option>
                                <option value="1">1학년</option>
                                <option value="2">2학년</option>
                                <option value="3">3학년</option>
                                <option value="4">4학년</option>
                                <option value="5">5학년</option>
                                <option value="6">6학년</option>
                                <option value="7">7학년</option>
                            </select>
                        </div>
                    </div>

                    <!-- 검색어 -->
                    <div class="form-group form-inline">
                        <label class="control-label col-sm-1" for="search-type">검색어</label>
                        <div class="col-sm-8">
                            <select class="form-control" id="search-type">
                                <option value="10">전체</option>
                                <option value="20">담당교사</option>
                                <option value="30">아이디</option>
                                <option value="40">답안ID</option>
                                <option value="50">교재명</option>
                            </select>

                            <input type="text" class="form-control" id="search-word">

                            <button type="button" class="btn btn-primary" id="btn-search">
                                <span class="fa fa-search"></span> 조회
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.panel 첨삭 검색 -->

        <!-- /.panel 버튼 -->
        <div class="panel panel-success">
            <div class="panel-heading">
				<span style="float: left; margin-top: 5px;">
					첨삭 리스트
				</span>
            	
            	<div class="btn-group pull-right">
					<button type="button" class="btn btn-primary" id="btn-excel-answer">
					    <span class="fa fa-file-excel-o"></span> 엑셀 다운로드
					</button>
					
					<button type="button" class="btn btn-primary" id="btn-approve">
					    <span class="fa fa-check-square-o"></span> 선택승인
					</button>
				</div>
	            
	            <div class="clearfix"></div>
            	
            </div>
            
            <div class="panel-body">
            	<ul class="nav nav-tabs" id="mytabs">
	                <li>
	                    <a href="#an-waiting" data-toggle="tab">
	                    	첨삭대기 <span class="badge" id="tabs-waiting">0</span>
	                    </a>
	                </li>
	                <li>
	                    <a href="#an-running" data-toggle="tab">
	                    	첨삭중 <span class="badge" id="tabs-running">0</span>
	                    </a>
	                </li>
	                <li class="active">
	                    <a href="#an-pending" data-toggle="tab">
	                    	승인대기 <span class="badge" id="tabs-pending">0</span>
	                    </a>
	                </li>
	                <li>
	                    <a href="#an-blocked" data-toggle="tab">
	                    	반려 <span class="badge" id="tabs-blocked">0</span>
	                    </a>
	                </li>
	                <li>
	                    <a href="#an-approve" data-toggle="tab">
	                    	승인완료 <span class="badge" id="tabs-approve">0</span>
	                    </a>
	                </li>
	            </ul>
	
	            <div class="table-responsive">
	                <table class="table table-bordered table-hover table-striped" id="dt-note" width="100%">
	                    <thead>
	                    <tr>
	                        <th>
	                            <button type="button" class="btn btn-default" id="btn-checkbox-all">
	                                <i class="glyphicon glyphicon-check"></i>
	                            </button>
	                        </th>
	                        <th>No.</th>
	                        <th>답안ID</th>
	                        <th>학년</th>
	                        <th>교재명</th>
	                        <th>페이지</th>
	                        <th>아이디</th>
	                        <th>학습자</th>
	                        <th>첨삭요청일</th>
	                        <th>첨삭목표일</th>
	                        <th>승인요청일</th>
	                        <th>승인완료일</th>
	                        <th>담당교사</th>
	                    </tr>
	                    </thead>
	                </table>
	            </div>
            </div>
            
            <div class="panel-footer clearfix">
                <div class="form-group form-inline ">
                    <div class="pull-right">
                        <select class="form-control hide" id="teacher-type"></select>
                        <button type="button" class="btn btn-primary hide" id="btn-change">
                            <span class="fa fa-check-square-o"></span> 담당교사 변경
                        </button>
                    </div>
                </div>
            </div>
            
        </div>
    	<!-- /.panel 첨삭 리스트 -->
	</div>
	<!-- /.container -->
	
	<div id="modal-detail" class="modal fade" role="dialog" style="display: none; z-index: 1050;">
		<div class="modal-dialog modal-lg">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">상세정보</h4>
				</div>
				<div class="modal-body">
					
					<div class="panel-group">
						<div class="panel panel-default">
							<div class="panel-heading">첨삭 상세</div>
							<div class="panel-body">
								<form class="form-horizontal" role="form">
									<div class="form-group">
										<label class="control-label col-sm-1" for="an_id">답안ID</label>
										<div class="col-sm-5">
											<h4 class="text-info" id="an_id" data-value></h4>
										</div>
										
										<label class="control-label col-sm-1" for="student_nm">학습자</label>
										<div class="col-sm-5">
											<h4 class="text-info" id="student_nm"></h4>
										</div>
									</div>
									
									<div class="form-group">
										<div class="col-sm-12">
											<div class="well well-sm">
												<img alt="" class="img-thumbnail img-responsive center-block" id="noteImage">
											</div>
										</div>
									</div>
									
									<div class="form-group">
										<label class="control-label col-sm-1" for="st_id">담당교사</label>
										<div class="col-sm-4">
											<select class="form-control" id="st_id" name="st_id"></select>
										</div>
										
									</div>
									
									<div class="form-group">
										<label class="control-label col-sm-1" for="an_state">상태</label>
										<div class="col-sm-10">
											<div class="btn-group" id="note-state" data-toggle="buttons">
											
												<label class="btn btn-default btn-set-or-raise">
													<input type="radio" id="state-0" name="an_state" value=0>첨삭대기
												</label>
				
												<label class="btn btn-default">
													<input type="radio" id="state-1" name="an_state" value=1>첨삭중
												</label>
				
												<label class="btn btn-default">
													<input type="radio" id="state-2" name="an_state" value=2>승인대기
												</label>
												
												<label class="btn btn-default">
													<input type="radio" id="state-3" name="an_state" value=3>반려
												</label>
												
												<label class="btn btn-default">
													<input type="radio" id="state-4" name="an_state" value=4>승인완료
												</label>
											</div>
										</div>
									</div>
								</form>
							</div>
							<!-- /. panel-body -->
						</div>
						<!-- /.panel 첨삭 상세 -->
						
						<!-- 반려사유 -->
						<div class="panel panel-warning hidden" id="panel-refuse-add">
							<div class="panel-body">
								<div id="refuse-add"></div>
							</div>
						</div>
						
						<!-- 반려사유 -->
						<div class="panel panel-warning hidden" id="panel-refuse">
							<div class="panel-body">
								<div id="refuse"></div>
							</div>
						</div>
						
						<!-- /.panel 버튼 -->
						<div class="panel panel-default">
							<div class="panel-body">
							
								<!-- <a href="/answer" class="btn btn-primary"> 
								<a href="javascript: history.back(1);" class="btn btn-primary">
									<span class="fa fa-table"></span> 목록
								</a>
								-->
								
								<div class="btn-group pull-right">
									<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-logs">
										<span class="fa fa-stack-exchange"></span> 로그
									</button>
									<button type="button" class="btn btn-primary" id="btn-save">
										<span class="fa fa-floppy-o"></span> 저장
									</button>
								</div>
							</div>
						</div>
						<!-- /.panel 버튼 -->
		  			</div>
		  			
				</div>
			</div>
		</div>
	</div>
	<!-- /.modal -->
	
	<div id="modal-logs" class="modal fade" role="dialog" style="display: none; z-index: 1060;">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">로그</h4>
				</div>
				<div class="modal-body">
					<div class="table-responsive">
						<table class="table table-bordered table-hover table-striped">
							<thead>
								<tr>
									<th>일자</th>
									<th>내용</th>
								</tr>
							</thead>
							<tbody id="tb-logs"></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /.modal -->

	<content tag="local_script">
		<script>
			$(document).ready(function() {
				
				var _DATA_TABLE;
				$.fnTeachers();
				
				//$('.nav-tabs a').on('shown.bs.tab', function (e) {
					//var x = $(event.target).text();         // active tab
					//var y = $(event.relatedTarget).text();  // previous tab
					//$.cookie('anTabs', '1', {expires: 7, path: '/'});
				//});
				
				$('#datetimepicker-start').datetimepicker({
				    locale: 'ko'
				    , format: 'YYYY-MM-DD'
				    , defaultDate: moment().add(-1, 'month').format('YYYY-MM-DD')
				});
				
				$('#datetimepicker-end').datetimepicker({
				    useCurrent: false //Important! See issue #1075
				    , locale: 'ko'
				    , format: 'YYYY-MM-DD'
				    , defaultDate: moment().format('YYYY-MM-DD')
				});
				
				//check datetimepicker
				$("#datetimepicker-start").on("dp.change", function (e) {
				    $('#datetimepicker-end').data("DateTimePicker").minDate(e.date);
				});
				
				$("#datetimepicker-end").on("dp.change", function (e) {
				    $('#datetimepicker-start').data("DateTimePicker").maxDate(e.date);
				});
			
				//DATA TABLE
				_DATA_TABLE = $.fnDataTablesNotes();
				
				$('input:text').keydown(function(e) {
					if (e.keyCode == 13) {
						_DATA_TABLE.draw();
						return false;    //<---- Add this line
					}
				});
				
				//검색
				$('#btn-search').on('click', function (e) {
				    _DATA_TABLE.draw();
				});
				
				//담당교사 변경
				$('#btn-change').on('click', function (e) {
					var items = _DATA_TABLE.rows({selected: true});
				    var count = items.count();
				
				    if (count > 0) {
				        if (confirm('선택한 첨삭의 담당 교사를 일괄 변경하시겠습니까?')) {
				        	
				        	//console.log($('#teacher-type').val());
				        	
							var rows = items.data();
							
							var data = [];
							$.each(rows, function (i, row) {
								data.push(row.an_id);
							});
							
							$.ajax({
								type: 'POST',
								url: '/answer/change',
								data: {
									anIds: data
									, stId: $('#teacher-type').val()
								},
								async: true,
								dataType: 'json',
								beforeSend: function (xhr) {
									$.blockUI();
								},
								success: function (data) {
									_DATA_TABLE.draw(false);
								},
								error: function (jqXHR, textStatus, errorThrown) {},
								complete: function (e) {
								    $.unblockUI();
								}
							});
			       		}
				    }else{
				    	$.notify({message: '선택한 첨삭이 없습니다', icon: 'fa fa-warning'}, {type: 'danger'});
				    };
				    
				});
				
				//상태버튼 클릭 이벤트
				$('input[name=an_state]').on('change', function (e) {
					
					//$('input[name=an_state]:checked');
					
					//반려버튼 클릭시
					if($(this).val() == 3){
						
						$('#panel-refuse-add').removeClass('hidden');
						
						$('#refuse-add').empty();
						$('#tmpl-add-refuse').tmpl().appendTo($('#refuse-add'));
						
					}else{
						
						$('#panel-refuse-add').addClass('hidden');
						
					}
					
					
				});
				
				//선택승인
				$('#btn-approve').on('click', function (e) {
				
				    var items = _DATA_TABLE.rows({selected: true});
				    var count = items.count();
				
				    if (count > 0) {
				        if (confirm('선택 항목을 승인 완료하시겠습니까?')) {
				            var rows = items.data();
				
				            var data = [];
				            $.each(rows, function (i, row) {
				                data.push(row.an_id);
				            });
				
				            $.ajax({
				                type: 'POST',
				                url: '/answer/approve',
				                data: {
				                    anIds: data
				                },
				                async: true,
				                dataType: 'json',
				                beforeSend: function (xhr) {
				                    $.blockUI();
				                },
				                success: function (data) {
				                    _DATA_TABLE.draw(false);
				                },
				                error: function (jqXHR, textStatus, errorThrown) {
				                },
				                complete: function (e) {
				                    $.unblockUI();
				                }
				            });
				        }
				    } else {
				        $.notify({message: '선택된 답안이 없습니다', icon: 'fa fa-warning'}, {type: 'danger'});
				    }
				});
				
				//tab click event
				$('.nav-tabs a').click(function (e) {
					
				    $(this).tab('show');
				    _DATA_TABLE.draw();
				    
				    var index = $('.nav-tabs .active').index();
				    
				    //세션 관리자 티입
				    var type = ${currentUser.getUser().getAdmin_type()};
				    
				    //change option
				    $.fnChangeOption(index, $("#date-type option"), type);
					
				    
				});
				
				//테이블 전체 선택
				$('#btn-checkbox-all').on('click', function (e) {
				    e.preventDefault();
				
				    if ($.type(_DATA_TABLE) === 'undefined') {
				        //option notify
				    } else {
				        if ($(this).hasClass('active')) {
				            $(this).removeClass('active');
				            _DATA_TABLE.rows().deselect();
				        } else {
				            $(this).addClass('active');
				            _DATA_TABLE.rows().select();
				        }
				    }
				});
			        
				//엑셀 다운로드
				$('#btn-excel-answer').on('click', function (e) {
				
					var page;
                    var size;
                    if ($.type(_DATA_TABLE) === 'undefined') {
                        page = 0;
                        size = 10;
                    } else {
                        page = _DATA_TABLE.page.info().page;
                        size = _DATA_TABLE.page.info().length;
                    }
                    
				    $.fileDownload(
				    		"/answer/excel/download"
				    		+"?page="+ page 
				    		+ "&size=" + size
				        	+ "&dateType=" + $('#date-type').val() 
				        	+ "&searchStart=" + $('#search-start').val()
				        	+ "&searchEnd=" + $('#search-end').val()
				        	+ "&studentType=" + $('#student-type').val()
				        	+ "&gradeType=" + $('#grade-type').val()
				        	+ "&searchType=" + $('#search-type').val()
				        	+ "&searchWord=" + $('#search-word').val()
				        	+ "&state=" + $('.nav-tabs .active').index()
				        )	
				        .done(function (e) {
				            $.notify({message: "다운로드가 완료되었습니다", icon: 'fa fa-info-circle'}, {type: 'success'});
				        })
				        .fail(function (jqXHR, textStatus, errorThrown) {
				            $.notify({message: jqXHR.responseText, icon: 'fa fa-warning'}, {type: 'danger'});
				        });
				    		
				    return false; //this is critical to stop the click event which will trigger a normal file download!
				});
			
				$('#student-type').on('click', function (e) {
				
				    var selectedValue = $("#student-type option:selected").val();
				    $('#grade-type').empty();
				    if (selectedValue == 10) {
				        $('#grade-type').append("<option value='0'>전체</option>");
				        $('#grade-type').append("<option value='1'>1학년</option>");
				        $('#grade-type').append("<option value='2'>2학년</option>");
				        $('#grade-type').append("<option value='3'>3학년</option>");
				        $('#grade-type').append("<option value='4'>4학년</option>");
				        $('#grade-type').append("<option value='5'>5학년</option>");
				        $('#grade-type').append("<option value='6'>6학년</option>");
				        $('#grade-type').append("<option value='7'>7학년</option>");
				    } else {
				        $('#grade-type').append("<option value='0'>전체</option>");
				        $('#grade-type').append("<option value='1'>1학년</option>");
				        $('#grade-type').append("<option value='2'>2학년</option>");
				        $('#grade-type').append("<option value='3'>3학년</option>");
				    }
				});
				
				//btn-save 첨삭상태 저장
				$('#btn-save').on('click', function(e){
					e.preventDefault();
					
					//반려사유
					var comment;
					
					//상태코드가 반려인경우 반려사유를 입력해야 한다.
					if($('.nav-tabs .active').index() == 2){
						if($('input[name=an_state]:checked').val() == 3) {
							comment = $('#refuse-add-comment').val();
							if($.isBlank(comment)) {
								alert('반려 사유를 입력해주세요'); 
								return;
							}
						}
					}
					
					if(confirm('저장하시겠습니까?')) {
						
						$.ajax({
							type : 'POST',
							url : '/answer/save',
							data: {
								an_id : $('#an_id').data("value")
								, st_id : $('#st_id').val()
								, an_state : $('input[name=an_state]:checked').val()
								, comment : comment
								, tab : $('.nav-tabs .active').index()
							},
							async : true,
							dataType : 'json',
							beforeSend : function(xhr) {
								$.blockUI();
							},
							success : function(data) {
								//$.redirect("/answer");
								$.notify({message: "저장되었습니다",icon: 'fa fa-info-circle'},{type: 'success'});
								$('#modal-detail').modal('hide');
								_DATA_TABLE.draw(false);
							},
							error : function(jqXHR, textStatus, errorThrown) {
								$.notify({message: jqXHR.responseJSON.message,icon: 'fa fa-warning'},{type: 'danger'});
								$('#modal-detail').modal('hide');
							},
							complete : function(e) {
								$.unblockUI();
							}
						});
						//.ajax
					};
				});
			});
			
			$.fnTeachers = function () {
			    $.ajax({
			        type: 'POST',
			        url: '/teacher/teachers',
			        data: {},
			        async: true,
			        dataType: 'json',
			        beforeSend: function (xhr) {},
			        success: function (data) {
	
			        	$('#teacher-type').empty();
	
			        	$("#tmpl-teachers")
			                .tmpl(data)
			                .appendTo($('#teacher-type'));
			
			            //setting teacher
			            $('#teacher-type').val();
			        },
			        error: function (jqXHR, textStatus, errorThrown) {},
			        complete: function (e) {}
			    });
			};
			
			//Refuse Remove
			$.fnRefuseRemove = function(seq) {
				
				var idx = '#comment-set-'+seq;
				
				if(confirm('반려 사유를 삭제하시겠습니까?')) {
					$.ajax({
						type : 'POST',
						url : '/answer/refuse/remove',
						data : {
							seq : seq
							, anId : $('#an_id').data("value")
						},
						async : true,
						dataType : 'json',
						beforeSend : function(xhr) {},
						success : function(data) {
							
							$(idx).fadeOut("slow", function() {
							    // Animation complete.
								$(idx).remove();
							  });
						},
						error : function(jqXHR, textStatus, errorThrown) {},
						complete : function(e) {}
					});
				}
				
				
			};
			
			//Refuse Save
			$.fnRefuseModify = function(seq) {
				
				var idx = '#comment-'+seq;
				var comment = $(idx).val();
				
				if($.isBlank(comment)) {
					alert('반려 사유를 입력해주세요'); 
					return;
				}
				
				if(confirm('반려 사유를 수정하시겠습니까?')) {
					
					$.ajax({
						type : 'POST',
						url : '/answer/refuse/modify',
						data : {
							seq : seq
							, comment : comment
							, anId : $('#an_id').data("value")
						},
						async : true,
						dataType : 'json',
						beforeSend : function(xhr) {},
						success : function(data) {
							
							//$('#modal-detail').modal('hide');
							$.notify({message: "반려 사유가 수정되었습니다.",icon: 'fa fa-info-circle'},{type: 'success'});
							
							
						},
						error : function(jqXHR, textStatus, errorThrown) {},
						complete : function(e) {}
					});
				}
				
			};
			
			//GET Teachers
			$.fnDtailTeachers = function(stId) {
				
				$.ajax({
					type : 'POST',
					url : '/teacher/teachers',
					data : {},
					async : true,
					dataType : 'json',
					beforeSend : function(xhr) {},
					success : function(data) {

						$('#st_id').empty();

						$("#tmpl-teachers")
						.tmpl(data)
						.appendTo($('#st_id'));
						
						//setting teacher
						$('#st_id').val(stId);
						
					},
					error : function(jqXHR, textStatus, errorThrown) {},
					complete : function(e) {}
				});
			};
			
			//modal detail
			$('#modal-detail').on("show.bs.modal", function(e) {
				
				//$(this).off("click");
				
				//call modal target
				var rt = $(e.relatedTarget);
				var anId = rt.data("id");
				
				$.ajax({
					type: 'POST',
					url: '/answer/detail',
					data: {
						anId : anId
					},
					async: true,
					dataType: 'json',
					beforeSend: function (xhr) {},
					success: function (data) {
					
						$("#an_id").text(data.note.an_id);
						$("#an_id").data('value', data.note.an_id);
						$("#student_nm").text(data.note.student_nm);
						$("#noteImage").attr('src', data.path);
						
						//GET TEACHERS
						$.fnDtailTeachers(data.note.st_id);
						
						//SET PAGE-SETTING
						$('input[name=an_state][value='+data.note.an_state+']').prop('checked', true);
						$.fnNoteStateHandler(parseInt(data.note.an_state), data.refuse);
						
					},
					error: function (jqXHR, textStatus, errorThrown) {},
					complete: function (e) {}
				});
			});
			
			//modal logs
			$('#modal-logs').on("show.bs.modal", function(e) {
				
				//call modal target
				var rt = $(e.relatedTarget); 
				
				$.ajax({
					type : 'POST',
					url : '/answer/logs',
					data: {
						anId : $('#an_id').data("value")
					},
					async : true,
					dataType : 'json',
					success : function(data) {
						
						$('#tb-logs').empty();
						if(data.length > 0){
							$('#tmpl-logs')
							.tmpl(data)
							.appendTo($('#tb-logs'));
						}else{
							$('#tmpl-nodata')
							.tmpl()
							.appendTo($('#tb-logs'));
						}
					}
				});
				//.ajax
			});
			
		</script>
	
		<script id="tmpl-teachers" type="text/x-jquery-tmpl">
			<option value="\${st_id}">\${st_nm}</option>
		</script>
		
		<script id="tmpl-logs" type="text/x-jquery-tmpl">
			<tr>
				<td>
					\${$.format.date(reg_date, "yyyy-MM-dd HH:mm")}
				</td>
				<td>
					
					{{if writer_id != ''}}
						<span class="badge badge-info">
		    				<span>\${writer_nm}</span>
		    			</span>
					{{/if}}
 
					{{if writer_type == 10 && writer_id != ''}}
	    				<span class="badge">
	    					<span>관리자</span>
	    				</span>
					{{else writer_type == 20}}
	    				<span class="badge">
	    					<span>교사</span>
	    				</span>
					{{/if}}
						
					{{if an_state == 0}}
						{{if rownum == total}}
							<span class="badge">
	    						<span>답안 최초 등록</span>
	    					</span>						
						{{else rownum != total && writer_nm != ''}}
							<span class="badge">
	    						<span>\${writer_nm} 최초 열람</span>
	    					</span>
						{{/if}}
	    				
					{{else an_state == 1}}
	    				<span class="badge badge-success">
	    					<span>첨삭중 상태로 저장</span>
	    				</span>
					{{else an_state == 2}}
	    				<span class="badge badge-warning">
	    					<span>승인요청</span>
	    				</span>
					{{else an_state == 3}}
	
						{{if refuse_state == 10}}
		    				<span class="badge badge-error">
	    						<span>반려 사유 등록</span>
	    					</span>
						{{else refuse_state == 20}}
	    					<span class="badge badge-error">
	    						<span>반려 사유 수정</span>
	    					</span>
						{{else refuse_state == 30}}
	    					<span class="badge badge-error">
	    						<span>반려 사유 삭제</span>
	    					</span>
						{{else}}
	    					<span class="badge badge-error">
	    						<span>반려</span>
	    					</span>
						{{/if}}

					{{else an_state == 4}}
	    				<span class="badge badge-gold">
	    					<span>승인완료</span>
	    				</span>
					{{/if}}
				</td>
			</tr>
		</script>
			
		<script id="tmpl-nodata" type="text/x-jquery-tmpl">
			<tr>
				<td colspan=10 class="text-center">데이터가 없습니다</td>
			</tr>
		</script>
		
		<script id="tmpl-refuse-status-2" type="text/x-jquery-tmpl">
			<div id="comment-set-\${seq}">
				<textarea class="form-control" rows="5" id="comment-\${seq}" disabled="disabled">\${comment}</textarea>
				<form class="form-horizontal">
					<div class="form-group">
						<label class="control-label col-sm-6">
							<span class="text-muted pull-left">
								\${admin_nm} \${$.format.date(reg_date, "yyyy-MM-dd HH:mm:ss")}
							</span>
						</label>
						<div class="col-sm-6" style="margin-top: 10px;">
							<!--
							<div class="btn-group pull-right">
								<button type="button" class="btn btn-warning btn-outline btn-xs" onclick="$.fnRefuseRemove(\${seq});">
									<span class="fa fa-stack-exchange"></span> 삭제
								</button>
								<button type="button" class="btn btn-warning btn-outline btn-xs" onclick="$.fnRefuseModify(\${seq});">
									<span class="fa fa-floppy-o"></span> 저장
								</button>
							</div>
							-->
						</div>
					</div>
				</form>
			</div>
		</script>
		
		<script id="tmpl-refuse-status-3" type="text/x-jquery-tmpl">
			<div id="comment-set-\${seq}">
				<textarea class="form-control" rows="5" id="comment-\${seq}">\${comment}</textarea>
				<form class="form-horizontal">
					<div class="form-group">
						<label class="control-label col-sm-6">
							<span class="text-muted pull-left">
								\${admin_nm} \${$.format.date(reg_date, "yyyy-MM-dd HH:mm:ss")}
							</span>
						</label>
						<div class="col-sm-6" style="margin-top: 10px;">
							<div class="btn-group pull-right">
								<button type="button" class="btn btn-warning btn-outline btn-xs" onclick="$.fnRefuseRemove(\${seq});">
									<span class="fa fa-stack-exchange"></span> 삭제
								</button>
								<button type="button" class="btn btn-warning btn-outline btn-xs" onclick="$.fnRefuseModify(\${seq});">
									<span class="fa fa-floppy-o"></span> 저장
								</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</script>
		
		<script id="tmpl-add-refuse" type="text/x-jquery-tmpl">
			<textarea id="refuse-add-comment" class="form-control" rows="5" placeholder="반려 사유를 입력해주세요."></textarea>
			<!--
			<form class="form-horizontal">
				<div class="form-group">
					<label class="control-label col-sm-6"></label>	
					<div class="col-sm-6" style="margin-top: 10px;">
						<div class="btn-group pull-right">
							<button type="button" class="btn btn-warning btn-outline btn-xs" onclick="$.fnRefuseSave(-1);">
								<span class="fa fa-floppy-o"></span> 저장
							</button>
						</div>
					</div>
				</div>
			</form>
			-->
		</script>
	</content>
</body>
</html>