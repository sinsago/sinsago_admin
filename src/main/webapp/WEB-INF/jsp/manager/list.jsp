<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head></head>
<body>

<!-- Page Content -->
<div class="container">
    <div class="panel-group">

        <div class="panel panel-default">
            <div class="panel-heading">
            	<span style="float: left; margin-top: 5px;">
					관리자 리스트
				</span>
				
				<div class="btn-group pull-right">
					<button type="button" class="btn btn-primary" id="btn-excel">
					    <span class="fa fa-file-excel-o"></span> 엑셀 다운로드
					</button>
				</div>
	            
	            <div class="clearfix"></div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped" id="dt-manager" width="100%">
                        <thead>
                        <tr>
                            <th class="dt-center">No.</th>
                            <th>관리자ID</th>
                            <th>구분</th>
                            <th>관리자명</th>
                            <th>등록일</th>
                            <th>담당학년</th>
                            <th>관리교사 수</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        <!-- /.panel 교사 리스트 -->

        <div class="panel panel-default">
            <div class="panel-body">
                <a href="/manager/form" class="btn btn-primary pull-right">
                    <span class="fa fa-user-plus"></span> 신규등록
                </a>
            </div>
        </div>
        <!-- /.panel 버튼 -->
    </div>
</div>
<!-- /.container -->

<content tag="local_script">
    <script>
		$(document).ready(function () {
			var dtManager = $.fnDataTablesManager();
		});

		//엑셀 다운로드
		$('#btn-excel').on('click', function (e) {
		    $.fileDownload("/manager/excel/download")
				.done(function (e) {
					$.notify({message: "다운로드가 완료되었습니다", icon: 'fa fa-info-circle'}, {type: 'success'});
				})
				.fail(function (jqXHR, textStatus, errorThrown) {
					$.notify({message: jqXHR.responseText, icon: 'fa fa-warning'}, {type: 'danger'});
				});
			
			return false; //this is critical to stop the click event which will trigger a normal file download!
		});
    </script>
</content>
</body>
</html>