<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head></head>
<body>

<!-- Page Content -->
<div class="container">
    <div class="panel-group">
        <div class="panel panel-default">
            <div class="panel-heading">관리자 정보</div>
            <div class="panel-body">

                <form class="form-horizontal" role="form">

                    <div class="form-group">
                        <label class="control-label col-sm-1" for="admin_id">관리자ID</label>
                        <div class="col-sm-5">
                            <div class="input-group">
                                <input type="text" class="form-control" id="admin_id" data-validation="false">
                                <span class="input-group-btn">
									<button type="button" class="btn btn-primary" id="btn-validation-id">
										중복체크
									</button>
								</span>
                            </div>
                        </div>

                        <label class="control-label col-sm-2" for="admin_pwd">비밀번호</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="admin_pwd" placeholder="">
                        </div>

                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-1" for="admin_nm">관리자명</label>
                        <div class="col-sm-5">
                            <div class="input-group">
                                <input type="text" class="form-control" id="admin_nm" data-validation="false" required>
                                <span class="input-group-btn">
									<button type="button" class="btn btn-primary" id="btn-validation-name">
										중복체크
									</button>
								</span>
                            </div>
                        </div>

                        <label class="control-label col-sm-2" for="managed_cnt">관리교사수</label>
                        <div class="col-sm-4">
                            <h4 class="text-info">${info.managed_cnt}
                                <small>명</small>
                            </h4>
                        </div>
                    </div>

                    <div class="form-group">
                        
                        <label class="control-label col-sm-1" for="admin_type">구분</label>
                        <div class="col-sm-5">
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-default">
                                    <input type="radio" id="type-20" name="admin_type" value=20>승인관리자
                                </label>
                                <label class="btn btn-default">
                                    <input type="radio" id="type-30" name="admin_type" value=30>검수관리자
                                </label>

                                <label class="btn btn-default">
                                    <input type="radio" id="type-10" name="admin_type" value=10>최고관리자
                                </label>
                            </div>
                        </div>
                        
                        <label class="control-label col-sm-2" for="student-type">학년</label>
                        <div class="col-sm-4">
                        	
                        	<div class="form-group form-inline">
                        		<select class="form-control" id="student-type" style="margin-left: 14px;">
						            <option value="">전체</option>
						            <option value="10">초등</option>
						            <option value="20">중등</option>
						            <option value="30">고등</option>
						        </select>
						        <select class="form-control" id="grade-type">
						            <option value="0">전체</option>
						        </select>
						        
								<button class="btn btn-primary" type="button" id="btn-grade">선택</button>
							</div>
							
							<div id="grade-charge"></div>
							
							
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.panel 관리자 정보 -->

        <div class="panel panel-default">
            <div class="panel-heading">
				관리교사 리스트
            </div>

            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped" id="dt-managed">
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>교사ID</th>
                            <th>교사명</th>
                            <th>구분</th>
                            <th>등록일</th>
                            <th>종료일</th>
                            <th>적용수수료</th>
                            <th>관리담당</th>
                            <th>상태</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        <!-- /.panel 관리교사 리스트 -->

        <div class="panel panel-default">
            <div class="panel-body">
                <!-- <a href="/manager" class="btn btn-primary"> -->
                <a href="javascript: history.back(1);" class="btn btn-primary">
                    <span class="fa fa-table"></span> 목록
                </a>

                <div class="btn-group pull-right">
                    <button type="button" class="btn btn-primary disabled" id="btn-remove">
                        <span class="fa fa-trash"></span> 삭제
                    </button>
                    <button type="button" class="btn btn-primary" id="btn-save">
                        <span class="fa fa-floppy-o"></span> 저장
                    </button>
                </div>
            </div>
        </div>
        <!-- /.panel 버튼 -->
    </div>
</div>
<!-- /.container -->

<content tag="local_script">

    <script>
        var _GRADE = [];
        var isModify = false;

        $(document).ready(function () {
        	
            if (!$.isBlank("${info.admin_id}")) {
            	
                isModify = true;
                
				//GET MANAGERS
				$.fnDataTablesManaged("${info.admin_id}");
				
				//get grade data
				$.fnGetGrade("${info.admin_id}");
				
                $('#admin_id').attr("data-validation", "true");
                $('#admin_nm').attr("data-validation", "true");
                
				//SET PAGE-SETTING
				$('#admin_id').val("${info.admin_id}");
				$('#admin_nm').val("${info.admin_nm}");
				$('#admin_pwd').val("${info.admin_pwd}");
				
            }
            
            var type = "${info.admin_type}";
			$('input:radio[name=admin_type][value=' + type + ']').attr('checked', true);
			$.fnSetManagerType(parseInt(type));

            //id 중복체크
            $('#btn-validation-id').on('click', function (e) {
                e.preventDefault();

                if ($.isBlank($('#admin_id').val())) {
                    $.notify({message: "입력된 값이 없습니다", icon: 'fa fa-warning'}, {type: 'danger'});
                    return;
                }

                $.ajax({
                    type: 'POST',
                    url: '/manager/validation/id',
                    data: {
                        adminId: $('#admin_id').val()
                    },
                    async: true,
                    dataType: 'json',
                    beforeSend: function (xhr) {
                        $.blockUI();
                    },
                    success: function (data) {
                        if (data) {
                            $.notify({message: "사용가능한  ID입니다", icon: 'fa fa-info-circle'}, {type: 'info'});
                            $('#admin_id').attr("data-validation", "true");
                        } else {
                            $.notify({message: "입력하신 ID는 이미 사용중 입니다", icon: 'fa fa-info-circle'}, {type: 'warning'});
                            $('#admin_id').val('').focus();
                            $('#admin_id').empty();
                            $('#admin_id').attr("data-validation", "false");
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $.notify({message: jqXHR.responseJSON.message, icon: 'fa fa-warning'}, {type: 'danger'});
                    },
                    complete: function (e) {
                        $.unblockUI();
                    }
                });
                //.ajax
            });

            //id 중복체크
            $('#btn-validation-name').on('click', function (e) {
                e.preventDefault();

                if ($.isBlank($('#admin_nm').val())) {
                    $.notify({message: "입력된 값이 없습니다", icon: 'fa fa-warning'}, {type: 'danger'});
                    return;
                }

                $.ajax({
                    type: 'POST',
                    url: '/manager/validation/name',
                    data: {
                        adminName: $('#admin_nm').val()
                    },
                    async: true,
                    dataType: 'json',
                    beforeSend: function (xhr) {
                        $.blockUI();
                    },
                    success: function (data) {
                        if (data) {
                            $.notify({message: "사용가능한  이름입니다", icon: 'fa fa-info-circle'}, {type: 'info'});
                            $('#admin_nm').attr("data-validation", "true");
                        } else {
                            $.notify({message: "입력하신 이름은 이미 사용중 입니다", icon: 'fa fa-info-circle'}, {type: 'warning'});
                            $('#admin_nm').val('').focus();
                            $('#admin_nm').empty();
                            $('#admin_nm').attr("data-validation", "false");
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $.notify({message: jqXHR.responseJSON.message, icon: 'fa fa-warning'}, {type: 'danger'});
                    },
                    complete: function (e) {
                        $.unblockUI();
                    }
                });
                //.ajax
            });

            //관리자 정보 저장
            $('#btn-save').on('click', function (e) {
                e.preventDefault();

                //validation
                if ($.isBlank($('#admin_id').val()) ||
                    $.isBlank($('#admin_nm').val()) ||
                    $.isBlank($('#admin_pwd').val())) {

                    $.notify({message: "필수정보가 누락되었습니다", icon: 'fa fa-info-circle'}, {type: 'danger'});

                    return;
                }

                if ($('#admin_id').attr('data-validation') != "true") {
                    $.notify({message: "관리자ID 중복검사가 누락되었습니다", icon: 'fa fa-info-circle'}, {type: 'danger'});
                    return;
                }

                if ($('#admin_nm').attr('data-validation') != "true") {
                    $.notify({message: "관리자명 중복검사가 누락되었습니다", icon: 'fa fa-info-circle'}, {type: 'danger'});
                    return;
                }

                if (confirm('관리자 정보를 저장하시겠습니까?')) {
                    $.ajax({
                        type: 'POST',
                        url: '/manager/save',
                        data: {
                            isModify: isModify
                            , admin_id: $('#admin_id').val()
                            , admin_nm: $('#admin_nm').val()
                            , admin_pwd: $('#admin_pwd').val()
                            , admin_type: $('input[name=admin_type]:checked').val()
                            , charge : JSON.stringify(_GRADE)
                        },
                        async: true,
                        dataType: 'json',
                        beforeSend: function (xhr) {
                            $.blockUI();
                        },
                        success: function (data) {
                            $.redirect("/manager");
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            $.notify({message: jqXHR.responseJSON.message, icon: 'fa fa-warning'}, {type: 'danger'});
                        },
                        complete: function (e) {
                            $.unblockUI();
                        }
                    });
                    //.ajax
                };
            });

            //관리자 삭제
            $('#btn-remove').on('click', function (e) {
                e.preventDefault();

                if ($('#btn-remove').hasClass('disabled')) {
                    $.notify({
                        message: "<br/>관리교사가 있거나 미등록 관리자인 경우 삭제 불가합니다",
                        icon: 'fa fa-info-circle fa-2x'
                    }, {type: 'danger'});
                } else {
                    if (confirm('관리자를 삭제 하시겠습니까?')) {
                        $.ajax({
                            type: 'POST',
                            url: '/manager/remove',
                            data: {
                                managerId: $('#admin_id').val()
                            },
                            async: true,
                            dataType: 'json',
                            beforeSend: function (xhr) {
                                $.blockUI();
                            },
                            success: function (data) {
                                $.redirect("/manager");
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                $.notify({
                                    message: jqXHR.responseJSON.message,
                                    icon: 'fa fa-warning'
                                }, {type: 'danger'});
                            },
                            complete: function (e) {
                                $.unblockUI();
                            }
                        });
                        //.ajax
                    };
                }
            });

            //keyup validation reset(false)
            $('#admin_id, #admin_nm').on('keyup', function (e) {
                e.preventDefault();
                $(this).attr("data-validation", "false");

            });
            
			$('#btn-grade').on('click', function (e) {
				_GRADE = $.fnCheckStudentGrade(_GRADE, $("#student-type").val(), $("#grade-type").val());
				$.fnDrawStudent($('#grade-charge'), _GRADE);
			});
            
            $('#student-type').on('change', function (e) {
				
			    var selectedValue = $("#student-type option:selected").val();
			    $('#grade-type').empty();

			    if (selectedValue == '') {
			        $('#grade-type').append("<option value='0'>전체</option>");
			    } else if (selectedValue == 10) {
			        $('#grade-type').append("<option value='0'>전체</option>");
			        $('#grade-type').append("<option value='1'>1학년</option>");
			        $('#grade-type').append("<option value='2'>2학년</option>");
			        $('#grade-type').append("<option value='3'>3학년</option>");
			        $('#grade-type').append("<option value='4'>4학년</option>");
			        $('#grade-type').append("<option value='5'>5학년</option>");
			        $('#grade-type').append("<option value='6'>6학년</option>");
			    } else {
			        $('#grade-type').append("<option value='0'>전체</option>");
			        $('#grade-type').append("<option value='1'>1학년</option>");
			        $('#grade-type').append("<option value='2'>2학년</option>");
			        $('#grade-type').append("<option value='3'>3학년</option>");
			    }
			});
        });
        //. document ready
    </script>
    <!-- /.script -->

</content>
<!-- /.content-->
</body>
</html>