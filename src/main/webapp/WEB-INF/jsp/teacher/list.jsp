<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
	<head></head>
	<body>
	
		<!-- Page Content -->
		<div class="container">
    		<div class="panel-group">
				<div class="panel panel-default">
					<div class="panel-heading">교사 검색</div>
					<div class="panel-body">
						<form class="form-inline">
							<div class="form-group">
								<select class="form-control" id="search-type">
									<option value="10">전체</option>
									<option value="20">아이디</option>
									<option value="30">교사명</option>
									<option value="40">관리담당자</option>
								</select>
								
								<input type="text" class="form-control" id="search-word">
								
								<button type="button" class="btn btn-primary" id="btn-search">
									<span class="fa fa-search"></span> 검색
								</button>
							</div>
						</form>
					</div>
				</div>
				<!-- /.panel 교사검색 -->
		
				<div class="panel panel-default">
					<div class="panel-heading">
						<span style="float: left; margin-top: 5px;">
							교사 리스트
						</span>
						
						<div class="btn-group pull-right">
							<button type="button" class="btn btn-primary" id="btn-excel">
							    <span class="fa fa-file-excel-o"></span> 엑셀 다운로드
							</button>
						</div>
			            
			            <div class="clearfix"></div>
					</div>
					
					<div class="panel-body">
				
						<div class="table-responsive">
							<table class="table table-bordered table-hover table-striped" id="dt-teacher" width="100%">
								<thead>
									<tr>
										<th class="dt-center">No.</th>
										<th>교사ID</th>
										<th>교사명</th>
										<th>구분</th>
										<th>등록일</th>
										<th>종료일</th>
										<th>적용 수수료</th>
										<th>관리담당자</th>
										<th>상태</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
				<!-- /.panel 교사 리스트 -->
			
				<div class="panel panel-default">
					<div class="panel-body">
						<a href="/teacher/form" class="btn btn-primary pull-right">
							<span class="fa fa-user-plus"></span> 신규등록
						</a>
					</div>
				</div>
				<!-- /.panel 버튼 -->
  			</div>
    	</div>
	    <!-- /.container -->
		
		<content tag="local_script">
			<script>
				$(document).ready(function() {
					var _DATA_TABLE;
					_DATA_TABLE = $.fnDataTablesTeacher();
					
					$('input:text').keydown(function(e) {
						if (e.keyCode == 13) {
							_DATA_TABLE.draw();
							return false;    //<---- Add this line
						}
					});
					
					$('#btn-search').on('click', function(e){
						_DATA_TABLE.draw();
					});
					
					//엑셀 다운로드
					$('#btn-excel').on('click', function (e) {
					    $.fileDownload(
					    		"/teacher/excel/download"
					    		+"?searchType=" + $('#search-type :checked').val() 
					        	+ "&searchWord=" + $('#search-word').val()
					        )	
					        .done(function (e) {
					            $.notify({message: "다운로드가 완료되었습니다", icon: 'fa fa-info-circle'}, {type: 'success'});
					        })
					        .fail(function (jqXHR, textStatus, errorThrown) {
					            $.notify({message: jqXHR.responseText, icon: 'fa fa-warning'}, {type: 'danger'});
					        });
					    		
					    return false; //this is critical to stop the click event which will trigger a normal file download!
					});
					
				});
			</script>
		</content>
	</body>
</html>