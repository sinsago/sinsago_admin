<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
	<head></head>
	<body>
	
		<!-- Page Content -->
	    <div class="container">
			<div class="panel-group">
				<div class="panel panel-default">
					<div class="panel-heading">교사 기본 정보</div>
					<div class="panel-body">
						<form class="form-horizontal" data-toggle="validator" role="form">
							<div class="form-group">
								<label class="control-label col-sm-1" for="st_id">교사ID</label>
								<div class="col-sm-5">
									<div class="input-group">
										<input type="text" class="form-control" id="st_id" data-validation="false" required>
										<span class="input-group-btn">
											<button type="button" class="btn btn-primary" id="btn-validation-id">
												중복체크
											</button>
										</span>
									</div>
								</div>
								
								<label class="control-label col-sm-1" for="st_pwd">비밀번호</label>
								<div class="col-sm-5">
									<input type="text" class="form-control" id="st_pwd" placeholder="">
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label col-sm-1" for="st_nm">교사명</label>
								<div class="col-sm-5">
									<div class="input-group">
										<input type="text" class="form-control" id="st_nm" data-validation="false" required>
										<span class="input-group-btn">
											<button type="button" class="btn btn-primary" id="btn-validation-name">
												중복체크
											</button>
										</span>
									</div>
								</div>
								
								<label class="control-label col-sm-1" for="st_state">상태</label>
								<div class="col-sm-5">
									
									<div class="btn-group" data-toggle="buttons">
										<label class="btn btn-default">
											<input type="radio" id="state-0" name="st_state" value=0>근무종료
										</label>
	
										<label class="btn btn-default">
											<input type="radio" id="state-1" name="st_state" value=1>근무중
										</label>
	
										<label class="btn btn-default">
											<input type="radio" id="state-2" name="st_state" value=2>휴직중
										</label>
									</div>
									
									<!--   
									<div class="btn-group" data-toggle="buttons" id="st_state">
										<button type="button" class="btn btn-default" data-value="0">근무종료</button>
										<button type="button" class="btn btn-default" data-value="1">근무중</button>
										<button type="button" class="btn btn-default" data-value="2">휴직중</button>
									</div>
									 -->
								</div>
							</div>
						
							<div class="form-group">
								<label class="control-label col-sm-1" for="default_fee">적용 수수료</label>
								<div class="col-sm-5">
									<h4 class="text-info" id="default_fee" data-value></h4>
								</div>
								
								<label class="control-label col-sm-1" for="mngr_id">관리 담당자</label>
								<div class="col-sm-5">
									<select class="form-control" id="mngr_id" name="mngr_id"></select>
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label col-sm-1" for="st_rrn1">주민번호</label>
								<div class="col-sm-5">
									<div class="form-inline">
										<input type="text" class="form-control width-100" id="st_rrn1" maxlength="6">
										 -  
										<input type="text" class="form-control width-100" id="st_rrn2" maxlength="7">
									</div>
								</div>
								
								<label class="control-label col-sm-1" for="st_tel">전화번호</label>
								<div class="col-sm-5">
									<input type="text" class="form-control" id="st_tel" placeholder="(ex)01012345678">
								</div>
							</div>
							
							<div class="form-group">
							
								<label class="control-label col-sm-1"></label>
								<div class="col-sm-5"></div>
								
								<label class="control-label col-sm-1" for="st_type">구분</label>
								<div class="col-sm-5">
									<div class="btn-group" data-toggle="buttons">
										<label class="btn btn-default">
											<input type="radio" id="type-10" name="st_type" value=10>교사
										</label>
	
										<label class="btn btn-default">
											<input type="radio" id="type-20" name="st_type" value=20>아르바이트
										</label>
	
										<label class="btn btn-default">
											<input type="radio" id="type-30" name="st_type" value=30>내부직원
										</label>
									</div>
								</div>
							</div>
							
							
						</form>
					</div>
				</div>
				<!-- /.panel 교사 기본 정보 -->
				
				<div class="panel panel-default">
					<div class="panel-heading">
						첨삭 통계
					</div>
					
					<div class="panel-body">
						<form class="form-inline">
							<div class="form-group">
								<div class='input-group date' id='datetimepicker-start'>
									<input type='text' class="form-control" id="startDate" />
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
							
								<span class="label label-default">~</span>
								
								<div class='input-group date' id='datetimepicker-end'>
									<input type='text' class="form-control" id="endDate" />
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
							
								<button type="button" class="btn btn-primary" id="btn-search">
									<span class="fa fa-search"></span> 조회
								</button>
							</div>
						</form>
					</div>
					
					<div class="panel-body">	
						<div class="table-responsive">
							<table class="table table-bordered table-hover table-striped">
								<thead>
									<tr>
										<th rowspan="2">승인요청 건수(A)</th>
										<th colspan="3">승인완료 건수</th>
										<th rowspan="2">반려 건수(E)</th>
										<th rowspan="2">일정 준수율</th>
										<th rowspan="2">승인 완료율</th>
									</tr>
									<tr>
										<th>일정 준수(B)</th>
										<th>일정 미준수(C)</th>
										<th>계(D)</th>
									</tr>
								</thead>
								<tbody id="tb-note-statistic"></tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- /.panel 첨삭 통계 -->
						
				<div class="panel panel-default">
					<div class="panel-heading">
						적용 수수료
					</div>
					
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-bordered table-hover table-striped">
								<thead>
									<tr>
										<th>기본(최소) 수수료</th>
										<th>근속기간 수수료</th>
										<th>평가결과 수수료</th>
										<th>적용 수수료</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>
											<h4 class="text-info" id="tb_default_fee" data-value></h4>
										</td>
										<td>
											<button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#modal-period">
												<span class="fa fa-stack-exchange"></span> 이력
											</button>
											<h4 class="text-info" id="tb_period_fee" data-value></h4>
										</td>
										<td>
											<button type="button" class="btn btn-primary pull-right" style="margin-left: 5px;" data-toggle="modal" data-target="#modal-added">
												<span class="fa fa-user"></span> 관리자 입력
											</button>
											<button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#modal-test">
												<span class="fa fa-stack-exchange"></span> 이력
											</button>
											<h4 class="text-info" id="tb_test_fee" data-value></h4>
										</td>
										<td>
											<h4 class="text-info" id="tb_sum_fee" data-value></h4>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
	        		</div>
				</div>
				<!-- /.panel 적용 수수료 -->
				
				<div class="panel panel-default">
					<div class="panel-body">
						<!-- <a href="/teacher" class="btn btn-primary"> -->
						<a href="javascript: history.back(1);" class="btn btn-primary">
							<span class="fa fa-table"></span> 목록
						</a>
						<button type="button" class="btn btn-primary pull-right" id="btn-save">
							<span class="fa fa-floppy-o"></span> 저장
						</button>
					</div>
				</div>
				<!-- /.panel 버튼 -->
			</div>
		</div>
		<!-- /.container -->

		<div id="modal-period" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">근속기간 수수료 이력</h4>
					</div>
					<div class="modal-body">
						<div class="table-responsive">
							<table class="table table-bordered table-hover table-striped">
								<thead>
									<tr>
										<th>기준일</th>
										<th>수수료</th>
										<th>적용월</th>
									</tr>
								</thead>
								<tbody id="tb-period"></tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.modal -->
		
		<!-- 평가결과 수수료 이력 -->
		<div id="modal-test" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<div class="modal-body">
						<h4 class="modal-title">평가결과 수수료 이력</h4>
						<div class="table-responsive">
							<table class="table table-bordered table-hover table-striped">
								<thead>
									<tr>
										<th>기준일</th>
										<th>평가 결과 (사유)</th>
										<th>수수료</th>
										<th>적용일자</th>
										<th class="dt-center">관리</th>
									</tr>
								</thead>
								<tbody id="tb-test"></tbody>
							</table>
						</div>
					</div>
					
					<div class="modal-body">
						<h4 class="modal-title">평가 결과 수수료 (금일 기준)</h4>
						<div class="table-responsive">
							<table class="table table-bordered table-hover table-striped">
								<thead>
									<tr>
										<th>기준일</th>
										<th>평가 결과 수수료</th>
									</tr>
								</thead>
								<tbody id="tb-test-todays"></tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.modal -->
		
		<div id="modal-added" class="modal fade" role="dialog">
			<div class="modal-dialog modal-lg">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">평가결과 수수료 관리자 입력</h4>
					</div>
					<div class="modal-body">
						<div>
							<table class="table table-bordered table-hover table-striped">
								<thead>
									<tr>
										<th>기준일</th>
										<th>평가 결과(사유)</th>
										<th>수수료</th>
										<th>적용일자</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>
											<label style="margin-top: 10px;" id="added-based-date"></label>
										</td>
										<td>
											<input type="text" class="form-control" id="added-comment" placeHolder="관리자 변경">
										</td>
										<td>
											<input type="number" pattern="/^[-]?\d*$/g" step="10" value="100" class="form-control pull-left" style="width: 90%" id="added-fee">
											<label for="added-fee" style="margin-top: 15px;">원</label>
										</td>
										<td>
											<div class='input-group date' id='datetimepicker-apply-date'>
												<input type='text' class="form-control" id="added-apply-date"/>
												<span class="input-group-addon">
													<i class="glyphicon glyphicon-calendar"></i>
												</span>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="panel-footer clearfix text-center">
						<button type="button" class="btn btn-primary" id="btn-save-added">
							<i class="glyphicon glyphicon-save"></i> 저장
						</button>
						&nbsp;
						<button type="button" class="btn btn-primary" data-dismiss="modal">
							<i class="glyphicon glyphicon-remove"></i> 취소
						</button>
					</div>
				</div>
			</div>
		</div>
		<!-- /.modal -->
		
		<content tag="local_script">
			
			<script>
				
				var isModify = false;
				var _COMPLATE_MONTH_OF_DAYS;
				var _START_DATE = moment().startOf('month').add(-2, 'month');
			
				$(document).ready(function() {
					
					//GET MANAGERS
					$.fnGetManagers();
					
					//GET Complate Of Closed Month
					_COMPLATE_MONTH_OF_DAYS = $.fnComplateOfClosedMonth(_START_DATE);
					
					//console.log(">>>>>>>>>>>>>>>>>>>>>>");
					//console.log(_COMPLATE_MONTH_OF_DAYS);
					//console.log(">>> isArray? " + $.isArray(_COMPLATE_MONTH_OF_DAYS));
					
					//$.each(_COMPLATE_MONTH_OF_DAYS, function(i, e) {
						//console.log("***********");
						//console.log(moment(e).format("YYYY-MM-DD"));
						//console.log(e);
						//console.log("***********");
					//});
					//console.log(">>>>>>>>>>>>>>>>>>>>>>");
					
					if(!$.isBlank("${tInfo.st_id}")){
						isModify = true;
						$('#st_id').attr("data-validation", "true");
						$('#st_nm').attr("data-validation", "true");
						
						//$('input[name=st_type]').attr("disabled", true);
					}
					
					//SET PAGE-SETTING
					$('#st_id').val("${tInfo.st_id}");
					$('#st_nm').val("${tInfo.st_nm}");
					
					//default_fee 적용수수료
					$('#default_fee').html("${tInfo.tot_fee}"+" <small>원</small>");
					$('#default_fee').attr("data-value", "${tInfo.tot_fee}");
					
					//주민번호
					if(!$.isBlank("${tInfo.st_rrn}")){
						$.fnSplitStRRN("${tInfo.st_rrn}");
					}
					
					//비밀번호
					$('#st_pwd').val("${tInfo.st_pwd}");
					
					//근무상태
					var state = "${tInfo.st_state}";
					$('input:radio[name=st_state][value='+state+']').attr('checked', true);
					$.fnSetActiveState(parseInt(state));
					
					//구분상태
					var type = "${tInfo.st_type}";
					$('input:radio[name=st_type][value='+type+']').attr('checked', true);
					$.fnSetActiveType(parseInt(type));
							
					//전화번호
					$('#st_tel').val("${tInfo.st_tel}");
					
					//TABLE FEE
					$('#tb_default_fee').html("${tInfo.default_fee}"+" <small>원</small>");
					$('#tb_period_fee').html($.defaultIfBlank("${tInfo.period_fee}",0)+" <small>원</small>");
					$('#tb_test_fee').html($.defaultIfBlank("${tInfo.test_fee}",0)+" <small>원</small>");
					
					$('#tb_sum_fee').html("${tInfo.tot_fee}"+" <small>원</small>");
					
					$('#datetimepicker-start').datetimepicker({
						locale: 'ko'
						, format: 'YYYY-MM-DD'
						, defaultDate: moment().add(-6, 'day').format('YYYY-MM-DD')
					});
					
					$('#datetimepicker-end').datetimepicker({
						useCurrent: false //Important! See issue #1075
						, locale: 'ko'
						, format: 'YYYY-MM-DD'
						, defaultDate: moment().format('YYYY-MM-DD')
				    });
					
					
					//get note statistic
					$.fnGetNoteStatistic("${tInfo.st_id}", $('#startDate').val(), $('#endDate').val());
					
					
					//check datetimepicker
					$("#datetimepicker-start").on("dp.change", function (e) {
						$('#datetimepicker-end').data("DateTimePicker").minDate(e.date);
					});
					
					$("#datetimepicker-end").on("dp.change", function (e) {
						$('#datetimepicker-start').data("DateTimePicker").maxDate(e.date);
					});
					
					//id 중복체크
					$('#btn-validation-id').on('click', function(e){
						e.preventDefault();
						
						if($.isBlank($('#st_id').val())){
							$.notify({message: "입력된 값이 없습니다",icon: 'fa fa-warning'},{type: 'danger'});
							return;
						}
						
						$.ajax({
							type : 'POST',
							url : '/teacher/validation/id',
							data: {
								stId : $('#st_id').val()
							},
							async : true,
							dataType : 'json',
							beforeSend : function(xhr) {
								$.blockUI();
							},
							success : function(data) {
								if(data){
									$.notify({message: "사용가능한  ID입니다",icon: 'fa fa-info-circle'},{type: 'info'});
									$('#st_id').attr("data-validation", "true");
								}else{
									$.notify({message: "입력하신 ID는 이미 사용중 입니다",icon: 'fa fa-info-circle'},{type: 'warning'});
									$('#st_id').val('').focus();
									$('#st_id').empty();
									$('#st_id').attr("data-validation", "false");
								}
							},
							error : function(jqXHR, textStatus, errorThrown) {
								$.notify({message: jqXHR.responseJSON.message,icon: 'fa fa-warning'},{type: 'danger'});
							},
							complete : function(e) {
								$.unblockUI();
							}
						});
						//.ajax
					});
					
					//id 중복체크
					$('#btn-validation-name').on('click', function(e){
						e.preventDefault();
						
						if($.isBlank($('#st_nm').val())){
							$.notify({message: "입력된 값이 없습니다",icon: 'fa fa-warning'},{type: 'danger'});
							return;
						}
						
						$.ajax({
							type : 'POST',
							url : '/teacher/validation/name',
							data: {
								stName : $('#st_nm').val()
							},
							async : true,
							dataType : 'json',
							beforeSend : function(xhr) {
								$.blockUI();
							},
							success : function(data) {
								if(data){
									$.notify({message: "사용가능한  이름입니다",icon: 'fa fa-info-circle'},{type: 'info'});
									$('#st_nm').attr("data-validation", "true");
								}else{
									$.notify({message: "입력하신 이름은 이미 사용중 입니다",icon: 'fa fa-info-circle'},{type: 'warning'});
									$('#st_nm').val('').focus();
									$('#st_nm').empty();
									$('#st_nm').attr("data-validation", "false");
								}
							},
							error : function(jqXHR, textStatus, errorThrown) {
								$.notify({message: jqXHR.responseJSON.message,icon: 'fa fa-warning'},{type: 'danger'});
							},
							complete : function(e) {
								$.unblockUI();
							}
						});
						//.ajax
					});
					
					//keyup validation reset(false)
					$('#st_id, #st_nm').on('keyup', function(e){
						e.preventDefault();
						$(this).attr("data-validation", "false");
						
					});
					
					//교사정보 저장
					$('#btn-save').on('click', function(e){
						e.preventDefault();
						
						//validation
						if($.isBlank($('#st_id').val()) ||
							$.isBlank($('#st_nm').val()) || 
							$.isBlank($('#st_rrn1').val()) || 
							$.isBlank($('#st_rrn2').val()) || 
							$.isBlank($('#st_pwd').val()) || 
							$.isBlank($('#st_tel').val())) {
							
							$.notify({message: "필수정보가 누락되었습니다",icon: 'fa fa-info-circle'},{type: 'danger'});
							return;
						}
						
						if($('#st_id').attr('data-validation') != "true"){
							$.notify({message: "교사ID 중복검사가 누락되었습니다",icon: 'fa fa-info-circle'},{type: 'danger'});
							return;
						}
						
						if($('#st_nm').attr('data-validation') != "true"){
							$.notify({message: "교사명 중복검사가 누락되었습니다",icon: 'fa fa-info-circle'},{type: 'danger'});
							return;
						}

						if(confirm('교사정보를 저장하시겠습니까?')) {
							
							$.ajax({
								type : 'POST',
								url : '/teacher/save',
								data: {
									isModify : isModify
									, st_id : $('#st_id').val()
									, st_nm : $('#st_nm').val()
									, st_rrn : $.fnConcatStRRN($('#st_rrn1').val(), $('#st_rrn2').val())
									, st_pwd : $('#st_pwd').val()
									, st_state : $('input[name=st_state]:checked').val()
									, st_type : $('input[name=st_type]:checked').val()
									, st_tel : $('#st_tel').val()
									, mngr_id : $('#mngr_id').val()
									, default_fee : $('#default_fee').attr("data-value")
								},
								async : true,
								dataType : 'json',
								beforeSend : function(xhr) {
									$.blockUI();
								},
								success : function(data) {
									$.redirect("/teacher");
								},
								error : function(jqXHR, textStatus, errorThrown) {
									$.notify({message: jqXHR.responseJSON.message,icon: 'fa fa-warning'},{type: 'danger'});
								},
								complete : function(e) {
									$.unblockUI();
								}
							});
							//.ajax
						};
					});
					
					//교사정보 저장
					$('#btn-search').on('click', function(e){
						$.fnGetNoteStatistic("${tInfo.st_id}", $('#startDate').val(), $('#endDate').val());
					});
					
					//교사상태
					$('input[name=st_type]').change('click', function(e){
						
						if($(this).val() == 30){
							
							//default_fee 적용수수료
							$('#default_fee').html("0"+" <small>원</small>");
							$('#default_fee').attr("data-value", 0);
							
							$('#tb_default_fee').html("0 <small>원</small>");
							$('#tb_period_fee').html("0 <small>원</small>");
							$('#tb_test_fee').html("0 <small>원</small>");
							$('#tb_sum_fee').html("0 <small>원</small>");
							
						}else{
							
							//default_fee 적용수수료
							$('#default_fee').html("600"+" <small>원</small>");
							$('#default_fee').attr("data-value", "600");
							
							$('#tb_default_fee').html("600"+" <small>원</small>");
							$('#tb_period_fee').html($.defaultIfBlank("${tInfo.period_fee}",0)+" <small>원</small>");
							$('#tb_test_fee').html($.defaultIfBlank("${tInfo.test_fee}",0)+" <small>원</small>");
							$('#tb_sum_fee').html("${600 + tInfo.period_fee + tInfo.test_fee}"+" <small>원</small>");
							
						}
					});
					
					//평가결과 수수료 관리자 입력
					$('#btn-save-added').on('click', function(e){
						e.preventDefault();
						
						/* 
						added-based-date
						added-comment
						added-fee
						added-apply-date */
						if($.isBlank($('#st_id').val())){
							$.notify({message: "입력된 값이 없습니다",icon: 'fa fa-warning'},{type: 'danger'});
							return;
						}
						if($.isBlank($('#added-fee').val())) {$.fnWarning("수수료가 올바르지 않습니다"); return;}
						if($.isBlank($('#added-apply-date').val())) {$.fnWarning("적용일자를 입력하세요"); return;}
						
						if(confirm('평가결과 수수료를 저장 하시겠습니까?')) {
							
							$.ajax({
								type : 'POST',
								url : '/teacher/fee/added',
								data: {
									st_id : $('#st_id').val()
									, based_date : $('#added-based-date').text()
									, comment : $('#added-comment').val()
									, fee : $('#added-fee').val()
									, apply_date : $('#added-apply-date').val()
									, is_recalculation: moment($('#added-apply-date').val()).diff(moment(), 'days') < 0
									, recalculation_day: moment().diff($('#added-apply-date').val(), 'days')
								},
								async : true,
								dataType : 'json',
								beforeSend : function(xhr) {
									$.blockUI();
								},
								success : function(data) {

									$.notify({message: "저장 되었습니다",icon: 'fa fa-info-circle'},{type: 'success'});
									$('#modal-added').modal('hide');
									
									location.reload();
									
								},
								error : function(jqXHR, textStatus, errorThrown) {
									$.notify({message: jqXHR.responseJSON.message,icon: 'fa fa-warning'},{type: 'danger'});
								},
								complete : function(e) {
									$.unblockUI();
								}
							});
							//.ajax
						};
					});
					
					//modal period
					$('#modal-period').on("show.bs.modal", function(e) {
						
						//call modal target
						var rt = $(e.relatedTarget); 
						
						$.ajax({
							type : 'POST',
							url : '/teacher/fee/period',
							data: {
								stId : $('#st_id').val()
							},
							async : true,
							dataType : 'json',
							beforeSend : function(xhr) {
								$.blockUI(null);
							},
							success : function(data) {
								
								$('#tb-period').empty();
								if(data.length > 0){
									$('#tmpl-period')
									.tmpl(data)
									.appendTo($('#tb-period'));
								}else{
									$('#tmpl-nodata')
									.tmpl()
									.appendTo($('#tb-period'));
								}
							},
							error : function(jqXHR, textStatus, errorThrown) {
								$('#modal-period').modal('hide');
								$.notify({message: jqXHR.responseJSON.message,icon: 'fa fa-warning'},{type: 'danger'});
							},
							complete : function(e) {
								$.unblockUI();
							}
						});
						//.ajax
					});
				
					//modal test
					$('#modal-test').on("show.bs.modal", function(e) {
						//call modal target
						var rt = $(e.relatedTarget); 
						
						$.ajax({
							type : 'POST',
							url : '/teacher/fee/test',
							data: {
								stId : $('#st_id').val()
							},
							async : true,
							dataType : 'json',
							beforeSend : function(xhr) {
								$.blockUI(null);
							},
							success : function(data) {
								
								$('#tb-test').empty();
								$('#tb-test-todays').empty();
								
								if(data.recordOfTest.length > 0){
									
									$('#tmpl-test')
									.tmpl(data.recordOfTest)
									.appendTo($('#tb-test'));
									
									$('#tmpl-test-todays')
									.tmpl(data.feeOfTodays)
									.appendTo($('#tb-test-todays'));
									
								}else{
									
									$('#tmpl-nodata')
									.tmpl()
									.appendTo($('#tb-test'));
									
									$('#tmpl-nodata')
									.tmpl()
									.appendTo($('#tb-test-todays'));
								}
							},
							error : function(jqXHR, textStatus, errorThrown) {
								$('#modal-test').modal('hide');
								$.notify({message: jqXHR.responseJSON.message,icon: 'fa fa-warning'},{type: 'danger'});
							},
							complete : function(e) {
								$.unblockUI();
							}
						});
						//.ajax
					});
					
					//modal added
					$('#modal-added').on("show.bs.modal", function(e) {
						
						//call modal target
						var rt = $(e.relatedTarget);
						
						//added-based-date
						$('#added-based-date').text(moment().format('YYYY-MM-DD'));
						
						//added-apply-date
						$('#datetimepicker-apply-date').datetimepicker({
						    useCurrent: false //Important! See issue #1075
						    , locale: 'ko'
						    , format: 'YYYY-MM-DD'
						    , widgetPositioning: {
								horizontal: 'auto'
								, vertical: 'auto'
						    }
							, defaultDate: moment().format('YYYY-MM-DD')
							//, minDate: moment().startOf('year')
							, minDate: _START_DATE
							, disabledDates: _COMPLATE_MONTH_OF_DAYS
						});
						
					});
				});
				//. document ready
				
				//GET managers
				$.fnGetManagers = function() {
					
					$.ajax({
						type : 'POST',
						url : '/teacher/managers',
						data : {},
						async : true,
						dataType : 'json',
						beforeSend : function(xhr) {},
						success : function(data) {

							$('#mngr_id').empty();

							$("#tmpl-mngr")
							.tmpl(data)
							.appendTo($('#mngr_id'));
							
							//set manager
							if(!$.isBlank('${tInfo.mngr_id}')){
								$('#mngr_id').val("${tInfo.mngr_id}");
							}
							
						},
						error : function(jqXHR, textStatus, errorThrown) {},
						complete : function(e) {}
					});
				};
				
				//remove added fee 
				$.fnAddedFeeRemove = function(seq) {
					if(confirm('삭제 하시겠습니까?')) {
						
						$.ajax({
							type : 'POST',
							url : '/teacher/fee/added/remove',
							data : {
								seq: seq
								, is_use: 'N'
							},
							async : true,
							dataType : 'json',
							beforeSend : function(xhr) {},
							success : function(data) {

								$.notify({message: "삭제 되었습니다",icon: 'fa fa-info-circle'},{type: 'success'});
								$('#modal-added').modal('hide');
								
								location.reload();
								
							},
							error : function(jqXHR, textStatus, errorThrown) {},
							complete : function(e) {}
						});
						
					}
					
				};
			</script>
			
			<script id="tmpl-mngr" type="text/x-jquery-tmpl">
				<option value="\${admin_id}">\${admin_nm}</option>
			</script>
			
			<script id="tmpl-period" type="text/x-jquery-tmpl">
				<tr>
					<td>
						\${$.format.date(period_date, "yyyy-MM-dd")}
					</td>
					<td>
						<h4 class="text-info">\${period_fee} <small>원</small></h4>
					</td>
					<td>
						\${$.format.date(apply_date, "yyyy-MM")}
					</td>
				</tr>
			</script>
			
			<script id="tmpl-test" type="text/x-jquery-tmpl">
				<tr>
					<td>
						\${$.format.date(test_date, "yyyy-MM-dd")}
					</td>
					<td>
						\${test_rating}
					</td>
					<td>
						<h4 class="text-info pull-right">
							{{if test_fee > 0}}+{{/if}}\${test_fee} <small>원</small>
						</h4>
					</td>
					<td>
						\${$.format.date(apply_date, "yyyy-MM-dd")}
					</td>
					<td class="dt-center">
						{{if is_removed == 'Y'}}
							<button type="button" class="btn btn-warning btn-outline btn-xs" onclick="$.fnAddedFeeRemove(\${test_seq});">
								<span class="fa fa-trash"></span> 삭제
							</button>
						{{/if}}
					</td>
				</tr>
			</script>
			
			<script id="tmpl-test-todays" type="text/x-jquery-tmpl">
				<tr>
					<td>
						\${$.format.date(based_date, "yyyy-MM-dd")}
					</td>
					<td>
						<h4 class="text-info pull-right">
							{{if fee > 0}}+{{/if}}\${fee} <small>원</small>
						</h4>
					</td>
				</tr>
			</script>
			
			<script id="tmpl-note-statistic" type="text/x-jquery-tmpl">
				<tr>
					<td>\${count_a}</td>
					<td>\${count_b}</td>
					<td>\${count_c}</td>
					<td>\${count_d}</td>
					<td>\${count_e}</td>
					<td>\${keep_rate + '%' }</td>
					<td>\${approve_rate + '%' }</td>
				</tr>
			</script>
		</content>
		<!-- /.script -->
	
	</body>
</html>