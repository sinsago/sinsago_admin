<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
	<head></head>
	<body>
	
		<!-- Page Content -->
	    <div class="container">
	
	        <div class="panel-group">
	        
	        	<div class="panel panel-default">
	        		<div class="panel-heading">APP관리</div>
					<div class="panel-body">
						<!-- 
						<form id="upload-file-form">
							<div class="row">
								<div class="col-sm-9">
									<input type="text" class="form-control" id="card-image-name" readonly="readonly">
								</div>
								<input 
									type="file" 
									class="filestyle" 
									id="card-image" 
									name="card-image" 
									accept="image/png"
									data-buttonName="btn-primary"
									data-buttonText="선택"
									data-iconName="glyphicon glyphicon-inbox"
									data-input="false"
									/>
							</div>
						</form>
						 -->
						<form id="upload-form"> 
							<div class="form-group form-inline">
							
								<input type="text" class="form-control" id="app_version" placeholder="(버전 정보를 입력하세요)">
								<input type="file" class="form-control" id="app_path" accept=".apk">
								
								<button type="button" class="btn btn-primary" id="btn-save">
									<span class="fa fa-cloud-upload"></span> 등록
								</button>
							</div>
						</form>
					</div>
				</div>
				<!-- /.panel 버튼 -->
		
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-bordered table-hover table-striped" id="dt-app" width="100%">
								<thead>
									<tr>
										<th class="dt-center">No.</th>
										<th>버전</th>
										<th>등록일</th>
										<th>파일</th>
										<th>삭제</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
				<!-- /.panel APP 리스트 -->
  			</div>
	
	    </div>
	    <!-- /.container -->
		
		<content tag="local_script">
		
			<script>
				$(document).ready(function() {
					
					//GET APP List
					var dtApps = $.fnDataTablesApp();
					
					$('#btn-save').on('click', function(e){
						e.preventDefault();
						
						if($.isBlank($('#app_version').val()) || $.isBlank($('#app_path').val())){
							$.notify({message: "버전 정보 입력 또는 파일을 선택하세요",icon: 'fa fa-warning'},{type: 'danger'});
							return;
						}
						
						var formData = new FormData();
						formData.append("app_version", $("#app_version").val());
						formData.append("app_path", $("input[id=app_path]")[0].files[0]);
						
						$.ajax({
							type : 'POST',
							url : '/app/save',
							data: formData,
							enctype: "multipart/form-data",
							cache: false,
					        contentType: false,
					        processData: false,
							beforeSend : function(xhr) {
								$.blockUI();
							},
							success : function(data) {
								$.notify({message: "APP 등록이 완료되었습니다",icon: 'fa fa-info-circle'},{type: 'info'});
								//data tables draw
								dtApps.draw();
							},
							error : function(jqXHR, textStatus, errorThrown) {
								$.notify({message: jqXHR.responseJSON.message,icon: 'fa fa-warning'},{type: 'danger'});
							},
							complete : function(e) {
								$.unblockUI();
							}
						});
						//.ajax
					});
					//.btn-save
					
					$.fnDeleteApp = function(seq) {
						
						
						if(confirm('APP을 삭제 하시겠습니까?')) {
							
							$.ajax({
								type : 'POST',
								url : '/app/remove',
								data: {
									seq : seq
								},
								async : true,
								dataType : 'json',
								beforeSend : function(xhr) {
									$.blockUI();
								},
								success : function(data) {
									$.notify({message: "APP이 삭제되었습니다.",icon: 'fa fa-info-circle'},{type: 'info'});
									
									//data tables draw
									dtApps.draw();
								},
								error : function(jqXHR, textStatus, errorThrown) {
									$.notify({message: jqXHR.responseJSON.message,icon: 'fa fa-warning'},{type: 'danger'});
								},
								complete : function(e) {
									$.unblockUI();
								}
							});
							//.ajax
						};
					};
					
				});
			</script>
		</content>
	</body>
</html>