<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head></head>
<body>
	<!-- Page Content -->
	<div class="container">
        <div class="panel panel-info">
            <div class="panel-heading">첨삭 검색</div>
            <div class="panel-body">
                <form class="form-horizontal" role="form">
                
                    <!-- 기간 -->
                    <!-- 
                    <div class="form-group form-inline">
                    	<label class="control-label col-sm-1">구분</label>
                        <div class="col-sm-*">
                        	<select class="form-control" id="date-type">
                                <option value="10">첨삭요청일</option>
                                <option value="20" class="hidden">첨삭목표일</option>
                                <option value="30">승인요청일</option>
                                <option value="40" class="hidden">승인완료일</option>
                            </select>
                        </div>
                    </div>
                    -->
                    
                    <div class="form-group form-inline">
                        <label class="control-label col-sm-1">기간</label>
                        
                        <div class="col-sm-*">
                        	<select class="form-control" id="date-type">
                                <option value="10">첨삭요청일</option>
                                <option value="20" class="hidden">첨삭목표일</option>
                                <option value="30">승인요청일</option>
                                <option value="40">승인완료일</option>
                            </select>
                            
							<select class="form-control" id="search-term">
						        <option value="1">1일</option>
						        <option value="7">7일</option>
						        <option value="30" selected="selected">한달</option>
						    </select>
						                                
                            <div class='input-group date' id='datetimepicker-start'>
								<input type='text' class="form-control" id="search-start"/>
								<span class="input-group-addon">
									<i class="glyphicon glyphicon-calendar"></i>
								</span>
                            </div>
							
                            <div class='input-group date' id='datetimepicker-end'>
                                <input type='text' class="form-control" id="search-end"/>
                                <span class="input-group-addon">
                                	<i class="glyphicon glyphicon-calendar"></i>
								</span>
                            </div>
                        </div>
                    </div>
                    
					<div class="form-group form-inline">
						
						<label class="control-label col-sm-1">학년</label>
						<div class="col-sm-*">
						    <select class="form-control" id="student-type">
						        <option value="10">초등</option>
						        <option value="20">중등</option>
						        <option value="30">고등</option>
						    </select>
						    <select class="form-control" id="grade-type">
						        <option value="0">전체</option>
						        <option value="1">1학년</option>
						        <option value="2">2학년</option>
						        <option value="3">3학년</option>
						        <option value="4">4학년</option>
						        <option value="5">5학년</option>
						        <option value="6">6학년</option>
						        <option value="7">7학년</option>
						    </select>
						    
						    <label class="control-label" style="margin-left: 20px; margin-right: 20px; vertical-align: baseline;">검수여부</label>
						    <select class="form-control" id="seach-inspect-type">
						        <option value="">전체</option>
						        <option value="10">미확인</option>
						        <option value="20">검수완료</option>
						        <option value="30">검수거절</option>
						    </select>
						    
						</div>
                    </div>

                    <!-- 검색어 -->
                    <div class="form-group form-inline">
                        <label class="control-label col-sm-1" for="search-type">검색어</label>
                        <div class="col-sm-*">
                            <select class="form-control" id="search-type">
                                <option value="10">전체</option>
                                <option value="20">담당교사</option>
                                <option value="30">아이디</option>
                                <option value="40">답안ID</option>
                                <option value="50">교재명</option>
                            </select>

                            <input type="text" class="form-control" id="search-word">

                            <button type="button" class="btn btn-primary" id="btn-search">
                                <span class="fa fa-search"></span> 조회
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.panel 첨삭 검색 -->

        <!-- /.panel 버튼 -->
        <div class="panel panel-success">
            <div class="panel-heading">
            	
				<span style="float: left; margin-top: 5px;">
					관리자 검수
				</span>
            	
            	<div class="btn-group pull-right">
					<button type="button" class="btn btn-primary" id="btn-excel">
					    <span class="fa fa-file-excel-o"></span> 엑셀 다운로드
					</button>
				</div>
	            
	            <div class="clearfix"></div>
            	
            </div>
            
            <div class="panel-body">
	            <div class="table-responsive">
	                <table class="table table-bordered table-hover table-striped" id="dt-inspect" width="100%">
	                    <thead>
	                    <tr>
	                        <th>No.</th>
	                        <th>답안ID</th>
	                        <th>학년</th>
	                        <th>교재명</th>
	                        <th>페이지</th>
	                        <th>아이디</th>
	                        <th>학습자</th>
	                        <th>첨삭요청일</th>
	                        <th>승인요청일</th>
	                        <th>승인완료일</th>
	                        <th>담당교사</th>
	                        <th>검수여부</th>
	                    </tr>
	                    </thead>
	                </table>
	            </div>
            </div>
            
			<div class="panel-footer clearfix">
				<div class="pull-right">
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-config">
						<span class="fa fa-check-square-o"></span> 검수 조건 관리
					</button>
				</div>
			</div>
            
        </div>
    	<!-- /.panel 첨삭 리스트 -->
	</div>
	<!-- /.container -->
	
	<div id="modal-detail" class="modal fade" role="dialog" style="display: none; z-index: 1050;">
		<div class="modal-dialog modal-lg">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">상세정보</h4>
				</div>
				<div class="modal-body">
					
					<div class="panel-group">
						<div class="panel panel-default">
							<div class="panel-heading">첨삭 상세</div>
							<div class="panel-body">
								<form class="form-horizontal" role="form">
									<div class="form-group">
										<label class="control-label col-sm-1" for="an_id">답안ID</label>
										<div class="col-sm-5">
											<h4 class="text-info" id="an_id" data-value></h4>
										</div>
										
										<label class="control-label col-sm-1" for="student_nm">학습자</label>
										<div class="col-sm-5">
											<h4 class="text-info" id="student_nm"></h4>
										</div>
									</div>
									
									<div class="form-group">
										<div class="col-sm-12">
											<div class="well well-sm">
												<img alt="" class="img-thumbnail img-responsive center-block" id="noteImage">
											</div>
										</div>
									</div>
									
									<div class="form-group">
										<label class="control-label col-sm-1" for="st_id">담당교사</label>
										<div class="col-sm-4">
											<select class="form-control" id="st_id" name="st_id"></select>
										</div>
										
									</div>
									
									<div class="form-group">
										<label class="control-label col-sm-1" for="an_state">상태</label>
										<div class="col-sm-10">
											<div class="btn-group" id="note-state" data-toggle="buttons">
											
												<label class="btn btn-default btn-set-or-raise">
													<input type="radio" id="state-0" name="an_state" value=0>첨삭대기
												</label>
				
												<label class="btn btn-default">
													<input type="radio" id="state-1" name="an_state" value=1>첨삭중
												</label>
				
												<label class="btn btn-default">
													<input type="radio" id="state-2" name="an_state" value=2>승인대기
												</label>
												
												<label class="btn btn-default">
													<input type="radio" id="state-3" name="an_state" value=3>반려
												</label>
												
												<label class="btn btn-default">
													<input type="radio" id="state-4" name="an_state" value=4>승인완료
												</label>
											</div>
										</div>
									</div>
								</form>
							</div>
							<!-- /. panel-body -->
						</div>
						<!-- /.panel 첨삭 상세 -->
						
						<!-- 검수사유 -->
						<div class="panel panel-warning">							
							<div class="panel-body">
								
								<div class="pull-right">
									<label class="radio-inline">
										<input type="radio" name="inspect-state" value="20">검수완료
									</label>
									<label class="radio-inline">
										<input type="radio" name="inspect-state" value="30">검수거절
									</label>
								</div>
								
								<textarea id="inspect-reason" class="form-control" rows="5" placeholder="검수 사유를 입력할 수 있습니다."></textarea>
								
								<div class="well well-sm">
						            <strong class="primary-font"><span id="admin-nm"></span></strong>
	
						            <small class="text-muted">
										&nbsp;<span id="reg-date"></span>
						            </small>
						            <div class="pull-right">
						            	<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal-logs">
											<span class="fa fa-stack-exchange"></span> 로그
										</button>
										<button type="button" class="btn btn-primary btn-xs" id="btn-remove">
											<span class="fa fa-eraser"></span> 삭제
										</button>
										<button type="button" class="btn btn-primary btn-xs" id="btn-save-inspect">
											<span class="fa fa-floppy-o"></span> 저장
										</button>
									</div>
					            </div>
							</div>
						</div>
						
						<!-- /.panel 검수사유 -->
						
						<!-- 
						<div class="panel panel-default">
							<div class="panel-body">
								<div class="btn-group pull-right">
									<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-logs">
										<span class="fa fa-stack-exchange"></span> 로그
									</button>
									<button type="button" class="btn btn-primary">
										<span class="fa fa-floppy-o"></span> 저장
									</button>
								</div>
							</div>
						</div>
						-->
						<!-- /.panel 버튼 -->
		  			</div>
		  			
				</div>
			</div>
		</div>
	</div>
	<!-- /.modal -->
	
	<div id="modal-config" class="modal fade" role="dialog" style="display: none; z-index: 1050;">
		<div class="modal-dialog modal-sm">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">검수 조건</h4>
				</div>
				<div class="modal-body">
					<div class="panel panel-default">
						<div class="panel-heading">검수 조건 설정</div>
						<div class="panel-body">
						
							<form class="form-horizontal" role="form">
								<div class="form-group">
									<label class="control-label col-sm-5" for="inspect-type">검수조건</label>
									<div class="col-sm-7">
									
										<select class="form-control" id="inspect-type">
			                                <option value="10">전체</option>
			                                <option value="20">학년별</option>
			                            </select>
									</div>
								</div>
								
								<div class="form-group">	
									<label class="control-label col-sm-5" for="inspect-rate">검수비율</label>
									<div class="col-sm-7">
										<div class="input-group">
											<input class="form-control" id="inspect-rate" type="number" pattern="^[0-9]" min="1" step="1">
											<span class="input-group-addon">%</span>
										</div>
									</div>
								</div>
								
								<!-- 
								<div class="form-group">
									<label class="control-label col-sm-5" for="inspect-fee">검수수수료</label>
									<div class="col-sm-7">
										<div class="input-group">
											<input class="form-control" id="inspect-fee" type="number" pattern="^[0-9]" min="0" step="100" value="200">
											<span class="input-group-addon">원</span>
										</div>
									</div>
								</div>
								-->
								
							</form>
							
						</div>
						<!-- /. panel-body -->
						
						<div class="panel-footer clearfix">
							<button type="button" class="btn btn-primary pull-right" id="btn-save-config">
								<i class="glyphicon glyphicon-save"></i> 저장
							</button>
						</div>
						
					</div>
					<!-- /.panel -->
				</div>
			</div>
		</div>
	</div>
	<!-- /.modal -->
	
	<div id="modal-logs" class="modal fade" role="dialog" style="display: none; z-index: 1060;">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">검수로그</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-lg-12" >
							<div class="panel panel-info"> 
								<div class="panel-heading">
									<h3 class="panel-title">로그</h3>
								</div>
								<!-- /.panel-heading -->
								
								<div class="panel-body">
									<div class="row">
										<div class="col-lg-12">
											<div id="logs"></div>												
										</div>
									</div>
								</div>
								<!-- /.panel-body -->
							</div>
							<!-- /.panel -->
							
						</div>
						<!-- /.col -->
					</div>
					<!-- /.row -->
				</div>
			</div>
		</div>
	</div>
	<!-- /.modal -->
	
	<content tag="local_script">
		<script>
			$(document).ready(function() {
				
				var _DATA_TABLE;
				//$.fnTeachers();
				
				$('#datetimepicker-start').datetimepicker({
				    locale: 'ko'
				    , format: 'YYYY-MM-DD'
				    , defaultDate: moment().add(-30, 'day').format('YYYY-MM-DD')
				});
				
				$('#datetimepicker-end').datetimepicker({
				    useCurrent: false //Important! See issue #1075
				    , locale: 'ko'
				    , format: 'YYYY-MM-DD'
				    , defaultDate: moment().format('YYYY-MM-DD')
				});
				
				//check datetimepicker
				$("#datetimepicker-start").on("dp.change", function (e) {
				    $('#datetimepicker-end').data("DateTimePicker").minDate(e.date);
				});
				
				$("#datetimepicker-end").on("dp.change", function (e) {
				    $('#datetimepicker-start').data("DateTimePicker").maxDate(e.date);
				});
			
				//DATA TABLE
				_DATA_TABLE = $.fnDataTablesInspect();
				$('input:text').keydown(function(e) {
					if (e.keyCode == 13) {
						_DATA_TABLE.draw();
						return false;    //<---- Add this line
					}
				});
				
				//검색
				$('#btn-search').on('click', function (e) {
				    _DATA_TABLE.draw();
				});
				
				//엑셀 다운로드
				$('#btn-excel').on('click', function (e) {
				
					var page;
                    var size;
                    if ($.type(_DATA_TABLE) === 'undefined') {
                        page = 0;
                        size = 10;
                    } else {
                        page = _DATA_TABLE.page.info().page;
                        size = _DATA_TABLE.page.info().length;
                    }
                    
				    $.fileDownload(
				    		"/inspect/excel"
				    		+"?dateType=" 			+ $('#date-type').val() 
				    		+ "&searchStart=" 		+ $('#search-start').val()
				        	+ "&searchEnd=" 		+ $('#search-end').val()
				        	+ "&studentType=" 		+ $('#student-type').val()
				        	+ "&gradeType=" 		+ $('#grade-type').val()
				        	+ "&searchInspectType=" + $('#seach-inspect-type').val()
				        	+ "&searchType=" 		+ $('#search-type').val()
				        	+ "&searchWord=" 		+ $('#search-word').val()
				        )	
				        .done(function (e) {
				            $.notify({message: "다운로드가 완료되었습니다", icon: 'fa fa-info-circle'}, {type: 'success'});
				        })
				        .fail(function (jqXHR, textStatus, errorThrown) {
				            $.notify({message: jqXHR.responseText, icon: 'fa fa-warning'}, {type: 'danger'});
				        });
				    		
				    return false; //this is critical to stop the click event which will trigger a normal file download!
				});
			
				$('#student-type').on('click', function (e) {
				
				    var selectedValue = $("#student-type option:selected").val();
				    $('#grade-type').empty();
				    if (selectedValue == 10) {
				        $('#grade-type').append("<option value='0'>전체</option>");
				        $('#grade-type').append("<option value='1'>1학년</option>");
				        $('#grade-type').append("<option value='2'>2학년</option>");
				        $('#grade-type').append("<option value='3'>3학년</option>");
				        $('#grade-type').append("<option value='4'>4학년</option>");
				        $('#grade-type').append("<option value='5'>5학년</option>");
				        $('#grade-type').append("<option value='6'>6학년</option>");
				        $('#grade-type').append("<option value='7'>7학년</option>");
				    } else {
				        $('#grade-type').append("<option value='0'>전체</option>");
				        $('#grade-type').append("<option value='1'>1학년</option>");
				        $('#grade-type').append("<option value='2'>2학년</option>");
				        $('#grade-type').append("<option value='3'>3학년</option>");
				    }
				});
				
				//검수 조건 저장
				$('#btn-save-config').on('click', function (e) {
					
					e.preventDefault();
					
					var type = $('#inspect-type').val();
					var rate = $('#inspect-rate').val();
					//var fee = $('#inspect-fee').val();
					
					if($.isBlank(type)) {$.fnWarning("검수 조건을 설정해 주세요"); return;}
					if($.isBlank(rate)) {$.fnWarning("검수 비율을 설정해 주세요"); return;}
					//if($.isBlank(fee)) {$.fnWarning("검수 수수료를 설정해 주세요"); return;}
					
					
					if (confirm('검수 조건을 저장 하시겠습니까?')) {
						$.ajax({
							type: 'POST',
							url: '/inspect/config/save',
							data: {
								inspect_type: type
								, inspect_rate: rate
								, inspect_fee: 200
							},
							async: true,
							dataType: 'json',
							beforeSend: function (xhr) {
								$.blockUI();
							},
							success: function (data) {
								$.notify({message: "검수 조건이 저장 되었습니다",icon: 'fa fa-info-circle'},{type: 'success'});
								$('#modal-config').modal('hide');
							},
							error: function (jqXHR, textStatus, errorThrown) {},
							complete: function (e) {
							    $.unblockUI();
							}
						});
		       		}
				});
				
				//검수 내용 삭제
				$('#btn-remove').on('click', function (e) {
					e.preventDefault();
					
					if (confirm('검수 사유를 삭제하시겠습니까?')) {
						$.ajax({
							type: 'POST',
							url: '/inspect/remove',
							data: {
								anId : $('#an_id').data("value")
							},
							async: true,
							dataType: 'json',
							beforeSend: function (xhr) {
								$.blockUI();
							},
							success: function (data) {
								$.notify({message: "검수 사유가 삭제 되었습니다",icon: 'fa fa-info-circle'},{type: 'success'});
								$('#modal-detail').modal('hide');
							},
							error: function (jqXHR, textStatus, errorThrown) {},
							complete: function (e) {
							    $.unblockUI();
							}
						});
		       		}
				});
				
				//검수 내용 저장
				$('#btn-save-inspect').on('click', function (e) {
					e.preventDefault();
					
					if (confirm('검수 사유를 저장하시겠습니까?')) {
						$.ajax({
							type: 'POST',
							url: '/inspect/save',
							data: {
								an_id : $('#an_id').data("value")
								, inspect_reason : $('#inspect-reason').val()
								, inspect_state : $(':radio[name="inspect-state"]:checked').val()
							},
							async: true,
							dataType: 'json',
							beforeSend: function (xhr) {
								$.blockUI();
							},
							success: function (data) {
								$.notify({message: "검수 사유가 저장 되었습니다",icon: 'fa fa-info-circle'},{type: 'success'});
								$('#modal-detail').modal('hide');
								_DATA_TABLE.draw();
							},
							error: function (jqXHR, textStatus, errorThrown) {},
							complete: function (e) {
							    $.unblockUI();
							}
						});
		       		}
				});
				
				//기간설정
				$('#search-term').on('change',function (e) {

					//change start date
					$('#datetimepicker-start')
						.data("DateTimePicker")
						.date(
							$('#datetimepicker-end').data("DateTimePicker").date()
							.add(($(this).val() * -1), 'day').format('YYYY-MM-DD')
						);
				});
			});
			
			//modal config
			$('#modal-config').on("show.bs.modal", function(e) {
				
				//call modal target
				var rt = $(e.relatedTarget);
				
				$.ajax({
					type : 'POST',
					url : '/inspect/config',
					data: {},
					async : true,
					dataType : 'json',
					beforeSend : function(xhr) {
						$.blockUI();
					},
					success : function(d) {
						
						var data = d.data;
						
						$('#inspect-type').val(data.inspect_type);
						$('#inspect-rate').val(data.inspect_rate);
						//$('#inspect-fee').val(data.inspect_fee);
						
					},
					error : function(jqXHR, textStatus, errorThrown) {
						$.notify({message: jqXHR.responseJSON.message,icon: 'fa fa-warning'},{type: 'danger'});
					},
					complete : function(e) {
						$.unblockUI();
					}
				});
				//.ajax
			});
			
			//modal detail
			$('#modal-detail').on("show.bs.modal", function(e) {
				
				//call modal target
				var rt = $(e.relatedTarget);
				var anId = rt.data("id");
				
				$.ajax({
					type: 'POST',
					url: '/inspect/detail',
					data: {
						anId : anId
					},
					async: true,
					dataType: 'json',
					beforeSend: function (xhr) {},
					success: function (data) {
					
						$("#an_id").text(data.note.an_id);
						$("#an_id").data('value', data.note.an_id);
						$("#student_nm").text(data.note.student_nm);
						
						if(data.inspect.inspect_state != 10){
							
							//checked
							$("input[name=inspect-state][value=" + data.inspect.inspect_state + "]").prop('checked', true);
							
							$("#inspect-reason").val(data.inspect.inspect_reason);
							$("#admin-nm").text(data.inspect.admin_nm);
							
							$("#btn-remove").removeClass('hidden');
							
							var regDate = "";
							if(!$.isBlank(data.inspect.admin_id)){
								regDate = $.format.date(data.inspect.reg_date, "yyyy-MM-dd HH:mm");
							}else{
								//검수 사유 버튼도 제거
								$("#btn-remove").addClass('hidden');
								
							}
							$("#reg-date").text(regDate);
							
						}else{
							
							//unchecked
							$("input:radio[name='inspect-state']").each(function(i) {
								this.checked = false;
							});
							
							$("#inspect-reason").val('');
							$("#admin-nm").text('');
							$("#reg-date").text('');
							
							$("#btn-remove").addClass('hidden');
							
						}
						
						$("#noteImage").attr('src', data.path);
						
						//GET TEACHERS
						$.fnDtailTeachers(data.note.st_id);
						
						//SET PAGE-SETTING
						$('input:radio[name=an_state][value='+data.note.an_state+']').attr('checked', true);
						$.fnNoteStateHandler(parseInt(data.note.an_state), data.refuse);
						
					}, 
					error: function (jqXHR, textStatus, errorThrown) {},
					complete: function (e) {}
				});
			});
			
			//modal logs
			$('#modal-logs').on("show.bs.modal", function(e) {
				
				//call modal target
				var rt = $(e.relatedTarget); 
				
				$.ajax({
					type : 'POST',
					url : '/inspect/logs',
					data: {
						anId : $('#an_id').data("value")
					},
					async : true,
					dataType : 'json',
					beforeSend : function(xhr) {},
					success : function(data) {
						
						$('#logs').empty();
						$('#tmpl-logs')
							.tmpl(data)
							.appendTo($('#logs'));
						
					},
					error : function(jqXHR, textStatus, errorThrown) {
						$('#modal-logs').modal('hide');
						$.notify({message: jqXHR.responseJSON.message,icon: 'fa fa-warning'},{type: 'danger'});
					},
					complete : function(e) {
						//$.unblockUI();
					}
				});
				//.ajax
			});
			
			//GET Teachers
			$.fnDtailTeachers = function(stId) {
				
				$.ajax({
					type : 'POST',
					url : '/teacher/teachers',
					data : {},
					async : true,
					dataType : 'json',
					beforeSend : function(xhr) {},
					success : function(data) {

						$('#st_id').empty();

						$("#tmpl-teachers")
						.tmpl(data)
						.appendTo($('#st_id'));
						
						//setting teacher
						$('#st_id').val(stId);
						
					},
					error : function(jqXHR, textStatus, errorThrown) {},
					complete : function(e) {}
				});
			};
			
		</script>
		
		<script id="tmpl-teachers" type="text/x-jquery-tmpl">
			<option value="\${st_id}">\${st_nm}</option>
		</script>
		
		<script id="tmpl-logs" type="text/x-jquery-tmpl">
			<ul class="chat">
				<li class="left clearfix">
				    <span class="chat-img pull-left">
				        <i class="fa fa-comment fa-3x"></i>
				    </span>

					<div class="chat-body clearfix">
				        <div class="header">
				        	<!-- ids -->
				            <strong class="primary-font">\${admin_nm}</strong>
							
							{{if inspect_state == 10}}
		    					<span class="label label-primary">검수여부&nbsp;&nbsp;&nbsp;미확인</span>
							{{else inspect_state == 20}}
								<span class="label label-success">검수여부&nbsp;&nbsp;&nbsp;검수완료</span>
							{{else inspect_state == 30}}
		    					<span class="label label-danger">검수여부&nbsp;&nbsp;&nbsp;검수거절</span>
							{{/if}}							

				            <!-- time -->
				            <small class="pull-right text-muted">
								<i class="fa fa-clock-o fa-fw"></i> \${$.format.date(reg_date, "yyyy-MM-dd HH:mm")}
				            </small>
				        </div>
				        
						<div class="well well-sm" style="margin-top: 10px; min-height: 30px;">
							<p class="text-muted">\${inspect_reason}</p>
						</div>
				    </div>
				</li>
			</ul>
		</script>
	</content>
</body>
</html>