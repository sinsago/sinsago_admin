package com.epopcon.smartssen;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epopcon.smartssen.domain.Admin;
import com.epopcon.smartssen.mapper.ManagerMapper;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(SmartssenAdminApplication.class)
public class SmartssenAdminApplicationTests {

	@Autowired private ManagerMapper managerMapper;
	//@Autowired private UserAnnotationMapper userMapper;
	
	@Test
	public void findAllUsers()  {
		List<Admin> users = managerMapper.findAllManagers();
		assertNotNull(users);
		assertTrue(!users.isEmpty());
		
	}
	
	@Test
	public void findUserById()  {
		Admin user = managerMapper.findManagerById("spadmin");
		assertNotNull(user);
	}
	
	/*
	@Test
	public void createUser() {
		Admin user = new Admin(0, "Siva", "siva@gmail.com");
		managerMapper.insertUser(user);
		Admin newUser = managerMapper.findUserById(user.getId());
		assertEquals("Siva", newUser.getName());
		assertEquals("siva@gmail.com", newUser.getEmail());
	}
	*/

}
