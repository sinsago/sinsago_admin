package com.epopcon.smartssen;

import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Test;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.epopcon.smartssen.domain.Article;
import com.epopcon.smartssen.domain.Comment;

public class RestfulTestClient {
	
	@Test
	public void testSubmit() throws Exception {
		
		
		URI uri = URI.create("http://api.smartssen.com/v1/answer_note_update.aspx");
		
//		File file = new File("D:\\epopcon\\images\\smartssen\\201610\\27\\1610-S000001-13968786024930228.png");
		
		Path path = Paths.get("D:\\epopcon\\images\\smartssen\\201610\\27\\1610-S000001-13968786024930228.png");
		
		RestTemplate restTemplate = new RestTemplate();

        MultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();
        map.add("an_id", "20161028-000005");
        //map.add("wr_img_file", file); // MultipartFile
        
        
        ByteArrayResource contentsAsResource = new ByteArrayResource(Files.readAllBytes(path)){
            @Override
            public String getFilename(){
                return path.getFileName().toString();
            }
        };
        map.add("wr_img_file", contentsAsResource);

        Article response = restTemplate.postForObject(uri, map, Article.class);
        
        
        System.out.println(response.getResult());
        
        Comment comment = response.getResult();
        
        System.out.println(comment.getCode());
        System.out.println(comment.getMessage());
        System.out.println(comment.getServer_date());
        
        
        
        
        
        
//        Map<String, Object> response = restTemplate.postForObject(uri, map, Map.class);
//        
//        System.out.println(response.toString());

//        Map<String, Object> result = Maps.newHashMap();
//        result = (Map<String, Object>) response.get("result");
//		
//        System.out.println(result.get("code"));
//        System.out.println(result.get("message"));
//        System.out.println(result.get("server_date"));
		
		
		
		
	}

}
